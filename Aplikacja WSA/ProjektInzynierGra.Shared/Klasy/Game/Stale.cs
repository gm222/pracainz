﻿using System;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Streams;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System.Runtime.CompilerServices;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Shapes;
using Windows.Foundation;


namespace ProjektInzynierGra.Klasy.Game
{
    static public class Stale
    {
        public const Byte iloscMiast = 5;
        public static ImageSource[] listaObrazkowBudynkow;
        public static ImageSource[] listaObrazkowPostaci;


        private static int szerokoscPola = 50;
        private static int iloscPol = 60;

       
        public static string NazwaKlasy(object obj)
        {
            return obj.GetType().Name;
        }

        public static int SzerokoscPola
        {
            get { return szerokoscPola; }
        }
        public static int IloscPol
        {
            get { return iloscPol; }

        }
        private static string[] domkiObrazki = new string[]
        {
            "ms-appx:///Assets/img/0.png",
            "ms-appx:///Assets/img/1.png"
        };

        

       

        public static Dictionary<Type, byte[]> daneFormatki = new Dictionary<Type, byte[]>()
        {
            {typeof(Formatki.Dom),              new byte[]{0,2}},
        };


        static public ImageSource PobierzObrazekDomkuFormatka(Int16 i)
        {
            return listaObrazkowBudynkow[i];
        }

        public static async Task<T> Odczyt<T>(StorageFile plik)
        {
            var objectPlik = new object();

            try
            {
                using(IInputStream plikStreamInput = await plik.OpenReadAsync())
                {
                    var serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(T));
                    using(var plikStream = plikStreamInput.AsStreamForRead())
                    {
                        objectPlik = serializer.ReadObject(plikStream);
                        await plikStream.FlushAsync();
                    }
                }
            }
            catch(Exception)
            {
                return (T)(new Object());
            }

            return (T)objectPlik;
        }

        public static async Task<string> StworzPlik(StorageFolder folderZapisu, string nazwa = null)
        {
            if(nazwa == null || nazwa.Length == 0)
            {
                var data = DateTime.Now;
                nazwa = String.Format("{0}{1}{2}{3}{4}{5}{6}", data.Year, data.Month, data.Day, data.Hour, data.Minute, data.Second, data.Millisecond);
            }

            var plik = await folderZapisu.CreateFileAsync(nazwa);


            await Zapisz(String.Empty, plik); 

            return Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.Add(plik);
        }
        public static async Task<bool> Zapisz<T>(T obj, StorageFile plik)
        {
            try
            {
                await FileIO.WriteTextAsync(plik, String.Empty);

                
                
                using(var otwartyPlik = await plik.OpenStreamForWriteAsync())
                {
                    
                        var serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(T));
                        serializer.WriteObject(otwartyPlik, obj);
                        await otwartyPlik.FlushAsync();
                    
                }
            }
            catch(Exception)
            {

                return false;
            }

            return true;
        }


        static public Windows.UI.Xaml.Controls.Image UtworzObrazek(double x, double y, string lok = "", Int16 z_index = 0, CompositeTransform comp = null)
        {
            if(comp == null)
                comp = new CompositeTransform();

            var o = new Windows.UI.Xaml.Controls.Image()
            {
                Width = x,
                Height = y,
                Source = new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri(lok)),
                RenderTransform = comp
            };
            Windows.UI.Xaml.Controls.Canvas.SetZIndex(o, z_index);
            return o;

        }

        public static Rectangle UtworzObrazek2(double x, double y, string lok = "", Int16 z_index = 0, CompositeTransform comp = null)
        {
            if(comp == null)
                comp = new CompositeTransform();
            var rec = new Rectangle()
            {
                Width = x,
                Height = y,
                Fill = new ImageBrush()
                {
                    AlignmentX = AlignmentX.Right,
                    AlignmentY = AlignmentY.Top,
                    RelativeTransform = comp,
                    ImageSource = new BitmapImage(new Uri(lok)),
                    Stretch = Stretch.UniformToFill,
                
                }
            };

            return rec;
        }
        public static Rectangle UtworzObrazek3(double x, double y, ImageSource lok, Int16 z_index = 0, CompositeTransform comp = null)
        {
            if(comp == null)
                comp = new CompositeTransform();
            var rec = new Rectangle()
            {
                Width = x,
                Height = y,
                Fill = new ImageBrush()
                {
                    AlignmentX = AlignmentX.Left,
                    AlignmentY = AlignmentY.Top,
                    RelativeTransform = comp,
                    ImageSource = lok, 
                    Stretch = Stretch.UniformToFill,

                }
            };

            return rec;
        }

        public static Rectangle UtworzObrazek4(double x, double y, ImageSource lok, Int16 z_index = 0, CompositeTransform comp = null)
        {
            if(comp == null)
                comp = new CompositeTransform();
            var rec = new Rectangle()
            {
                Width = x,
                Height = y,
                Fill = new ImageBrush()
                {
                    AlignmentX = AlignmentX.Left,
                    AlignmentY = AlignmentY.Top,
                    RelativeTransform = comp,
                    ImageSource = lok,
                    Stretch = Stretch.None,

                }
            };

            return rec;
        }

        //static public void UstawPozycjeXYZPostaci(Windows.UI.Xaml.UIElement obj, Point pos,int z_index = 0)
        //{
        //    Canvas.SetLeft(obj, pos.X);
        //    Canvas.SetTop(obj, pos.Y);
        //    Canvas.SetZIndex(obj, z_index);
        //}

        static public void UstawPozycjeXYZPostaci(Windows.UI.Xaml.UIElement obj, Point pos, ObrazkiWlasciwosci parametr = null, int z_index = 0)
        {
            Canvas.SetLeft(obj, (int)pos.X - parametr.PrzesuniecieObrazkaX);
            Canvas.SetTop(obj,  (int)pos.Y - parametr.PrzesuniecieObrazkaY);
            Canvas.SetZIndex(obj, z_index);

        }
        static public void UstawPozycjeXYZ(Windows.UI.Xaml.UIElement obj, Point pos, ObrazkiWlasciwosci parametr = null ,int z_index = 0)
        {
            Canvas.SetLeft(obj, ((int)pos.X / Stale.SzerokoscPola * Stale.SzerokoscPola) - parametr.PrzesuniecieObrazkaX);
            Canvas.SetTop(obj, ((int)pos.Y / Stale.SzerokoscPola * Stale.SzerokoscPola) - parametr.PrzesuniecieObrazkaY);
            Canvas.SetZIndex(obj, z_index);
        }
        static public void WlasciwoscObrazkaComposit(ref Windows.UI.Xaml.UIElement obj, Point pos)
        {
            var translate = obj.RenderTransform as Windows.UI.Xaml.Media.CompositeTransform;

            translate.TranslateX = (int)pos.X / 50 * 50;
            translate.TranslateY = (int)pos.Y / 50 * 50;
            translate.Rotation = 0;
        }

    }
}
