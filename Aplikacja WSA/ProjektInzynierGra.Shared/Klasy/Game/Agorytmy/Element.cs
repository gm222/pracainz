﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektInzynierGra.Klasy.Game.AlgorytmyFunckje
{
    public class Element
    {
        private int x;
        private int y;
        private Element rodzic;
        private double odlegloscOdKonca;
        private int kara;// za każdy kolejny wstecz = +1 kary
        private double suma;

        private List<Element> sasiedzi = new List<Element>();

        public Element(int X, int Y, Element Rodzic, double OdlegloscOdKonca, int Kara)
        {
            x = X; 
            y = Y; 
            rodzic = Rodzic; 
            odlegloscOdKonca = OdlegloscOdKonca;
            kara = Kara;
            suma = OdlegloscOdKonca + Kara;
        }

        public int X
        {
            get { return x; }
        }
        public int Y
        {
            get { return y; }
        }
        public Element Rodzic
        {
            get { return rodzic; }
        }
        public double OdlegloscOdKonca
        {
            get { return odlegloscOdKonca; }
        }
        public int Kara
        {
            get { return kara; }
            set 
            { 
                kara = value;
                suma = OdlegloscOdKonca + value;
            }
        }
        public double Suma
        {
            get { return suma; }
        }
        public int IloscSasiadow
        {
            get { return sasiedzi.Count; }
        }

        public Element NajlepszySasiad()
        {
            Element tym = sasiedzi[0];
            sasiedzi.RemoveAt(0);
            return tym;
        }
        public bool CzyBezKary
        {
            get 
            {
                foreach(Element aktualnySasiad in sasiedzi)
                    if(aktualnySasiad.Kara < this.kara)
                        return true;
                return false;
            }
        }

        public void DodajSasiada(Element nowySasiad)
        {
            int i = 0;
            foreach(Element sasiad in sasiedzi)
            {
                if(sasiad.odlegloscOdKonca+ sasiad.kara > nowySasiad.odlegloscOdKonca + nowySasiad.kara)
                    break;
                i++;
            }
            sasiedzi.Insert(i, nowySasiad);
        }

        public void Delete()
        {
            if(rodzic!=null)
                rodzic.sasiedzi.Remove(this);
        }
    }
}
