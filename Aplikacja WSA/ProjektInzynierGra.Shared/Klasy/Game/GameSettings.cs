﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;


namespace ProjektInzynierGra.Klasy.Game
{
    public class GameSettings
    {
        public ObrazkiWlasciwosci[] obrazkiWlasciwosciTabela = new ObrazkiWlasciwosci[]{};
        public ObrazkiWlasciwosci[] obrazkiWlasciwosciPostacTabela = new ObrazkiWlasciwosci[] { };
        Kampania[] kampanie;
        string error;

        List<string> miasta = new List<string>()
        {
            "PierwszeMiasto",
            "DrugieMiasto",
            "TrzecieMiasto",
            "CzwarteMiasto",
            "PiąteMiasto",
            "SzósteMiasto"
        };

        public GameSettings()
        {

        }

        public GameSettings(string e) : this()
        {
            this.error = e;
            
        }

        public async Task<bool> InicializujeAnimacje()
        {
            try
            {
                Stale.listaObrazkowBudynkow = await PobierzObrazki("Budynki");
                Stale.listaObrazkowPostaci  = await PobierzObrazki("Postacie");

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }
        
        private async Task<ImageSource[]> PobierzObrazki(string nazwaFolderu)
        {
            var assetsFolder = (await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFoldersAsync()).ToList().First(a => a.DisplayName.Equals("Assets"));

            var imgFolder = await assetsFolder.GetFolderAsync(nazwaFolderu);
            var itemsOfimgFolder = await imgFolder.GetFilesAsync();

            ImageSource[] obrazkiTymczasowe = new ImageSource[itemsOfimgFolder.Count];

            for(int i = 0; i < itemsOfimgFolder.Count; i++)
                obrazkiTymczasowe[i] = new BitmapImage(new Uri(String.Format("ms-appx:///Assets/{0}/{1}.png", nazwaFolderu, i)));

            return obrazkiTymczasowe;
        }

        private async Task<bool> Odczyt(Uri url, bool postac = false) //dodac odczyt gdzie robilem zapisywanie, jest to funkcja uniwersalna
        {
            try
            {
                using(IInputStream fileStream = await (await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(url)).OpenReadAsync())
                {
                    var sessionSerializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(Int16[][]));

                    using(var x = fileStream.AsStreamForRead())
                    {
                        var listobjects = sessionSerializer.ReadObject(x) as Int16[][];
                        int j = 0;

                        switch(postac)
                        {
                            case false:
                            obrazkiWlasciwosciTabela = listobjects.Select((u, i) => new ObrazkiWlasciwosci((byte)u[0], (byte)u[1], u[2], u[3], u[4], u[5])).ToArray();
                            break;
                            case true:
                            obrazkiWlasciwosciPostacTabela = listobjects.Select((u, i) => new ObrazkiWlasciwosci((byte)u[0], (byte)u[1], u[2], u[3])).ToArray();
                            break;
                        
                        
                    }

                        await x.FlushAsync();
                    }
                }
            }
            catch(Exception)
            {
                return false;
            }
            
            return true;
        }

        public async Task<bool> LoadSettings()
        {

            var localFolder = ApplicationData.Current.LocalFolder;

            StorageFile plikGraf = null;

            try
            {
                plikGraf = await localFolder.GetFileAsync("graf");
            }
            catch(Exception)
            {
            }
            

            if(plikGraf == null)
            {
                //tutaj stworzyc strukture
            }

            //tutaj załadować strukturę

            if(!await Odczyt(new Uri("ms-appx:///Assets/ustawienia/obrazkiBydynkow.json")))
            {
                return false;
            }

            if(!await Odczyt(new Uri("ms-appx:///Assets/ustawienia/obrazkiPostacie.json"), true))
            {
                return false;
            }
            
            return true;
        }



    }


    
}
