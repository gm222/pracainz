﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ProjektInzynierGra.Klasy.Game
{
    public class Kampania
    {
        
        bool dostepny;
        byte id_miasta;
        byte[] parent;

        public bool Dostepny
        {
            get
            {
                return this.dostepny;
            }
            set
            {
                this.dostepny = value;
            }
        }

        

    }

    public class MiastoSave : ObservableCollection<SaveGame>, INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private string nazwaMiasta;

        [DataMember]
        public string NazwaMiasta
        {
            get { return nazwaMiasta; }
            set { nazwaMiasta = value; this.OnPropertyChanged(); }
        }

        private ObservableCollection<SaveGame> gra;

        [DataMember]
        public ObservableCollection<SaveGame> ListaStanowGier
        {
            get { return gra; }
            set {

                this.gra.Clear();
                foreach(var it in value)
                {
                    gra.Add(it);
                }
                this.OnPropertyChanged(); 
            }
        }

        private bool dodawalny;

        private sbyte maxIloscLista = 10;

        public MiastoSave(string nazwa, ObservableCollection<SaveGame> list)
        {
            this.NazwaMiasta = nazwa;
            this.gra = list;
            Dodawalny = (ListaStanowGier.Count < maxIloscLista) ? true : false;
        }

        public void Remove(SaveGame item)
        {
            this.gra.Remove(item);

            if(ListaStanowGier.Count < maxIloscLista)
            {
                Dodawalny = true;
            }
        }

        public void Add(SaveGame item)
        {
            if(ListaStanowGier.Count >= maxIloscLista)
                return;

            ListaStanowGier.Add(item);

            if(ListaStanowGier.Count >= maxIloscLista)
            {
                Dodawalny = false;
            }

        }

        [DataMember]
        public bool Dodawalny //mozna rozwiazac lepiej, bez zapisywania tej zmiennej
        {
            get { return dodawalny; }
            set { dodawalny = value; this.OnPropertyChanged(); }
        }
    }

    [DataContract]
    public class SaveGame
    {

        private string token;
        private DateTime czasdata;
        private String nazwaSave;
        private sbyte stan;



        [DataMember]
        public DateTime DataCzas
        {
            get { return czasdata; }
            set { czasdata = value; }
        }

        [DataMember]
        public String NazwaSave
        {
            get { return nazwaSave; }
            set { nazwaSave = value; }
        }

        [DataMember]
        public sbyte StanGry
        {
            get { return stan; }
            set { stan = value; }
        }

        [DataMember]
        public string Token
        {
            get { return token; }
            set { token = value; }
        }



        public SaveGame(string nazwa, sbyte stan, string token)
        {
            this.NazwaSave = nazwa;
            this.Token = token;
            this.StanGry = stan;
            this.DataCzas = DateTime.Now;
        }


    }
}
