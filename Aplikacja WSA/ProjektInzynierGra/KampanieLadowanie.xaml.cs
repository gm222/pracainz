﻿using ProjektInzynierGra.Common;
using ProjektInzynierGra.Klasy.Game;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.AccessCache;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace ProjektInzynierGra
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class KampanieLadowanie : Page
    {

        StorageFolder localFolder;
        public static StorageFolder folderZapisu;

        public static StorageFile plikUstawien = null;
        public static StorageItemAccessList zapisanePliki;

        public static ObservableCollection<ObservableCollection<SaveGame>> listaMiast = new ObservableCollection<ObservableCollection<SaveGame>>();
        bool schowany = true;
        sbyte miastoID = -1;
        ObservableCollection<MiastoSave> MiastoInfo;


        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public KampanieLadowanie()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
            this.NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Enabled;
            this.Loaded += KampanieLadowanie_Loaded;
            //LoadSettings(); //to jest dla gui, przerobic dla logiki loading
        }

        async void KampanieLadowanie_Loaded(object sender, RoutedEventArgs e)
        {
            await LoadSettings();  
        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private async Task LoadSettings()
        {
            //if(isLoaded)
            //    return;
            //isLoaded = true;
            zapisanePliki = Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList;

            //tutaj zrobić if który tylko sprawdza a nie przeładowuje - szkoda pamięci




            this.localFolder = ApplicationData.Current.LocalFolder;

            bool exists = true;

            try
            {
                folderZapisu = await localFolder.GetFolderAsync("SaveGame");
            }
            catch(FileNotFoundException)
            {
                exists = !true;
            }

            if(exists != true)
            {
                folderZapisu = await localFolder.CreateFolderAsync("SaveGame");
                exists = true;
            }

            try
            {
                plikUstawien = await localFolder.GetFileAsync("KampanieUstawienia");
                var doc = await plikUstawien.GetBasicPropertiesAsync();

                if(doc.Size == 0)
                    throw new FileNotFoundException();
            }
            catch(FileNotFoundException)
            {
                exists = !true;
            }

            if(exists != true)
            {
                for(byte i = 0; i < Stale.iloscMiast; i++)
                {
                    listaMiast.Add(new ObservableCollection<SaveGame>());
                }

                plikUstawien = await localFolder.CreateFileAsync("KampanieUstawienia", CreationCollisionOption.ReplaceExisting);

                await Stale.Zapisz(listaMiast, plikUstawien);

            }

            plikUstawien = await localFolder.GetFileAsync("KampanieUstawienia"); //wyłączyć w opcjach by się nie czepił niezadeklarowanych wartości

            try
            {
                listaMiast = await Stale.Odczyt<ObservableCollection<ObservableCollection<SaveGame>>>(plikUstawien) as ObservableCollection<ObservableCollection<SaveGame>>;
                //dopracować odczyt
            }
            catch(Exception)
            {

            }

            List<string> tokenyKopia = new List<string>();
            List<List<int>> tokenK   = new List<List<int>>();
            List<List<IStorageItem>> tokenKSI = new List<List<IStorageItem>>();


            for(int i = 0; i < listaMiast.Count; i++)
            {
                tokenK.Add(new List<int>());
                tokenKSI.Add(new List<IStorageItem>());

                for(int j = 0; j < listaMiast[i].Count; j++)
                {
                    
                    if(zapisanePliki.ContainsItem(listaMiast[i][j].Token) != true) // a != true
                    {

                        listaMiast[i].Remove(listaMiast[i][j]);
                        j--;
                        continue;
                    }

                    try
                    {
                        await zapisanePliki.GetItemAsync(listaMiast[i][j].Token);
                    }
                    catch(FileNotFoundException)
                    {
                        zapisanePliki.Remove(listaMiast[i][j].Token);
                        listaMiast[i].Remove(listaMiast[i][j]);
                        j--;
                        continue;
                    }
                    tokenK[i].Add(j);
                    tokenyKopia.Add(listaMiast[i][j].Token);
                    string tok = listaMiast[i][tokenK[i][j]].Token;
                    IStorageItem itm = await zapisanePliki.GetItemAsync(tok);
                    tokenKSI[i].Add(itm);

                }
            }

            zapisanePliki.Clear();

            for(int i = 0; i < tokenK.Count; i++)
            {
                for(int j = 0; j < tokenK[i].Count; j++)
                {
                    listaMiast[i][tokenK[i][j]].Token = zapisanePliki.Add(tokenKSI[i][j]);
                }
            }

            tokenK.Clear();      tokenK      = null;
            tokenyKopia.Clear(); tokenyKopia = null;
            tokenKSI.Clear();    tokenKSI    = null;

            await Stale.Zapisz(listaMiast, plikUstawien);

            MiastoInfo = new ObservableCollection<MiastoSave>();

            for(int i = 0; i < Stale.iloscMiast; i++)
            {
                MiastoInfo.Add(new MiastoSave(String.Format("Miasto{0}", i + 1), listaMiast[i]));

                
            }

            if(miastoID != -1)
            {
                var miasto = MiastoInfo[miastoID];
                //tutaj wystepuje od czasu do czasu błąd zwiazany z niezaładowaniem MiastoInfo - przetestować

                this.SaveInfoBar.DataContext = miasto;

                this.listaSave.ItemsSource = miasto.ListaStanowGier;
                //this.listaSave.ItemsSource = miasto.TestRead;
            }

        }

        private async void Miasto_Click(object sender, RoutedEventArgs e)
        {
            if(schowany == true)
            {
                
                Visible.Begin();
                schowany = false;
            }

            miastoID = (sbyte)(Convert.ToSByte((sender as Button).Tag.ToString()) - 1);
            if(MiastoInfo == null)
            {
                await LoadSettings();
            }
            var miasto = MiastoInfo[miastoID];

            this.SaveInfoBar.DataContext = miasto;
            this.listaSave.ItemsSource = miasto.ListaStanowGier;
        }

        private async void RemoveSave_Click(object sender, RoutedEventArgs e)
        {
            var sg = (sender as Button).DataContext as SaveGame;
            
            var miastolista = this.SaveInfoBar.DataContext as MiastoSave;

            if(miastolista == null)
                return;
            var plik = await zapisanePliki.GetFileAsync(sg.Token);

            plik.DeleteAsync();
            zapisanePliki.Remove(sg.Token);
            miastolista.Remove(sg);

            Stale.Zapisz(listaMiast, plikUstawien); //nie musimy czekać na rezultat
        }

        private async void NewSaveAdd_Click(object sender, RoutedEventArgs e)
        {
            var miastosaveItem = (sender as Button).DataContext as MiastoSave;

            if(miastosaveItem == null)
                return;

            var obj = new object[]
            {
              miastosaveItem
            };

            this.Frame.ForwardStack.Clear();
            await Task.Delay(500);
            this.Frame.Navigate(typeof(MainPage), (object)obj);
        }

        private async void LoadSelectedSave_Click(object sender, RoutedEventArgs e)
        {
            var selected = this.listaSave.SelectedItem;

            if(selected == null)
            {
                await new Windows.UI.Popups.MessageDialog("Wybierz zapisany stan gry lub utwórz nowy").ShowAsync();
                return;
            }

            var lista = listaSave.DataContext as MiastoSave;

            var obj = new object[]
            {
              lista,
              selected as SaveGame
            };

            List<string> listaa = new List<string>();
            foreach(AccessListEntry entry in zapisanePliki.Entries)
            {
                listaa.Add(entry.Token);
            }

            //pomysł z loadingiem taki, by tutaj wszystko poładować a później tylko przesłać do formatki

            this.Frame.ForwardStack.Clear();
            await Task.Delay(500);
            this.Frame.Navigate(typeof(MainPage), (object)obj);

            selected = null;

            this.listaSave.SelectedIndex = -1;
        }

        private void Grid_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
                miastoID = -1;
                Hide.Begin();
                schowany = true;
        }

        private void Wstecz_Click(object sender, RoutedEventArgs e)
        {
            if(navigationHelper.CanGoBack())
                this.navigationHelper.GoBack();
        }

        private void Ustawienia_Click(object sender, RoutedEventArgs e)
        {
            App.settingsPanel.ShowIndependent();
        }
    }
}