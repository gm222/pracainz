﻿using ProjektInzynierGra.Common;
using ProjektInzynierGra.Klasy.Game;
using System;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;


namespace ProjektInzynierGra
{

    sealed partial class App : Application
    {
        public static GameSettings gameConstans = new GameSettings();
        public static UstawieniaPanel settingsPanel = new UstawieniaPanel() { Title = "Ustawienia" };
        public static InformacjePanel infoPanel = new InformacjePanel() { Title = "Ustawienia" };

        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
        }

       
        protected async override void OnLaunched(LaunchActivatedEventArgs e)
        {

            bool resposne = await gameConstans.LoadSettings();


//#if DEBUG
//            if (System.Diagnostics.Debugger.IsAttached)
//            {
//                this.DebugSettings.EnableFrameRateCounter = true;
//            }
//#endif

            Frame rootFrame = Window.Current.Content as Frame;

            
            if (rootFrame == null)
            {
                rootFrame = new Frame();

                rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];
                rootFrame.NavigationFailed += OnNavigationFailed;

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }

                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                rootFrame.Navigate(typeof(StartPage), e.Arguments);
            }
           
            Window.Current.Activate();
        }

        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            await SuspensionManager.SaveAsync();
            deferral.Complete();
        }

        //protected async override void OnWindowCreated(WindowCreatedEventArgs e)
        //{
        //    SettingsPane.GetForCurrentView().CommandsRequested += OnCommandsRequested;
        //    settingsPanel.Title = "Ustawienia";

        //    //infoPanel.GetForCurrentView().CommandsRequested += OnCommandsRequested2;
        //    infoPanel.Title = "Informacje";
        //}

        //private void OnCommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        //{
        //    args.Request.ApplicationCommands.Add(new SettingsCommand("Ustawienia", "Ustawienia", (handler) => ShowSettingsBar()));
        //}

        //private void ShowSettingsBar()
        //{
        //    settingsPanel.Show();
        //}

        //private void OnCommandsRequested2(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        //{
        //    args.Request.ApplicationCommands.Add(new SettingsCommand("Informacje", "Informacje", (handler) => { infoPanel.Show(); }));
        //}
    }
}
