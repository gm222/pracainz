﻿using ProjektInzynierGra.Common;
using ProjektInzynierGra.Klasy.Game;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.AccessCache;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace ProjektInzynierGra
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : Page
    {



        
        private MiastoSave miastoInfo = null;

        public MainPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;

            
        }

        bool load = false;

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);

            var obj = e.Parameter as object[];

            miastoInfo = obj[0] as MiastoSave;

            if(obj.Count() == 1)
            {
                this.gSilnik = new GameEngine();
                this.Loaded += GameStart;
            }
            else
            {
                load = true;
                SaveGame sG = obj[1] as SaveGame;

                try
                {
                    var plik = await Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.GetFileAsync(sG.Token);

                    this.gSilnik = await Stale.Odczyt<GameEngine>(plik);
                    this.Loaded += GameStartLoaded;
                    
                }
                catch(Exception)
                {
                    //tutaj Zabezpieczenie
                    //this.navigationHelper.GoBack();
                    return;
                }
            }

            

            miastoInfo.ListaStanowGier.OrderBy(x => x.DataCzas);

            SaveDodatkoweOpcje.DataContext = miastoInfo;
            this.listaSave.ItemsSource = miastoInfo.ListaStanowGier;

            this.DataContext = this.gSilnik;
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            this.SaveBarVisible.Begin();
        }


        private void Load_Click(object sender, RoutedEventArgs e)
        {

        }

        private void RemoveSave_Click(object sender, RoutedEventArgs e)
        {

        }

        bool click = false;

        private void appBarButton_Click(object sender, RoutedEventArgs e)
        {
            this.listaSave.SelectedIndex = -1;

            (this.Resources["Clicked1"] as Storyboard).Begin();

            click = true;
        }



        private void AppBar_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            if(click == true)
            {
                (this.Resources["Clicked2"] as Storyboard).Begin();
                click = false;

            }
        }

        private void Anuluj_Click(object sender, RoutedEventArgs e)
        {
            this.SaveBarHide.Begin();
            //tutaj reszte animacji

        }

        private async void Zapisz_Click(object sender, RoutedEventArgs e)
        {

            GameEngine stanGry = this.gSilnik;
            SaveGame zapisanaGra;

            StorageFile plik;

            if(this.listaSave.SelectedIndex >= 0)
            {
                zapisanaGra = this.listaSave.SelectedItem as SaveGame;
                plik = await Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.GetFileAsync(zapisanaGra.Token);
            }
            else
            {
                var data    = DateTime.Now;
                var nazwa   = String.Format("{0}{1}{2}{3}{4}{5}{6}", data.Year, data.Month, data.Day, data.Hour, data.Minute, data.Second, data.Millisecond);

                string tok  = await Stale.StworzPlik(KampanieLadowanie.folderZapisu);
                plik        = await Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.GetFileAsync(tok);
                zapisanaGra = new SaveGame(this.textBox.Text.Length == 0 ? nazwa : this.textBox.Text, (sbyte)new Random().Next(0, 100), tok);
                miastoInfo.Add(zapisanaGra);

                //to może lecieć w tle - zrobić tylko sprawdzenie czy zapisze

            }

            zapisanaGra.DataCzas = DateTime.Now;

            gSilnik.ZatrzymajLogike();

            //Tutaj zrobić zatrzymanie gry

            if(await Stale.Zapisz(stanGry, plik))
            {
                var sorted = miastoInfo.ListaStanowGier.OrderByDescending(a =>
                {
                    return a.DataCzas;
                    
                });

                miastoInfo.ListaStanowGier = new System.Collections.ObjectModel.ObservableCollection<SaveGame>(sorted);

                await Stale.Zapisz(KampanieLadowanie.listaMiast, KampanieLadowanie.plikUstawien);//to może lecieć w tle - zrobić tylko sprawdzenie czy zapisze
                await new Windows.UI.Popups.MessageDialog("Gra zapisana").ShowAsync();
                gSilnik.WznowLogike();
            }
            else
            {
                await new Windows.UI.Popups.MessageDialog("Błąd podczas zapisywania.").ShowAsync();
            }



        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            //this.Frame.Content = new MainPage2();
        }

       
       

        #region NavigationHelper registration
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }


        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

       
    }
}
