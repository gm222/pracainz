﻿using ProjektInzynierGra.Klasy.Game;
using Struktury;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using ProjektInzynierGra.Klasy;
using ProjektInzynierGra.Klasy.Budynki;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Formatki
{
    public sealed partial class Budowa : UserControl
    {

        public Budowa()
        {
            this.InitializeComponent();
        }

        public Budowa(object obj) : this()
        {
            this.lLista.DataContext = (new FormatkiDynamiczne((obj as object[]), this, new Type[]
                {
                    //typeof(ProjektInzynierGra.Klasy.Testowe.ATestowa),
                    typeof(DomekMieszkalny),
                    typeof(Straz)
                })).ListaObiektow;
        }
    }
}


            /*
            for(Int16 i = 0; i < 2; i++)
            {
                var o = Stale.UtworzObrazek(50, 50, Stale.PobierzObrazekDomkuFormatka(i));
                o.Tag = i;
                o.PointerPressed += PointerPressed;
                lLista.Items.Add(o);
            }

            
            //chyba mozna wywalic uzgodnic z Mariuszem

            //var dMieszkalnyImage    =   Stale.UtworzObrazek(50, 50, "ms-appx:///Assets/img/domek1.png");
            //var dWojskowyImage      =   Stale.UtworzObrazek(50, 50, "ms-appx:///Assets/img/domek2.png");

            //dMieszkalnyImage.Tag    = 0;
            //dWojskowyImage.Tag      = 1;

            //dMieszkalnyImage.PointerPressed +=  PointerPressed;
            //dWojskowyImage.PointerPressed   +=  PointerPressed;

            //mozna wyrzucic
            //dMieszkalnyImage.PointerPressed += dMieszkalnyImage_PointerPressed; 
            //dWojskowyImage.PointerPressed   += dWojskowyImage_PointerPressed;
            
            //lLista.Items.Add(dMieszkalnyImage);
            //lLista.Items.Add(dWojskowyImage);
        }
        private async void PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            var butt = e.GetCurrentPoint(this);

            if(!butt.Properties.IsLeftButtonPressed)
                return;

            try
            {
                id = Convert.ToInt16((sender as Image).Tag);
            }
            catch(Exception)
            {
                id = -1;
                return;
            }

            obj2[1] = false;
            obj2[2] = parametry = App.gameConstans.obrazkiWlasciwosciTabela[id];

            cContentPlotna.Visibility = Visibility.Visible;

            Window.Current.Content.PointerReleased += PointerReleased; //globalny pointer do zdarzenia opuszczenia elementow gui

            var obrazekObiektu = Stale.UtworzObrazek(Stale.PobierzObrazekDomkuFormatka(id), 0);

            var pos = Window.Current.CoreWindow.PointerPosition;

            obrazekObiektu.RenderTransform = new CompositeTransform() { TranslateX = pos.X, TranslateY = pos.Y, Rotation = 45 };

            await funkcja(obrazekObiektu);
        }
        private async void PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            e.Handled = true;

            cContentPlotna.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            cContentPlotna.Children.Clear();

            if((bool)obj2[1] == true)
            {
                var element = obj2[0] as UIElement;
                var pos = e.GetCurrentPoint(cPlansza).Position;

                Stale.WlasciwoscObrazkaComposit(ref element, pos, parametry);
                cPlansza.Children.Add(element);
            }//tutaj zrobic odpowiednie dodawanie do danej list w GameEngine, zrobic to przez switch(id)

            id      = -1;
            obj2[0] = null;
            obj2[1] = false;

            Window.Current.Content.PointerReleased -= PointerReleased;
            await Task.Delay(100); //albo dac 100
        }



        //mozna wyrzucic
        private async void dWojskowyImage_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            var butt = e.GetCurrentPoint(this);

            if(!butt.Properties.IsLeftButtonPressed)
                return;

            obj2[1] = false;
            obj2[2] = parametry = App.gameConstans.obrazkiWlasciwosciTabela[1];

            cContentPlotna.Visibility = Visibility.Visible;

            Window.Current.Content.PointerReleased += PointerReleased; //globalny pointer do zdarzenia opuszczenia elementow gui

            var obrazekObiektu = Stale.UtworzObrazek("ms-appx:///Assets/img/domek2.png", 0);

            var pos = Window.Current.CoreWindow.PointerPosition;

            obrazekObiektu.RenderTransform = new CompositeTransform() { TranslateX = pos.X, TranslateY = pos.Y, Rotation = 45 };

            await funkcja(obrazekObiektu);
        }
        #region stare
        //private async void dMieszkalnyImage_PointerReleased(object sender, PointerRoutedEventArgs e)
        //{
        //    e.Handled = true;

        //    cContentPlotna.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        //    cContentPlotna.Children.Clear();

        //    if((bool)obj2[1] == true)
        //    {
        //        var element     =   obj2[0] as UIElement;
        //        var pos         =   e.GetCurrentPoint(cPlansza).Position;

        //        Stale.WlasciwoscObrazkaComposit(ref element, pos, parametry);
        //        cPlansza.Children.Add(element);
        //    }

        //    obj2[0] = null;
        //    obj2[1] = false;

        //    Window.Current.Content.PointerReleased -= dMieszkalnyImage_PointerReleased;
        //    await Task.Delay(1); //albo dac 100
        //}
        #endregion
        private async void dMieszkalnyImage_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            var butt = e.GetCurrentPoint(this);
            
            if(!butt.Properties.IsLeftButtonPressed)
                return;

            obj2[1] = false;
            obj2[2] = parametry = App.gameConstans.obrazkiWlasciwosciTabela[0];

            cContentPlotna.Visibility = Visibility.Visible;

            Window.Current.Content.PointerReleased += PointerReleased; //globalny pointer do zdarzenia opuszczenia elementow gui

            var obrazekObiektu = Stale.UtworzObrazek("ms-appx:///Assets/img/domek1.png", 0);

            var pos = Window.Current.CoreWindow.PointerPosition;
            
            obrazekObiektu.RenderTransform = new CompositeTransform() { TranslateX = pos.X, TranslateY = pos.Y, Rotation = 45 };

            await funkcja(obrazekObiektu);

        }

    }
}
*/