﻿using ProjektInzynierGra.Klasy;
using ProjektInzynierGra.Klasy.Budynki;
using ProjektInzynierGra.Klasy.Game;
using ProjektInzynierGra.Klasy.Game.AlgorytmyFunckje;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

namespace Formatki
{
    public sealed partial class Informacje : Page
    {
        #region Members of Informacje (14)

        FunkcjeXAML mainpageParametry;
        FormatkiDynamiczne formatkaD;
        GameEngine gSilnik;
        List<Image> nowoDodaneObrazki = new List<Image>();

        bool budynekUsun = false;
        bool drogaUsun = false;
        bool drogaDodaj = false;

        bool[, ,] drogi;
        bool nowaDrogaPressed = false;
        Point start;
        Point endPoint;
        int[][] tymczasowaDroga;
        Image[] obrazkiDrogi = new Image[0];
        ObservableCollection<Budynek> listaBudynkowDoUsuniecia = new ObservableCollection<Budynek>();

        #endregion Members of Informacje (14)

        #region Constructors of Informacje (1)

        public Informacje()
        {
            this.InitializeComponent();
            EfektZakonczenia.Completed += EfektZakonczenia_Completed;
            this.lListaUsunietychBudynkow.ItemsSource = listaBudynkowDoUsuniecia;
        }

        #endregion Constructors of Informacje (1)

        #region Methods of Informacje (13)

        private void BudynekUsuniecie_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            var img = (sender as Rectangle);
            var s = img.DataContext as Budynek;
            img.Opacity = 0;

            this.listaBudynkowDoUsuniecia.Add(s);
        }

        private void DodajDrogeClick(object sender, RoutedEventArgs e)
        {
            gSilnik.ZatrzymajLogike();
            nowaDrogaPressed = false;

            nowoDodaneObrazki = new List<Image>();
            drogi = new bool[2, Stale.IloscPol, Stale.IloscPol];
            for(int i = 0; i < Stale.IloscPol; i++)
                for(int j = 0; j < Stale.IloscPol; j++)
                    if(GameEngine.ukladMapy[1][i][j] > 0)
                    {
                        drogi[0, i, j] = true;
                        drogi[1, i, j] = true;
                    }

            WygladDodawaniaDrogi.Visibility = Visibility.Visible;
            EfektKlikniecia.Begin();

            mainpageParametry.NowaDrogaCanvas.Visibility = Windows.UI.Xaml.Visibility.Visible;
            Window.Current.Content.PointerPressed += NowaDrogaCanvas_PointerPressed;
            Window.Current.Content.PointerMoved += NowaDrogaCanvas_PointerMoved;
            Window.Current.Content.PointerReleased += NowaDrogaCanvas_PointerReleased;

            drogaDodaj = true;

        }

        void EfektZakonczenia_Completed(object sender, object e)
        {
            WygladDodawaniaDrogi.Visibility = Visibility.Collapsed;
            WygladUsuwaniaDrogi.Visibility = Visibility.Collapsed;
            WygladUsuwaniaBudynku.Visibility = Visibility.Collapsed;
            gSilnik.WznowLogike();
            drogaUsun = false;
            drogaDodaj = false;
            budynekUsun = false;
        }

        private void GuzikAkceptacji_Click(object sender, RoutedEventArgs e)
        {
            if (drogaDodaj == true)
            {
                for (int i = 0; i < Stale.IloscPol; i++)
                    for (int j = 0; j < Stale.IloscPol; j++)
                        if (drogi[0, i, j] != drogi[1, i, j])
                            GameEngine.ukladMapy[1][i][j] = 1;
                foreach (var obrazek in nowoDodaneObrazki)
                    mainpageParametry.DrogaCanvas.Children.Add(new Windows.UI.Xaml.Controls.Image() { Width = obrazek.Width, Height = obrazek.Height, Source = obrazek.Source, RenderTransform = obrazek.RenderTransform });

                mainpageParametry.NowaDrogaCanvas.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
                mainpageParametry.NowaDrogaCanvas.Children.Clear();

                Window.Current.Content.PointerPressed -= NowaDrogaCanvas_PointerPressed;
                Window.Current.Content.PointerMoved -= NowaDrogaCanvas_PointerMoved;
                Window.Current.Content.PointerReleased -= NowaDrogaCanvas_PointerReleased;
            }
            if(budynekUsun == true)
            {
                foreach(var item in mainpageParametry.BudynkiCanvas.Children)
                {
                    item.PointerPressed -= BudynekUsuniecie_PointerPressed;
                }
                for(int i = 0; i < listaBudynkowDoUsuniecia.Count; i++)
                {
                    listaBudynkowDoUsuniecia[i].UsunBudynek();
                    listaBudynkowDoUsuniecia[i] = null;
                }

                this.listaBudynkowDoUsuniecia.Clear();


            }

            drogaDodaj = false;
            budynekUsun = false;
            drogaUsun = false;
            EfektZakonczenia.Begin();
        }

        private void GuzikRezygnacji_Click(object sender, RoutedEventArgs e)
        {
            if(drogaDodaj == true)
            {
                mainpageParametry.NowaDrogaCanvas.Visibility = Visibility.Collapsed;
                mainpageParametry.NowaDrogaCanvas.Children.Clear();

                Window.Current.Content.PointerPressed -= NowaDrogaCanvas_PointerPressed;
                Window.Current.Content.PointerMoved -= NowaDrogaCanvas_PointerMoved;
                Window.Current.Content.PointerReleased -= NowaDrogaCanvas_PointerReleased;
            }
            if(budynekUsun == true)
            {
                foreach(var item in mainpageParametry.BudynkiCanvas.Children)
                {
                    item.PointerPressed -= BudynekUsuniecie_PointerPressed;
                }
                
                this.listaBudynkowDoUsuniecia.Clear();
                

            }
            drogaDodaj = false;
            budynekUsun = false;
            drogaUsun = false;

            EfektZakonczenia.Begin();
        }

        void NowaDrogaCanvas_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            if (nowaDrogaPressed == true)
            {
                var asd = e.GetCurrentPoint(mainpageParametry.NowaDrogaCanvas).Position;
                if (asd.X < mainpageParametry.NowaDrogaCanvas.Width && asd.X > 0 && asd.Y < mainpageParametry.NowaDrogaCanvas.Height && asd.Y > 0 && 
                    endPoint != new Point(Convert.ToInt16(asd.X) / Stale.SzerokoscPola, Convert.ToInt16(asd.Y) / Stale.SzerokoscPola) && GameEngine.obiektyNaMapie[(int)asd.X/Stale.SzerokoscPola][(int)asd.Y/Stale.SzerokoscPola] == null)
                {
                    endPoint = new Point(Convert.ToInt16(asd.X) / Stale.SzerokoscPola, Convert.ToInt16(asd.Y) / Stale.SzerokoscPola);
                    var tym = Algorytmy.BFSDroga(start, endPoint);
                    List<Image> obrazkiTymczasowe = new List<Image>();
                    for (int i = 0; i < tym.Length; i++) //sprawdzenie wszystkich elementów w nowej liście
                    {
                        bool istnieje = false;
                        for (int j = 0; j < tymczasowaDroga.Length; j++) //przeszukanie czy już nie istnieją w starej liście
                            if (obrazkiDrogi[j] != null && tym[i][0] == tymczasowaDroga[j][0] && tym[i][1] == tymczasowaDroga[j][1]) //porównanie nowych z starymi
                            {
                                obrazkiTymczasowe.Add(obrazkiDrogi[j]); // przeniesienie istniejącej referencji do nowej drogi
                                obrazkiDrogi[j] = null; // usunięcie starej referencji
                                istnieje = true;
                                break;
                            }
                        if (istnieje == true)
                            continue;
                        Image obrazek = new Windows.UI.Xaml.Controls.Image() { Width = Stale.SzerokoscPola + 1, Height = Stale.SzerokoscPola + 1, Source = new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri("ms-appx:///Assets/img/droga.png")) };
                        obrazek.RenderTransform = new CompositeTransform() { TranslateX = tym[i][0] * Stale.SzerokoscPola, TranslateY = tym[i][1] * Stale.SzerokoscPola };
                        mainpageParametry.NowaDrogaCanvas.Children.Add(obrazek);
                        obrazkiTymczasowe.Add(obrazek);

                    }
                    for (int i = 0; i < obrazkiDrogi.Length; i++)
                        mainpageParametry.NowaDrogaCanvas.Children.Remove(obrazkiDrogi[i]);
                    obrazkiDrogi = obrazkiTymczasowe.ToArray();
                    tymczasowaDroga = tym;
                }
            }
        }

        void NowaDrogaCanvas_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            var asd = e.GetCurrentPoint(mainpageParametry.NowaDrogaCanvas).Position;
            if (asd.X < mainpageParametry.NowaDrogaCanvas.Width && asd.X > 0 && asd.Y < mainpageParametry.NowaDrogaCanvas.Height && asd.Y > 0 && GameEngine.obiektyNaMapie[(int)asd.X/Stale.SzerokoscPola][(int)asd.Y/Stale.SzerokoscPola] == null)
            {
                nowaDrogaPressed = true;
                endPoint = start = new Point(Convert.ToInt16(asd.X) / Stale.SzerokoscPola, Convert.ToInt16(asd.Y) / Stale.SzerokoscPola);
                drogi[0, (int)start.X, (int)start.Y] = true;

                Image obrazek = new Windows.UI.Xaml.Controls.Image() { Width = Stale.SzerokoscPola + 1, Height = Stale.SzerokoscPola + 1, Source = new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri("ms-appx:///Assets/img/droga.png")) };
                obrazek.RenderTransform = new CompositeTransform() { TranslateX = start.X * Stale.SzerokoscPola, TranslateY = start.Y * Stale.SzerokoscPola };
                mainpageParametry.NowaDrogaCanvas.Children.Add(obrazek);

                tymczasowaDroga = new int[][] { new int[] { (int)start.X, (int)start.Y } };
                obrazkiDrogi = new Image[] { obrazek };
            }
        }

        void NowaDrogaCanvas_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            if (nowaDrogaPressed == true)
            {
                for (int i = 0; i < obrazkiDrogi.Length; i++)
                    nowoDodaneObrazki.Add(obrazkiDrogi[i]);

                nowaDrogaPressed = false;
                for (int i = 0; i < tymczasowaDroga.Length; i++)
                    drogi[0, tymczasowaDroga[i][0], tymczasowaDroga[i][1]] = true;
            }

        }

        protected override async void OnNavigatedFrom(NavigationEventArgs e)
        {
            mainpageParametry.NowaDrogaCanvas.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            mainpageParametry.NowaDrogaCanvas.PointerPressed -= NowaDrogaCanvas_PointerPressed;
            mainpageParametry.NowaDrogaCanvas.PointerMoved -= NowaDrogaCanvas_PointerMoved;
            mainpageParametry.NowaDrogaCanvas.PointerReleased -= NowaDrogaCanvas_PointerReleased;

            foreach(var item in mainpageParametry.BudynkiCanvas.Children)
            {
                item.PointerPressed -= BudynekUsuniecie_PointerPressed;
            }

            gSilnik.WznowLogike();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            var obj = e.Parameter as object[];
            this.mainpageParametry = obj[0] as FunkcjeXAML;
            gSilnik = obj[1] as GameEngine;
        }

        private void UsunBudynekClick(object sender, RoutedEventArgs e)
        {
            gSilnik.ZatrzymajLogike();
            WygladUsuwaniaBudynku.Visibility = Visibility.Visible;
            EfektKlikniecia.Begin();
            budynekUsun = true;

            foreach(var item in mainpageParametry.BudynkiCanvas.Children)
            {
                item.PointerPressed += BudynekUsuniecie_PointerPressed;
            }
        }

        private void UsunDrogeClick(object sender, RoutedEventArgs e)
        {
            gSilnik.ZatrzymajLogike();
            WygladUsuwaniaDrogi.Visibility = Visibility.Visible;
            EfektKlikniecia.Begin();
            drogaUsun = true;
        }

        private void UsuniecieZListyUsunBudynek_Click(object sender, RoutedEventArgs e)
        {
            var obiekt = (sender as Button).DataContext as Budynek;
            obiekt.obrazek.Opacity = 255;
            this.listaBudynkowDoUsuniecia.Remove(obiekt);
            
        }

        #endregion Methods of Informacje (13)
    }
}
