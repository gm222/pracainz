﻿using ProjektInzynierGra.Klasy.Game;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace ProjektInzynierGra.Klasy.Postacie
{
    public class Chorzy : PracownikMiasta
    {
    }

    public class DostawcaWody : PracownikMiasta
    {
    }

    public class Fiskus : PracownikMiasta
    {
    }

    public class Handlarz : PracownikMiasta
    {
        #region Members of Handlarz (2)

        private Boolean rodzajZasobu;
        private Int16 ilosc;

        #endregion Members of Handlarz (2)
    }

    public class Medyk : PracownikMiasta
    {
    }

    [DataContract]
    [KnownType(typeof(Strazak))]
    public abstract class PracownikMiasta : Postac
    {
        #region Members of PracownikMiasta (5)

        internal int[][] droga;
        internal Int16 maxOdleglosc;
        internal Point budynekPozycja;
        internal Budynki.Budynek budynekRodzic;
        internal GameEngine.DodajZadanieHandler zadanieEventGlobal;

        #endregion Members of PracownikMiasta (5)
    }

    public class Rebeliant : PracownikMiasta
    {
        #region Members of Rebeliant (1)

        private Int16 atak;

        #endregion Members of Rebeliant (1)
    }
    [DataContract]
    [KnownType(typeof(Strazak))]
    public class Strazak : PracownikMiasta
    {
        #region Members of Strazak (5)
        [DataMember]
        private Boolean zapasWody;
        [DataMember]
        private bool czyObserwacyjny = false;
        [DataMember]
        private int krok = 0;
        [DataMember]
        byte krokPosredni = 0;
        [DataMember]
        bool powrot = false;

        #endregion Members of Strazak (5)

        #region Constructors of Strazak (2)
        public override void PrzywrocObrazek()
        {
            base.PrzywrocObrazek();
            if(czyObserwacyjny)
            {
                this.obrazek = Stale.UtworzObrazek2(50, 50, "ms-appx:///Assets/img/postac.png", 0, this.obrazekCT);
            }
            else
            {
                this.obrazek = Stale.UtworzObrazek2(50, 50, "ms-appx:///Assets/img/postac.png", 0, this.obrazekCT);
            }
            this.obrazekIB = this.obrazek.Fill as ImageBrush;
            Stale.UstawPozycjeXYZ(obrazek, this.pozycja);
            GameEngine.mainPageFunkcje.BudynkiCanvas.Children.Add(obrazek);
        }
        public Strazak()
        {
            zapasWody = true;
            czyWidoczny = true;
            this.obrazekCT = new CompositeTransform()
            {
                TranslateX = 59 * 50,
                TranslateY = 59 * 50,
                CenterX = .5f,
                CenterY = .5f
            };
            if(czyObserwacyjny)//obserwacyjny
            {
                this.obrazek = Stale.UtworzObrazek2(50, 50, "ms-appx:///Assets/img/postac.png", 0, this.obrazekCT);
            }
            else
            {//strazak
                this.obrazek = Stale.UtworzObrazek2(50, 50, "ms-appx:///Assets/img/postac.png", 0, this.obrazekCT);
            }

            this.obrazekIB = this.obrazek.Fill as ImageBrush;
            if(this.obrazek!=null)
                GameEngine.mainPageFunkcje.BudynkiCanvas.Children.Add(this.obrazek); // postacie sa dodawane do plotna gdzie sa budynki - w przyszlosci zastapic to czyms innym 

        }

        public Strazak(params object[] obiekt)
        {
            this.budynekRodzic = obiekt[0] as Budynki.Straz;
            this.zadanieEventGlobal = obiekt[1] as GameEngine.DodajZadanieHandler;
            this.droga = obiekt[2] as int[][];
            this.pozycja = (Point)obiekt[3];
            this.czyObserwacyjny = (bool)obiekt[4];

            if(czyObserwacyjny == true) //czy jest to strazak czy strazak obserwacyjny
            //true - strazak obserwacyjny
            {
                this.obrazek = Stale.UtworzObrazek2(50, 50, "ms-appx:///Assets/img/postac.png", 0, this.obrazekCT);
                zadanieEventGlobal(SprawdzajStanBudynkow, this, this.id, 0, true);
                zadanieEventGlobal(Idz, this, this.id, 0, true);
                //tutaj funkcje dla strazaka obserwacyjnego
            }
            else
            {
                this.obrazek = Stale.UtworzObrazek2(50, 50, "ms-appx:///Assets/img/postac.png", 0, this.obrazekCT);
            }
            this.obrazekIB = this.obrazek.Fill as ImageBrush;
            GameEngine.mainPageFunkcje.BudynkiCanvas.Children.Add(this.obrazek);
        }

        #endregion Constructors of Strazak (2)

        #region Methods of Strazak (2)

        public void Idz()
        {
            switch(powrot)
            {
                case false:
                    if(krokPosredni < 4 && krok + 1 < droga.Length)
                    {
                        pozycja.X = droga[krok][0] * 50 + ((droga[krok + 1][0] - droga[krok][0]) * 12.5 * krokPosredni);
                        pozycja.Y = droga[krok][1] * 50 + ((droga[krok + 1][1] - droga[krok][1]) * 12.5 * krokPosredni);
                        zadanieEventGlobal(Idz, this, this.id, 0, true);
                        krokPosredni++;
                    }
                    else if(krok + 1 < droga.Length)
                    {
                        pozycja.X = droga[++krok][0] * 50;
                        pozycja.Y = droga[krok][1] * 50;

                        SprawdzajStanBudynkow();
                        zadanieEventGlobal(Idz, this, this.id, 0, true);
                        krokPosredni = 0;
                    }
                    else
                    { 
                        //nie patrzymy na budynek

                        powrot = true;
                        krokPosredni = 3;
                        zadanieEventGlobal(Idz, this, this.id, 0, true);
                    }
                break;
                case true:
                if(krokPosredni > 0 && krok - 1 > 0)
                {
                    pozycja.X = droga[krok][0] * 50 + ((droga[krok - 1][0] - droga[krok][0]) * 12.5 * krokPosredni);
                    pozycja.Y = droga[krok][1] * 50 + ((droga[krok - 1][1] - droga[krok][1]) * 12.5 * krokPosredni);
                    zadanieEventGlobal(Idz, this, this.id, 0, true);
                    krokPosredni++;
                }
                else if(krok - 1 > 0)
                {
                    pozycja.X = droga[--krok][0] * 50;
                    pozycja.Y = droga[krok][1] * 50;

                    SprawdzajStanBudynkow();
                    zadanieEventGlobal(Idz, this, this.id, 0, true);
                    krokPosredni = 3;
                }
                else
                {

                }
                break;
            }

            Stale.UstawPozycjeXYZPostaci(this.obrazek, pozycja);
        }

        private void SprawdzajStanBudynkow()
        {
            int xWartosc, yWartosc;

            if(krok + 1 <= droga.Length)
            {
                xWartosc = droga[krok][0];
                yWartosc = droga[krok][1];

                if(GameEngine.obiektyNaMapie[xWartosc - 1][yWartosc] != null)
                        GameEngine.obiektyNaMapie[xWartosc - 1][yWartosc].hp = GameEngine.obiektyNaMapie[xWartosc - 1][yWartosc].maxHP;
                if(GameEngine.obiektyNaMapie[xWartosc + 1][yWartosc] != null)
                        GameEngine.obiektyNaMapie[xWartosc + 1][yWartosc].hp = GameEngine.obiektyNaMapie[xWartosc + 1][yWartosc].maxHP;
                if(GameEngine.obiektyNaMapie[xWartosc][yWartosc - 1] != null)
                    GameEngine.obiektyNaMapie[xWartosc][yWartosc - 1].hp = GameEngine.obiektyNaMapie[xWartosc][yWartosc - 1].maxHP;
                if(GameEngine.obiektyNaMapie[xWartosc][yWartosc + 1] != null)
                    GameEngine.obiektyNaMapie[xWartosc][yWartosc + 1].hp = GameEngine.obiektyNaMapie[xWartosc][yWartosc + 1].maxHP;
            }
            else
            {
                int x = droga[krok + 1][0] - droga[krok][0];
                int y = droga[krok + 1][1] - droga[krok][1];

                xWartosc = droga[krok][0];
                yWartosc = droga[krok][1];

                if (x != 0)
                {
                    if (x == -1)
                    {
                        if (GameEngine.obiektyNaMapie[xWartosc - 1][yWartosc] != null)
                            GameEngine.obiektyNaMapie[xWartosc - 1][yWartosc].hp = GameEngine.obiektyNaMapie[xWartosc - 1][yWartosc].maxHP;
                    }
                    else
                        if (GameEngine.obiektyNaMapie[xWartosc + 1][yWartosc] != null)
                            GameEngine.obiektyNaMapie[xWartosc + 1][yWartosc].hp = GameEngine.obiektyNaMapie[xWartosc + 1][yWartosc].maxHP;

                    if (GameEngine.obiektyNaMapie[xWartosc][yWartosc - 1] != null)
                        GameEngine.obiektyNaMapie[xWartosc][yWartosc - 1].hp = GameEngine.obiektyNaMapie[xWartosc][yWartosc - 1].maxHP;
                    if (GameEngine.obiektyNaMapie[xWartosc][yWartosc + 1] != null)
                        GameEngine.obiektyNaMapie[xWartosc][yWartosc + 1].hp = GameEngine.obiektyNaMapie[xWartosc][yWartosc + 1].maxHP;
                }
                else
                {
                    if (y == -1)
                    {
                        if (GameEngine.obiektyNaMapie[xWartosc][yWartosc - 1] != null)
                            GameEngine.obiektyNaMapie[xWartosc][yWartosc - 1].hp = GameEngine.obiektyNaMapie[xWartosc][yWartosc - 1].maxHP;
                    }
                    else
                        if (GameEngine.obiektyNaMapie[xWartosc][yWartosc + 1] != null)
                            GameEngine.obiektyNaMapie[xWartosc][yWartosc + 1].hp = GameEngine.obiektyNaMapie[xWartosc][yWartosc + 1].maxHP;

                    if (GameEngine.obiektyNaMapie[xWartosc - 1][yWartosc] != null)
                        GameEngine.obiektyNaMapie[xWartosc - 1][yWartosc].hp = GameEngine.obiektyNaMapie[xWartosc - 1][yWartosc].maxHP;
                    if (GameEngine.obiektyNaMapie[xWartosc + 1][yWartosc] != null)
                        GameEngine.obiektyNaMapie[xWartosc + 1][yWartosc].hp = GameEngine.obiektyNaMapie[xWartosc + 1][yWartosc].maxHP;
                }
            }
        }

        #endregion Methods of Strazak (2)
    }

    public class Straznik : PracownikMiasta
    {
        #region Members of Straznik (1)

        private Int16 atak;

        #endregion Members of Straznik (1)
    }
}
