﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;


namespace ProjektInzynierGra.Klasy.Game
{
    public class GameSettings
    {
        public ObrazkiWlasciwosci[] obrazkiWlasciwosciTabela = new ObrazkiWlasciwosci[]{};
        Kampania[] kampanie;
        string error;
        List<string> miasta = new List<string>()
        {
            "PierwszeMiasto",
            "DrugieMiasto",
            "TrzecieMiasto",
            "CzwarteMiasto",
            "PiąteMiasto",
            "SzósteMiasto"
        };

        

        public GameSettings()
        {

        }

        public GameSettings(string e) : this()
        {
            this.error = e;
            
        }

        public async void InicializujeAnimacje()
        {
            var assetsFolder     = (await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFoldersAsync()).ToList().First(a => a.DisplayName.Equals("Assets"));

            var imgFolder        = await assetsFolder.GetFolderAsync("img");
            var itemsOfimgFolder = await imgFolder.GetFilesAsync();
            Stale.listaAnimacji  = new ImageBrush[itemsOfimgFolder.Count];

            for(int i = 0; i < itemsOfimgFolder.Count; i++)
            {
                var it = itemsOfimgFolder[i];

                using(var s = await it.OpenAsync(FileAccessMode.Read))
                {
                    BitmapImage bi = new BitmapImage();

                    await bi.SetSourceAsync(s);

                    Stale.listaAnimacji[i] = new ImageBrush()
                    {
                        ImageSource = bi,
                        AlignmentX  = AlignmentX.Left,
                        AlignmentY  = AlignmentY.Top,
                        Stretch     = Stretch.None,

                        RelativeTransform = new CompositeTransform()
                        {
                            TranslateX = -1,
                            TranslateY = -1,
                            CenterX    = .5f,
                            CenterY    = .5f,
                            ScaleX     = 1.0f,
                            ScaleY     = 1.0f
                        }

                    };
                }
            }
        }
        

        private async Task<bool> Odczyt(Uri url) //dodac odczyt gdzie robilem zapisywanie, jest to funkcja uniwersalna
        {
            try
            {
                using(IInputStream fileStream = await (await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(url)).OpenReadAsync())
                {
                    var sessionSerializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(Int16[][]));

                    using(var x = fileStream.AsStreamForRead())
                    {
                        var listobjects = sessionSerializer.ReadObject(x) as Int16[][];
                        int j = 0;

                        obrazkiWlasciwosciTabela = listobjects.Select((u,i) => new ObrazkiWlasciwosci(u[0], u[1], i)).ToArray();

                        await x.FlushAsync();
                    }
                }
            }
            catch(Exception)
            {
                return false;
            }
            
            return true;
        }

        public async Task<bool> LoadSettings()
        {

            var localFolder = ApplicationData.Current.LocalFolder;

            StorageFile plikGraf = null;

            try
            {
                plikGraf = await localFolder.GetFileAsync("graf");
            }
            catch(Exception)
            {
            }
            

            if(plikGraf == null)
            {
                //tutaj stworzyc strukture
            }

            //tutaj załadować strukturę

            if(! await Odczyt(new Uri("ms-appx:///Assets/ustawienia/obrazki.json")))
            {
                return false;
            }
            
            return true;
        }



    }


    
}
