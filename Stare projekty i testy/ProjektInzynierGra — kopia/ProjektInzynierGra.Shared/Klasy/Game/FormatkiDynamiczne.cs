﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

namespace ProjektInzynierGra.Klasy.Game
{
    public class FormatkiDynamiczne
    {
        FunkcjeXAML mainPageParametry;
        ObrazkiWlasciwosci parametry;
        Int16 id = 0;
        GameEngine gSilnik;
        private List<Rectangle> lLista = new List<Rectangle>();
        UIElement formatka;
        Type TypFormatkiZKtorejPochodzi;
        Type wyborKlasy;
        bool moznaBudowac = false;
      
        SolidColorBrush[] color = new SolidColorBrush[] { new SolidColorBrush(Windows.UI.Color.FromArgb(150, 255, 0, 0)), new SolidColorBrush(Windows.UI.Color.FromArgb(150, 0, 255, 0)) };

        public List<Rectangle> ListaObiektow{ get { return lLista; } }
       
        public FormatkiDynamiczne(object[] obiekt, UIElement form, Type[] typy)
        {
            this.mainPageParametry = obiekt[0] as FunkcjeXAML;
            this.gSilnik           = obiekt[1] as GameEngine;
            this.formatka          = form;

            TypFormatkiZKtorejPochodzi = this.formatka.GetType();

            var dane = Stale.daneFormatki[TypFormatkiZKtorejPochodzi];

            var miejsce = dane[0];
            var ilosc = dane[1];

            for(byte i = miejsce, j = 0; j < ilosc; i++, j++)
            {
                var comp = new CompositeTransform()
                {
                    TranslateX = 1,
                    TranslateY = 0
                };
                var o = Stale.UtworzObrazek2(50, 50, Stale.PobierzObrazekDomkuFormatka(i), 0, comp);
                o.Tag = j;
                o.DataContext = typy[j];
                //if(TypFormatkiZKtorejPochodzi == typeof(Forma))
                o.PointerPressed += PointerPressed; //zmienic to
                lLista.Add(o);
            }
        }

        private void PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            var butt = e.GetCurrentPoint(formatka);

            if (!butt.Properties.IsLeftButtonPressed)
                return;

            try
            {
                id = Convert.ToInt16((sender as Rectangle).Tag);
                wyborKlasy = (sender as Rectangle).DataContext as Type;
            }
            catch (Exception)
            {
                id = -1;
                wyborKlasy = null;
                return;
            }

            gSilnik.ZatrzymajLogike();

            moznaBudowac = false;
            parametry    = App.gameConstans.obrazkiWlasciwosciTabela[id];

            mainPageParametry.ContentPlotnaCanvas.Visibility = Visibility.Visible;
            mainPageParametry.MyszkaContentCanvas.PointerMoved  += BudynkiPointerMoved;
            Window.Current.Content.PointerReleased              += BudynkiPointerReleased;
            var comp = new CompositeTransform()
            {
                TranslateX = 0
            };
            obrazekObiektu                 = Stale.UtworzObrazek2(parametry.X * 50, parametry.Y * 50, Stale.PobierzObrazekDomkuFormatka(id), 0, comp);
            
            GridObrazka = new Grid();
            tymCanv     = new Canvas() 
            { 
                Background          = new SolidColorBrush(Windows.UI.Color.FromArgb(150, 255, 0, 0)), 
                HorizontalAlignment = HorizontalAlignment.Stretch, 
                VerticalAlignment   = VerticalAlignment.Stretch 
            };

            GridObrazka.Children.Add(tymCanv);
            GridObrazka.Children.Add(obrazekObiektu);

            Canvas.SetZIndex(GridObrazka.Children[0], 2);
            Canvas.SetZIndex(GridObrazka.Children[1], 0);

            var pos = Window.Current.CoreWindow.PointerPosition;

            GridObrazka.RenderTransform = new CompositeTransform() { TranslateX = pos.X, TranslateY = pos.Y, Rotation = 45 };

            mainPageParametry.ContentPlotnaCanvas.Children.Add(GridObrazka);
        }

        Grid GridObrazka = null;
        Canvas tymCanv = null;
        Rectangle obrazekObiektu = null;

        double pierwiastekDwa = Math.Sqrt(2);

        private void BudynkiPointerMoved(object sender, PointerRoutedEventArgs e)
        {
            var cContentPlotna = mainPageParametry.ContentPlotnaCanvas;
            var cPlansza = mainPageParametry.PlanszaCanvas;

            var pos       = e.GetCurrentPoint(cContentPlotna).Position;
            var translate = GridObrazka.RenderTransform as CompositeTransform;

            if(cContentPlotna.Width - pos.X < 375 || (e.GetCurrentPoint(cPlansza).Position.X < 0 || e.GetCurrentPoint(cPlansza).Position.Y < 0 || e.GetCurrentPoint(cPlansza).Position.X > cPlansza.Width || e.GetCurrentPoint(cPlansza).Position.Y > cPlansza.Height))
            {
                moznaBudowac       = false;
                tymCanv.Background = color[0];

                translate.TranslateX = pos.X;
                translate.TranslateY = pos.Y - (obrazekObiektu.ActualHeight * pierwiastekDwa / 2);
            }
            else
            {
                Point pozycja = new Point((int)e.GetCurrentPoint(cPlansza).Position.X / Stale.SzerokoscPola, (int)e.GetCurrentPoint(cPlansza).Position.Y / Stale.SzerokoscPola);

                bool pozycjaZajeta = false;
                for(int i = (int)pozycja.X; i < (int)pozycja.X + parametry.X; i++)
                    for(int j = (int)pozycja.Y; j < (int)pozycja.Y + parametry.Y; j++)
                        if(GameEngine.obiektyNaMapie[i][j] != null || GameEngine.ukladMapy[1][i][j]>0)
                            pozycjaZajeta = true;

                if(pozycjaZajeta == true)
                {
                    (GridObrazka.Children[0] as Canvas).Background = color[0];
                    moznaBudowac = false;
                }
                else
                {
                    (GridObrazka.Children[0] as Canvas).Background = color[1];
                    moznaBudowac = true;
                }

                double h = (50 * pierwiastekDwa);
                translate.TranslateY = ((pozycja.X + pozycja.Y) / 2) * h + (cPlansza.RenderTransform as CompositeTransform).TranslateY;
                translate.TranslateX = (3000 / 50 - (pozycja.Y - pozycja.X)) / 2 * h + (cPlansza.RenderTransform as CompositeTransform).TranslateX;

            }
        }

        private async void BudynkiPointerReleased(object sender, PointerRoutedEventArgs e)
        {
            mainPageParametry.MyszkaContentCanvas.PointerMoved -= BudynkiPointerMoved;
            
            e.Handled = true;

            var cContentPlotna = mainPageParametry.ContentPlotnaCanvas;
            var cPlansza = mainPageParametry.PlanszaCanvas;

            cContentPlotna.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            cContentPlotna.Children.RemoveAt(1);
            
            if(moznaBudowac == true)
            {
                var pos       = e.GetCurrentPoint(cPlansza).Position;
                Point pozycja = new Point((int)pos.X / 50, (int)pos.Y / 50);

                

                //, gSilnik.UsunBudynekZPlanszy, gSilnik.UsunZadanie, gSilnik.ListaOsobPracujacych, gSilnik.ListaOsobNiePracujacych
                Budynki.Budynek budynek = Activator.CreateInstance(wyborKlasy, pos, parametry, gSilnik.dodajZadanie ) as Budynki.Budynek;

                if(budynek == null)
                    throw new ArgumentNullException("budynek");

                budynek.DodajDane(gSilnik.dodajZadanie, gSilnik.dodajPostacie, gSilnik.usunBudynek, gSilnik.usunZadanie, gSilnik.usunPostac, gSilnik.ListaOsobNiePracujacych, gSilnik.ListaOsobPracujacych, gSilnik.pobierzPostac);

                gSilnik.dodajBudynki(budynek);

                for(int i = (int)pozycja.X; i < (int)pozycja.X + parametry.X; i++)
                    for(int j = (int)pozycja.Y; j < (int)pozycja.Y + parametry.Y; j++)
                    {
                        GameEngine.obiektyNaMapie[i][j] = budynek;
                    }

                //gSilnik.dodajBudynki(budynek);

                GridObrazka.Children.Clear();
                //var obrazek = obrazekObiektu as UIElement;
                //Stale.WlasciwoscObrazkaComposit(ref obrazek, pos);
                //cPlansza.Children.Add(obrazekObiektu); //tutaj dodać Obiekt a nie obrazek globalny -.- 
            }//tutaj zrobic odpowiednie dodawanie do danej list w GameEngine, zrobic to przez switch(id)

            id = -1;
            moznaBudowac = false;
            GridObrazka = null;

            Window.Current.Content.PointerReleased -= BudynkiPointerReleased;

            gSilnik.WznowLogike();
        }
      
    }
}