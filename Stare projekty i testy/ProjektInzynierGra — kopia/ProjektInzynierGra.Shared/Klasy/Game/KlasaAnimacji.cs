﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;

namespace ProjektInzynierGra.Klasy.Game
{
    public class Animacja : IComparable<Animacja>
    {
        internal IEnumerator<bool> animacjaEnum;
        internal TimeSpan nastepneWywolanie;
        internal TimeSpan czas;
        internal Action renderAkcja;
        internal bool wykonany;

        internal void RenderujJesliWykonano()
        {
            if(!wykonany)
                return;

            wykonany = false;

            if(renderAkcja != null)
                renderAkcja();
        }

        public int CompareTo(Animacja innaAnimacja)
        {
            return nastepneWywolanie.CompareTo(innaAnimacja.nastepneWywolanie);
        }
    }

    public static class SilnikAnimacji
    {
        private static readonly TimeSpan sekunda = TimeSpan.FromMilliseconds(1000);
        private static readonly List<Animacja> listaAnimacji = new List<Animacja>();
        private static readonly Stopwatch stoper = new Stopwatch();
        private static readonly List<Animacja> listaAnimacjiDoWykonania = new List<Animacja>();
        private static readonly List<Animacja> listaAnimacjiDoUsuniecia = new List<Animacja>();
        private static readonly EventHandler<object> uchwytRenderEvent = Render;

        public static Animacja DodajAnimacje(IEnumerator<bool> animacjaL, TimeSpan czas)
        {
            return DodajAnimacje(animacjaL, czas, null);
        }

        public static void UsunAnimacje(Animacja animacjaL)
        {
            listaAnimacjiDoUsuniecia.Add(animacjaL);
        }

        public static Animacja DodajAnimacje(IEnumerator<bool> animacjaL, TimeSpan czas, Action render)
        {
            if(animacjaL == null)
                throw new ArgumentNullException("animacja");

            if(czas <= TimeSpan.Zero)
                throw new ArgumentException("Czas musi być różny od zera!");

            var animacja = new Animacja();

            animacja.animacjaEnum      = animacjaL;
            animacja.czas              = czas;
            animacja.nastepneWywolanie = stoper.Elapsed + czas;
            animacja.renderAkcja       = render;

            listaAnimacji.Add(animacja);

            if(listaAnimacji.Count == 1) //odpalany zaraz po dodaniu pierwszej animacji; nie tworzony event bez potrzeby
            {
                stoper.Start();
                CompositionTarget.Rendering += uchwytRenderEvent;
            }

            return animacja;
        }
        private static void UsunListeAnimacji()
        {
            var theSet = new HashSet<Animacja>(listaAnimacjiDoUsuniecia);
            listaAnimacji.RemoveAll(item => theSet.Contains(item));
        }
        private static void Render(object sender, object e)
        {
            var teraz = stoper.Elapsed;
            var minLimit = teraz - sekunda;

            listaAnimacjiDoWykonania.Clear();
            UsunListeAnimacji();

            listaAnimacjiDoWykonania.AddRange(listaAnimacji);

            while(listaAnimacjiDoWykonania.Count > 0)
            {
                if(listaAnimacjiDoWykonania.Count > 1) //sortujemy animacje po priorytecie czasu wykonania
                    listaAnimacjiDoWykonania.Sort();

                TimeSpan dodatkowyCzas = teraz - listaAnimacjiDoWykonania[0].nastepneWywolanie;

                int indexAnimacjiDoWykonania = 0;
                while(indexAnimacjiDoWykonania < listaAnimacjiDoWykonania.Count)
                {
                    var animacja = listaAnimacjiDoWykonania[indexAnimacjiDoWykonania];

                    if(animacja == null)
                    {
                        indexAnimacjiDoWykonania++;
                        continue;
                    }
                    
                    var animacjaE = animacja.animacjaEnum; //enumeracja animacji - kolejna "tablica"/stan
                    var nastepneWykonanieAnimacji = animacja.nastepneWywolanie;

                    if(nastepneWykonanieAnimacji > teraz) //jezeli animacja jest w przyszlosci(szybciej wykonana) to kazda ktora po niej przychodzi tez jest, nastepnie lista animacji jest sortowana
                    {
                        int count = listaAnimacjiDoWykonania.Count;

                        for(int i = indexAnimacjiDoWykonania; i < count; i++)
                            listaAnimacjiDoWykonania[i].RenderujJesliWykonano();

                        listaAnimacjiDoWykonania.RemoveRange(indexAnimacjiDoWykonania, listaAnimacjiDoWykonania.Count - indexAnimacjiDoWykonania);

                        continue;
                    }

                    if(!animacjaE.MoveNext())
                    {
                        animacjaE.Dispose();

                        listaAnimacjiDoWykonania.RemoveAt(indexAnimacjiDoWykonania);
                        listaAnimacji.Remove(animacja);
                        animacja.RenderujJesliWykonano();

                        if(listaAnimacji.Count == 0)
                        {
                            stoper.Stop();
                            CompositionTarget.Rendering -= uchwytRenderEvent; //usuwamy event jeśli nie ma żadnej animacji
                            return;
                        }

                        continue;
                    }

                    animacja.wykonany = true;
                    
                    if(nastepneWykonanieAnimacji < minLimit) //jesli przekroczyl limit czasu do wykonania
                    {
                        listaAnimacjiDoWykonania.RemoveAt(indexAnimacjiDoWykonania);
                        animacja.RenderujJesliWykonano();

                        animacja.nastepneWywolanie = nastepneWykonanieAnimacji + animacja.czas + dodatkowyCzas;
                        continue;
                    }

                    animacja.nastepneWywolanie = nastepneWykonanieAnimacji + animacja.czas;
                    indexAnimacjiDoWykonania++;
                }
            }
        }
    }
}