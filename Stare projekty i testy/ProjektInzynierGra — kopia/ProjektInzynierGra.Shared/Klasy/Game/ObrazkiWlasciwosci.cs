﻿using System;

namespace ProjektInzynierGra.Klasy.Game
{
    public class ObrazkiWlasciwosci
    {
        private Int16 x;
        private Int16 y;
        private int index;

        public int Index
        {
            get { return index; }
            set { index = value; }
        }
        

        public Int16 X
        {
            get { return this.x; }
            private set { this.x = value; }
        }

        public Int16 Y
        {
            get { return this.y; }
            private set { this.y = value; }
        }

        public ObrazkiWlasciwosci(Int16 x, Int16 y)
        {
            this.X = x;
            this.Y = y;
        }

        public ObrazkiWlasciwosci(Int16 x, Int16 y, int _index)
        {
            this.X = x;
            this.Y = y;
            this.Index = _index;
        }
    }
}