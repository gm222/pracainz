﻿using System;

namespace ProjektInzynierGra.Klasy.Game
{
    public class ObrazkiWlasciwosci
    {
        byte iloscZajetychPolX, iloscZajetychPolY; //obiekty na mapie
        Int16 wielkoscObrazkaX, wielkoscObrazkaY;
        Int16 przesuniecieObrazkaX, przesuniecieObrazkaY;



        public byte IloscZajetychPolX     { get { return this.iloscZajetychPolX;    } }
        public byte IloscZajetychPolY     { get { return this.iloscZajetychPolY;    } }
        public Int16 WielkoscObrazkaX     { get { return this.wielkoscObrazkaX;     } }
        public Int16 WielkoscObrazkaY     { get { return this.wielkoscObrazkaY;     } }
        public Int16 PrzesuniecieObrazkaX { get { return this.przesuniecieObrazkaX; } }
        public Int16 PrzesuniecieObrazkaY { get { return this.przesuniecieObrazkaY; } }

        public ObrazkiWlasciwosci(byte izpX, byte izpY, Int16 poX, Int16 poY, Int16 woX, Int16 woY)
        {
            this.iloscZajetychPolX    = izpX;
            this.iloscZajetychPolY    = izpY;
            this.przesuniecieObrazkaX = poX;
            this.przesuniecieObrazkaY = poY;
            this.wielkoscObrazkaX     = woX;
            this.wielkoscObrazkaY     = woY;
        }

    }
}