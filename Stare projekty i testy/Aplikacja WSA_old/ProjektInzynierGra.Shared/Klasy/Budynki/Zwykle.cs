﻿using ProjektInzynierGra.Klasy.Game;
using ProjektInzynierGra.Klasy.Game.AlgorytmyFunckje;
using ProjektInzynierGra.Klasy.Postacie;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Windows.Foundation;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Shapes;

namespace ProjektInzynierGra.Klasy.Budynki
{
    public class BramaMiasta : Zwykle
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }
    [DataContract]
    [KnownType(typeof(DomekMieszkalny))]
    
    public abstract class BudynkiMieszkalne : Zwykle
    {
        #region Members of BudynkiMieszkalne (5)

        internal Boolean typZasobow;
        internal Int16 iloscZasobow;
        internal Int16 czasOdwiedzenieStrazy;
        internal Int16 czasOdwiedzeniaMedyka;
        internal Int16 czasOdwiedzeniaWodnika;

        #endregion Members of BudynkiMieszkalne (5)

        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }
    [DataContract]
    public class DomekMieszkalny : BudynkiMieszkalne
    {
        #region Members of DomekMieszkalny (7)
        List<Postacie.Postac> listaMieszkancowPrzychodzacych;
        List<Postacie.Postac> listaMieszkancow;
        [DataMember]
        List<ulong> listaIDMieszkancow;
        [DataMember]
        List<ulong> listaIDMieszkancowPrzychodzacych;

        #endregion Members of DomekMieszkalny (7)

        #region Constructors of DomekMieszkalny (2)
        public override void PrzywrocObrazek()
        {
            base.PrzywrocObrazek();

            obrazekCT.TranslateX = -2;

            var parametry = App.gameConstans.obrazkiWlasciwosciTabela[idWlasciwosci];
            this.obrazek = Stale.UtworzObrazek3(parametry.WielkoscObrazkaX, parametry.WielkoscObrazkaY, Stale.PobierzObrazekDomkuFormatka(idWlasciwosci), 0, this.obrazekCT);
            this.obrazekIB = this.obrazek.Fill as ImageBrush;

            Stale.UstawPozycjeXYZ(obrazek, this.lokalizacja, parametry);
            GameEngine.mainPageFunkcje.BudynkiCanvas.Children.Add(obrazek);
        }

        public override void PrzywrocDane()
        {
            this.listaMieszkancow = new List<Postac>();
            this.listaMieszkancowPrzychodzacych = new List<Postac>();

            foreach(var it in listaIDMieszkancowPrzychodzacych)
            {
                var postac = this.pobierzPostac(it);
                this.listaMieszkancowPrzychodzacych.Add(postac);
            }

            foreach(var it in listaIDMieszkancow)
            {
                var postac = this.pobierzPostac(it);
                this.listaMieszkancow.Add(postac);
            }
        }

        public DomekMieszkalny()
        {
            this.hp               = 100;
            this.maxHP            = 100;
            this.maxPojemnoscOsob = 10;

            this.listaIDMieszkancowPrzychodzacych = new List<ulong>();
            this.listaIDMieszkancow               = new List<ulong>();
            this.listaMieszkancowPrzychodzacych   = new List<Postacie.Postac>();
            this.listaMieszkancow                 = new List<Postacie.Postac>(maxPojemnoscOsob);
        }

        public DomekMieszkalny(params object[] obj) : this()
        {
            var pos                = (Point)obj[0];
            var wl                 = obj[1] as ObrazkiWlasciwosci;
            this.dodajZadanieEvent = obj[2] as GameEngine.DodajZadanieHandler;
            this.iloscZajetychPol  = new Point(wl.IloscZajetychPolX, wl.IloscZajetychPolY);
            this.lokalizacja       = pos;
            this.idWlasciwosci = Convert.ToByte(obj[3]);

            this.dodajZadanieEvent(StworzMieszkancaPrzychodzacego, this, this.id, 10); //można tutaj 

        }

        #endregion Constructors of DomekMieszkalny (2)

        #region Methods of DomekMieszkalny (2)

        public override void UsunBudynek() 
        {
            Postac[] tymPost = new Postac[listaMieszkancow.Count];
            listaMieszkancow.CopyTo(tymPost);
            listaMieszkancow.Clear();
            listaMieszkancow = null;

            for(int i = 0; i < tymPost.Length; i++) //pamietac o obiektach do usuniecia z listy zadan !!!!!!!!!!!!!!!!
            {
                tymPost[i].UsunReferencje();
                tymPost[i] = null;
            }

            for(int i = 0; i < listaMieszkancowPrzychodzacych.Count; i++)
            {
                listaMieszkancowPrzychodzacych[i].UsunReferencje();
                GameEngine.mainPageFunkcje.BudynkiCanvas.Children.Remove(listaMieszkancowPrzychodzacych[i].obrazek);
                listaMieszkancowPrzychodzacych[i] = null;
            }

            this.listaIDMieszkancow.Clear();
            this.listaIDMieszkancowPrzychodzacych.Clear();

            listaMieszkancowPrzychodzacych.Clear();
            listaMieszkancowPrzychodzacych = null;

            tymPost = null;
            usunZadanieEvent(this);
            usunBudynekEvent(this);
        }

        public void PrzeniesienieMieszkanca(Postacie.MieszkaniecPrzychodzacy mp)
        {
            //zmienić wygląd domu
            hpLost = 1;
            mp.czyWidoczny = false;
            this.obrazekCT.TranslateX = -4;
            //this.obrazekIB.ImageSource = new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri("ms-appx:///Assets/img/2.png")); //
            listaMieszkancowPrzychodzacych.Remove(mp);
            listaMieszkancow.Add(mp);
            this.listaNiepracujacych.Add(mp);
            this.listaIDMieszkancowPrzychodzacych.Remove(mp.id);
            this.listaIDMieszkancow.Add(mp.id);

            for(byte i = 0; i < mp.iloscLudzi - 1; i++)
            {
                var p = new Postacie.Mieszkaniec();
                this.dodajPostacEvent(p);
                listaMieszkancow.Add(p);
                this.listaIDMieszkancow.Add(p.id);
                this.listaNiepracujacych.Add(p);
            }
        }

        private async void StworzMieszkancaPrzychodzacego()
        {
            var tym = Algorytmy.AStar(new Point(59, 59), new Point((int)lokalizacja.X / Stale.SzerokoscPola, (int)lokalizacja.Y / Stale.SzerokoscPola), (this as Budynek));
            if(tym == null)
            {
                System.Diagnostics.Debug.WriteLine("Brak drogi");
                //await new MessageDialog("dsfdsfSD").ShowAsync();
                return;
            }

            var mp = new Postacie.MieszkaniecPrzychodzacy(tym, this.lokalizacja, this.dodajZadanieEvent, this, (byte)(maxPojemnoscOsob - listaMieszkancow.Count));
            mp.domekRodzinnyID = this.id;
            mp.DodajDane(this.dodajZadanieEvent, this.usunZadanieEvent, this.usunPostacEvent, this.pobierzZUkladuMapy);
            listaMieszkancowPrzychodzacych.Add(mp);
            this.dodajPostacEvent(mp);
            this.listaIDMieszkancowPrzychodzacych.Add(mp.id);

        }

        #endregion Methods of DomekMieszkalny (2)
    }

    public class DomekWojskowy : BudynkiMieszkalne
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class MorObronny : Zwykle
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class Palac : Zwykle
    {
        #region Members of Palac (2)

        private Int16 iloscZlota;
        private Int16 ludnosc;

        #endregion Members of Palac (2)

        #region Properties of Palac (2)

        public Int16 IloscZlota
        {
            get { return this.iloscZlota; }
            set { this.iloscZlota = value; }
        }

        public Int16 Ludnosc
        {
            get { return this.ludnosc; }
            set { this.ludnosc = value; }
        }

        #endregion Properties of Palac (2)

        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class Podjum : Zwykle
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }
    [DataContract]
    [KnownType(typeof(Strazak))]
    
    public class Straz : Zwykle
    {
        /*
         
        * Odwołanie do GameEngine by przenieść osoby pracujące czyli liste postaci do listy osób niepracujących
        * Usuniecie siebie samego z listy budynków
        * usunąć strażaka jeżeli jest
        * 
         
        */


        #region Members of Straz (2)
        [DataMember]
        Postacie.Strazak strazakObserwacyjny;
        [DataMember]
        bool straznikWolny = true;
        [DataMember]
        List<Postacie.Strazak> listaStrazakow;
        List<Postacie.Postac> listaPracownikow;
        [DataMember]
        List<ulong> listaIDPracownikow;
        [DataMember]
        List<int[][]> droga;
        [DataMember]
        bool zmianaDrogi = false;
        [DataMember]
        byte aktualnaDroga = 0;

        #endregion Members of Straz (2)

        #region Properties of Straz (2)

        public int PracownicyPrzyjeci
        {
            get
            {
                return listaPracownikow.Count;
            }
        }

        public bool StraznikWolny
        {
            get { return this.straznikWolny; }
            set { this.straznikWolny = value; }
        }

        #endregion Properties of Straz (2)

        #region Constructors of Straz (2)
  
        public Straz()
        {
            this.maxPojemnoscOsob = 15;
            this.maxWysOsob = 1;

            this.listaIDPracownikow = new List<ulong>();
            this.listaPracownikow   = new List<Postacie.Postac>(base.maxPojemnoscOsob);
            this.listaStrazakow     = new List<Postacie.Strazak>(base.maxPojemnoscOsob);
            
            
            

        }

       // Budynki.Budynek budynek = Activator.CreateInstance(wyborKlasy, 0pos, 1parametry) as Budynki.Budynek;

        public Straz(params object[] obj)
            : this()
        {
            var pos                  = (Point)obj[0];
            var wl                   = obj[1] as ObrazkiWlasciwosci;
            this.dodajZadanieEvent   = obj[2] as GameEngine.DodajZadanieHandler;
            this.iloscZajetychPol    = new Point(wl.IloscZajetychPolX, wl.IloscZajetychPolY);
            this.lokalizacja         = pos;
            this.idWlasciwosci       = Convert.ToByte(obj[3]);

            


            this.dodajZadanieEvent(this.UzupelnijPracownikow, this, this.id);
        }

        #endregion Constructors of Straz (2)

      
        public override void PrzywrocDane()
        {
            this.listaPracownikow = new List<Postac>();

            foreach(var it in this.listaIDPracownikow)
            {
                var postac = this.pobierzPostac(it);
                this.listaPracownikow.Add(postac);
            }
        }
        public override void PrzywrocObrazek()
        {
            base.PrzywrocObrazek();
            this.obrazek = Stale.UtworzObrazek2(100, 100, "ms-appx:///Assets/img/1.png", 0, this.obrazekCT);
            this.obrazekIB = this.obrazek.Fill as ImageBrush;
            Stale.UstawPozycjeXYZ(obrazek, this.lokalizacja, null);
            GameEngine.mainPageFunkcje.BudynkiCanvas.Children.Add(obrazek);
        }
        public override void UsunBudynek() 
        {
            foreach(var item in listaPracownikow)
            {
                this.usunPostacEvent(item, 2);
            }//tutaj dodac usuwanie zadan poszczegolnych obiektow np. strazaka obserwacyjnego i strazaka

            listaPracownikow.Clear();
            listaIDPracownikow.Clear();
            this.strazakObserwacyjny = null;  //dodac usuwanie jego zadan
            listaPracownikow = null;
        }
        public override void UsunPostac(Postac post) 
        {
            this.listaPracownikow.Remove(post);
            this.listaIDPracownikow.Remove(post.id);
            this.usunPostacEvent(post);
            this.dodajZadanieEvent(this.UzupelnijPracownikow, this, this.id, 2);
            //tutaj dodanie zadania szukania pracownikow w liscie osób niepracujacych w celu zatrudnienia
        }
        private void SprawdzZmianeDrogi()
        {
            if(strazakObserwacyjny != null)
            {
                var droga_tym = Algorytmy.BFS_Obserwacja(5, new Point((int)(this.lokalizacja.X / 50), (int)(this.lokalizacja.Y / 50)));
                
                if(droga == null)
                {
                    droga = droga_tym;
                }
                else
                {
                    foreach(var it in droga)
                    {
                        foreach(var it2 in droga_tym)
                        {
                            if(it.Length == it2.Length)
                            {
                                for(int i = 0; i < it.Length; i++)
                                {
                                    if(it[i][0] != it2[i][0] || it[i][1] != it2[i][1])
                                    {
                                        droga = droga_tym;
                                        zmianaDrogi = true;
                                        aktualnaDroga = 0;
                                        break;
                                    }
                                    
                                }
                                if(zmianaDrogi)
                                    break;
                            }
                        }
                        if(zmianaDrogi)
                            break;
                    }
                }
                this.dodajZadanieEvent(SprawdzZmianeDrogi, this, this.id, 80);
                
            }
            
        }
        private void DodajStrazakaObserwacyjnego()
        {
            if(strazakObserwacyjny == null && PracownicyPrzyjeci == 10)
            {
                droga = Algorytmy.BFS_Obserwacja(10, new Point((int)(this.lokalizacja.X / Stale.SzerokoscPola), (int)(this.lokalizacja.Y / Stale.SzerokoscPola)));
                if (droga == null || droga.Count == 0)
                {
                    this.dodajZadanieEvent(DodajStrazakaObserwacyjnego, this, this.id, 3);
                    return;
                }
                this.strazakObserwacyjny = new Postacie.Strazak(this, this.dodajZadanieEvent, droga[0], lokalizacja, true);
                

                this.dodajZadanieEvent(SprawdzZmianeDrogi, this, this.id, 80);
            }
            else
                this.dodajZadanieEvent(DodajStrazakaObserwacyjnego, this, this.id, 3);
        }

        private void UzupelnijPracownikow()
        {
            if(PracownicyPrzyjeci < base.maxPojemnoscOsob)
            {
                if(this.listaNiepracujacych.Count == 0)
                {
                    this.dodajZadanieEvent(UzupelnijPracownikow, this, this.id);
                    return;
                }

                var min = Math.Min(this.listaNiepracujacych.Count, (int)base.maxPojemnoscOsob - PracownicyPrzyjeci);

                for(int i = 0; i < min; i++)
                {
                    listaPracownikow.Add(this.listaNiepracujacych[0]);
                    this.listaIDPracownikow.Add(listaPracownikow[listaPracownikow.Count - 1].id);
                    this.listaPracujacych.Add(this.listaNiepracujacych[0]);
                    this.listaNiepracujacych.RemoveAt(0);
                }

                if(PracownicyPrzyjeci < base.maxPojemnoscOsob)
                {
                    this.dodajZadanieEvent(UzupelnijPracownikow, this, this.id);
                }

                if(PracownicyPrzyjeci == 10)
                {
                    this.dodajZadanieEvent(DodajStrazakaObserwacyjnego, this, this.id);
                }
            }


        }

        public int[][] PobierzNastepnaDroga()
        {
            if(droga != null)
            {
                if(++aktualnaDroga < droga.Count)
                {
                    return droga[aktualnaDroga];
                }
                else
                {
                    aktualnaDroga = 0;
                    return droga[aktualnaDroga];
                }
            }
            else
                return null;
        }
    }

    public class StrazMiejska : Zwykle
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class Szkola : Zwykle
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class SzkolaAktorska : Zwykle
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class Szpital : Zwykle
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class Teatr : Zwykle
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class UrzadGminy : Zwykle
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class ZrodloWody : Zwykle
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }
    [DataContract]
    
    public abstract class Zwykle : Budynek
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }
}
