﻿using ProjektInzynierGra.Klasy.Budynki;
using ProjektInzynierGra.Klasy.Game;
using ProjektInzynierGra.Klasy.Game.AlgorytmyFunckje;
using System;
using System.Runtime.Serialization;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace ProjektInzynierGra.Klasy.Postacie
{
    [DataContract]
    [KnownType(typeof(MieszkaniecPrzychodzacy))]
    [KnownType(typeof(DomekMieszkalny))]
    public class Mieszkaniec : Postac
    {
        internal Point miejsceDocelowe;



        
    }

    public class MieszkaniecOdchodzacy : Mieszkaniec
    {
       [DataMember]
        int[][] droga;
        [DataMember]
        int krok = 0;
        [DataMember]
        byte krokPosredni = 0;
        [DataMember]
        public byte iloscLudzi = 0;
        public override void PrzywrocObrazek()
        {
            base.PrzywrocObrazek();

            this.obrazek = Stale.UtworzObrazek2(50, 50, "ms-appx:///Assets/img/postac.png", 0, this.obrazekCT);
            this.obrazekIB = this.obrazek.Fill as ImageBrush;
            Stale.UstawPozycjeXYZ(obrazek, this.pozycja);
            if(this.czyWidoczny)
                GameEngine.mainPageFunkcje.BudynkiCanvas.Children.Add(obrazek);
        }
        public MieszkaniecOdchodzacy(int[][] sciezka, Point lok, Game.GameEngine.DodajZadanieHandler zadanieEventGlobal, DomekMieszkalny dom, byte ilosc)
        {
            // TODO: Complete member initialization

            this.obrazekCT = new CompositeTransform()
            {
                TranslateX = 0,
                TranslateY = 0,
                CenterX    = .5f,
                CenterY    = .5f
            };
            this.czyWidoczny = true;
            this.iloscLudzi = ilosc;
            this.domekRodzinny = dom;
            this.droga = sciezka;
            this.pozycja = lok;
            this.dodajZadanieEvent = zadanieEventGlobal;
            this.obrazek = Stale.UtworzObrazek2(50, 50, "ms-appx:///Assets/img/postac.png", 0, this.obrazekCT);

            Stale.UstawPozycjeXYZPostaci(obrazek, new Point(59 * 50, 59 * 50));
            GameEngine.mainPageFunkcje.BudynkiCanvas.Children.Add(obrazek);
            //dodajZadanieEvent(IdzDoDomu, this, this.id, 0, true);
        }

        private bool ZmianaTrasy()
        {
            if(GameEngine.obiektyNaMapie[droga[krok + 1][0]][droga[krok + 1][1]] != null && GameEngine.obiektyNaMapie[droga[krok + 1][0]][droga[krok + 1][1]] != domekRodzinny)
            {
                droga = Algorytmy.AStar(new Point(this.pozycja.X / Stale.SzerokoscPola, this.pozycja.Y / Stale.SzerokoscPola), new Point(this.domekRodzinny.lokalizacja.X / Stale.SzerokoscPola, this.domekRodzinny.lokalizacja.Y / Stale.SzerokoscPola), domekRodzinny);
                krok = 0;
                krokPosredni = 0;
                //this.dodajZadanieEvent(IdzDoDomu, this, this.id, 0, true);
                return true;
            }
            return false;
        }

        private void WyjdzZMiasta()
        {

            if(krokPosredni<4 && krok+1 < droga.Length)
            {
                if(ZmianaTrasy() == true)
                    return;
                pozycja.X = droga[krok][0] * 50 + ((droga[krok + 1][0] - droga[krok][0]) * 12.5 * krokPosredni);
                pozycja.Y = droga[krok][1] * 50 + ((droga[krok + 1][1] - droga[krok][1]) * 12.5 * krokPosredni);
                
               // this.dodajZadanieEvent(IdzDoDomu, this, this.id, 0, true);
                krokPosredni++;
            }
            else if(krok+1 < droga.Length)
            {
                if(ZmianaTrasy() == true)
                    return;
                pozycja.X = droga[++krok][0] * 50;
                pozycja.Y = droga[krok][1] * 50;
                //this.dodajZadanieEvent(IdzDoDomu, this, this.id, 0, true);
                krokPosredni = 0;
            }
            else
            {
                czyWidoczny = false;
                GameEngine.mainPageFunkcje.BudynkiCanvas.Children.Remove(obrazek);
                //(this.domekRodzinny as DomekMieszkalny).PrzeniesienieMieszkanca(this);
                return;
            }

            Stale.UstawPozycjeXYZPostaci(this.obrazek, pozycja);
        }
    }
    [DataContract]
    [KnownType(typeof(DomekMieszkalny))]
    public class MieszkaniecPrzychodzacy : Mieszkaniec
    {
        [DataMember]
        int[][] droga;
        [DataMember]
        int krok = 0;
        [DataMember]
        byte krokPosredni = 0;
        [DataMember]
        public byte iloscLudzi = 0;
        public override void PrzywrocObrazek()
        {
            base.PrzywrocObrazek();

            this.obrazek = Stale.UtworzObrazek2(50, 50, "ms-appx:///Assets/img/postac.png", 0, this.obrazekCT);
            this.obrazekIB = this.obrazek.Fill as ImageBrush;
            Stale.UstawPozycjeXYZPostaci(obrazek, this.pozycja);
            if(this.czyWidoczny)
                GameEngine.mainPageFunkcje.BudynkiCanvas.Children.Add(obrazek);
        }
        public MieszkaniecPrzychodzacy(int[][] sciezka, Point lok, Game.GameEngine.DodajZadanieHandler zadanieEventGlobal, DomekMieszkalny dom, byte ilosc)
        {
            // TODO: Complete member initialization

            this.obrazekCT = new CompositeTransform()
            {
                TranslateX = 0,
                TranslateY = 0,
                CenterX    = .5f,
                CenterY    = .5f
            };
            this.czyWidoczny = true;
            this.iloscLudzi = ilosc;
            this.domekRodzinny = dom;
            this.droga = sciezka;
            this.pozycja = lok;
            this.dodajZadanieEvent = zadanieEventGlobal;
            this.obrazek = Stale.UtworzObrazek2(50, 50, "ms-appx:///Assets/img/postac.png", 0, this.obrazekCT);

            Stale.UstawPozycjeXYZPostaci(obrazek, new Point(59 * 50, 59 * 50));
            GameEngine.mainPageFunkcje.BudynkiCanvas.Children.Add(obrazek);
            dodajZadanieEvent(IdzDoDomu, this, this.id, 0, true);
        }

        private bool ZmianaTrasy()
        {
            if(GameEngine.obiektyNaMapie[droga[krok + 1][0]][droga[krok + 1][1]] != null && GameEngine.obiektyNaMapie[droga[krok + 1][0]][droga[krok + 1][1]] != domekRodzinny)
            {
                droga = Algorytmy.AStar(new Point(this.pozycja.X / Stale.SzerokoscPola, this.pozycja.Y / Stale.SzerokoscPola), new Point(this.domekRodzinny.lokalizacja.X / Stale.SzerokoscPola, this.domekRodzinny.lokalizacja.Y / Stale.SzerokoscPola), domekRodzinny);
                krok = 0;
                krokPosredni = 0;
                this.dodajZadanieEvent(IdzDoDomu, this, this.id, 0, true);
                return true;
            }
            return false;
        }

        private void IdzDoDomu()
        {

            if(krokPosredni<4 && krok+1 < droga.Length)
            {
                if(ZmianaTrasy() == true)
                    return;
                pozycja.X = droga[krok][0] * 50 + ((droga[krok + 1][0] - droga[krok][0]) * 12.5 * krokPosredni);
                pozycja.Y = droga[krok][1] * 50 + ((droga[krok + 1][1] - droga[krok][1]) * 12.5 * krokPosredni);
                
                this.dodajZadanieEvent(IdzDoDomu, this, this.id, 0, true);
                krokPosredni++;
            }
            else if(krok+1 < droga.Length)
            {
                if(ZmianaTrasy() == true)
                    return;
                pozycja.X = droga[++krok][0] * 50;
                pozycja.Y = droga[krok][1] * 50;
                this.dodajZadanieEvent(IdzDoDomu, this, this.id, 0, true);
                krokPosredni = 0;
            }
            else
            {
                czyWidoczny = false;
                GameEngine.mainPageFunkcje.BudynkiCanvas.Children.Remove(obrazek);
                if(domekRodzinny == null)
                {
                    this.usunZadanieEvent(this);
                    this.UsunReferencje();
                    this.usunPostacEvent(this);
                }
                (this.domekRodzinny as DomekMieszkalny).PrzeniesienieMieszkanca(this);
                return;
            }

            Stale.UstawPozycjeXYZPostaci(this.obrazek, pozycja);
        }

        


    }
}