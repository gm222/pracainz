﻿
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

namespace ProjektInzynierGra.Klasy.Game
{
    public class FormatkiDynamiczne
    {
        FunkcjeXAML mainPageParametry;
        ObrazkiWlasciwosci parametry;
        Int16 id = 0;
        GameEngine gSilnik;
        private List<Rectangle> lLista = new List<Rectangle>();
        UIElement formatka;
        Type TypFormatkiZKtorejPochodzi;
        Type wyborKlasy;
        bool moznaBudowac = false;
        
      
        SolidColorBrush[] color = new SolidColorBrush[] { new SolidColorBrush(Windows.UI.Color.FromArgb(150, 255, 0, 0)), new SolidColorBrush(Windows.UI.Color.FromArgb(150, 0, 255, 0)) };

        public List<Rectangle> ListaObiektow{ get { return lLista; } }
       
        public FormatkiDynamiczne(object[] obiekt, UIElement form, Type[] typy)
        {
            this.mainPageParametry = obiekt[0] as FunkcjeXAML;
            this.gSilnik           = obiekt[1] as GameEngine;
            this.formatka          = form;

            TypFormatkiZKtorejPochodzi = this.formatka.GetType();
            
            var dane = Stale.daneFormatki[TypFormatkiZKtorejPochodzi];

            var miejsce = dane[0];
            var ilosc = dane[1];

            for(byte i = miejsce, j = 0; j < ilosc; i++, j++)
            {
                var comp = new CompositeTransform()
                {
                    TranslateX = 0,
                    TranslateY = 0
                };
                var o = Stale.UtworzObrazek3(Stale.SzerokoscPola, Stale.SzerokoscPola, Stale.PobierzObrazekDomkuFormatka(i), 0, comp);
                o.Tag = i;
                o.DataContext = typy[j];
                //if(TypFormatkiZKtorejPochodzi == typeof(Forma))
                o.PointerPressed += PointerPressed; //zmienic to
                lLista.Add(o);
            }
        }

        private void PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            var butt = e.GetCurrentPoint(formatka);

            if (!butt.Properties.IsLeftButtonPressed)
                return;

            try
            {
                id = Convert.ToInt16((sender as Rectangle).Tag);
                wyborKlasy = (sender as Rectangle).DataContext as Type;
            }
            catch (Exception)
            {
                id = -1;
                wyborKlasy = null;
                return;
            }

            gSilnik.ZatrzymajLogike();

            moznaBudowac = false;
            parametry    = App.gameConstans.obrazkiWlasciwosciTabela[id];

            mainPageParametry.ContentPlotnaCanvas.Visibility    = Visibility.Visible;
            Window.Current.Content.PointerMoved                 += BudynkiPointerMoved;
            Window.Current.Content.PointerReleased              += BudynkiPointerReleased;
            var comp = new CompositeTransform()
            {
                TranslateX = -1,
                TranslateY = 0
            };
            obrazekObiektu = Stale.UtworzObrazek3(parametry.WielkoscObrazkaX, parametry.WielkoscObrazkaY, Stale.PobierzObrazekDomkuFormatka(id), 0, comp);

            GridObrazka = new Grid()
            {
                Opacity = 0.50
            };
            tymCanv     = new Canvas() 
            { 
                Background          = new SolidColorBrush(Windows.UI.Color.FromArgb(0, 255, 0, 0)), 
                HorizontalAlignment = HorizontalAlignment.Stretch, 
                VerticalAlignment   = VerticalAlignment.Stretch 
            };

            GridObrazka.Children.Add(tymCanv);
            GridObrazka.Children.Add(obrazekObiektu);

            Canvas.SetZIndex(GridObrazka.Children[0], 2);
            Canvas.SetZIndex(GridObrazka.Children[1], 0);

            var pos = Window.Current.CoreWindow.PointerPosition;

            GridObrazka.RenderTransform = new CompositeTransform()
            {
                TranslateX = pos.X,
                TranslateY = pos.Y - Math.Sqrt(Math.Pow(obrazekObiektu.ActualHeight - (obrazekObiektu.ActualHeight - parametry.PrzesuniecieObrazkaY) / 2, 2) +
                                                        Math.Pow(obrazekObiektu.ActualWidth - (obrazekObiektu.ActualWidth - parametry.PrzesuniecieObrazkaX) / 2, 2)),
                Rotation = 45
            };

            mainPageParametry.ContentPlotnaCanvas.Children.Add(GridObrazka);
        }

        Grid GridObrazka = null;
        Canvas tymCanv = null;
        Rectangle obrazekObiektu = null;

        double pierwiastekDwa = Math.Sqrt(2);

        private void BudynkiPointerMoved(object sender, PointerRoutedEventArgs e)
        {
            var cContentPlotna = mainPageParametry.ContentPlotnaCanvas;
            var cPlansza = mainPageParametry.PlanszaCanvas;

            var pos       = e.GetCurrentPoint(cContentPlotna).Position;
            var translate = GridObrazka.RenderTransform as CompositeTransform;

            Point pozycja = new Point((int)e.GetCurrentPoint(cPlansza).Position.X / Stale.SzerokoscPola, (int)e.GetCurrentPoint(cPlansza).Position.Y / Stale.SzerokoscPola);

            if(cContentPlotna.Width - pos.X < 375 || (e.GetCurrentPoint(cPlansza).Position.X < 0 || 
                e.GetCurrentPoint(cPlansza).Position.Y < 0 || e.GetCurrentPoint(cPlansza).Position.X > cPlansza.Width ||
                e.GetCurrentPoint(cPlansza).Position.Y > cPlansza.Height || pozycja.X + parametry.IloscZajetychPolX > Stale.IloscPol || 
                pozycja.Y + parametry.IloscZajetychPolY > Stale.IloscPol))
            {
                moznaBudowac       = false;
               // tymCanv.Background = color[0];

                (obrazekObiektu.Fill.RelativeTransform as CompositeTransform).TranslateX = -1;

                translate.TranslateX = pos.X ;
                translate.TranslateY = pos.Y - Math.Sqrt(Math.Pow(obrazekObiektu.ActualHeight - (obrazekObiektu.ActualHeight - parametry.PrzesuniecieObrazkaY) / 2, 2) +
                                                        Math.Pow(obrazekObiektu.ActualWidth - (obrazekObiektu.ActualWidth - parametry.PrzesuniecieObrazkaX) / 2, 2));
            }
            else
            {

                bool pozycjaZajeta = false;
                for(int i = (int)pozycja.X; i < (int)pozycja.X + parametry.IloscZajetychPolX; i++)
                    for(int j = (int)pozycja.Y; j < (int)pozycja.Y + parametry.IloscZajetychPolY; j++)
                        if(i>=Stale.IloscPol || j>=Stale.IloscPol || GameEngine.obiektyNaMapie[i][j] != null || gSilnik.ukladMapy[1][i][j]>0)
                            pozycjaZajeta = true;

                if(pozycjaZajeta == true)
                {
                    (obrazekObiektu.Fill.RelativeTransform as CompositeTransform).TranslateX = -1;
                    moznaBudowac = false;
                }
                else
                {
                    (obrazekObiektu.Fill.RelativeTransform as CompositeTransform).TranslateX = -2;
                    moznaBudowac = true;
                }

                Canvas.SetZIndex(GridObrazka, gSilnik.ukladMapy[0][(int)pozycja.X + parametry.IloscZajetychPolX-1][(int)pozycja.Y + parametry.IloscZajetychPolY-1]);

                double a = Math.Sqrt(Math.Pow(parametry.PrzesuniecieObrazkaX, 2) + Math.Pow(parametry.PrzesuniecieObrazkaY, 2));

                double h = 50 * Math.Sqrt(2);
                translate.TranslateX = (3000 / 50 - (pozycja.Y - pozycja.X)) / 2 * h + (cPlansza.RenderTransform as CompositeTransform).TranslateX;

                translate.TranslateY = ((pozycja.X + pozycja.Y) / 2) * h - a + (cPlansza.RenderTransform as CompositeTransform).TranslateY;
                

            }
        }

        private async void BudynkiPointerReleased(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.Content.PointerMoved -= BudynkiPointerMoved;
            
            e.Handled = true;

            var cContentPlotna = mainPageParametry.ContentPlotnaCanvas;
            var cPlansza = mainPageParametry.PlanszaCanvas;

            cContentPlotna.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            cContentPlotna.Children.RemoveAt(1);
            
            if(moznaBudowac == true)
            {
                var pos       = e.GetCurrentPoint(cPlansza).Position;
                Point pozycja = new Point((int)pos.X / Stale.SzerokoscPola, (int)pos.Y / Stale.SzerokoscPola);

                Budynki.Budynek budynek = Activator.CreateInstance(wyborKlasy, pos, parametry, gSilnik.dodajZadanie, id) as Budynki.Budynek;

                if(budynek == null)
                    throw new ArgumentNullException("budynek");

                budynek.DodajDane(gSilnik.dodajZadanie, gSilnik.dodajPostacie, gSilnik.usunBudynek, gSilnik.usunZadanie, gSilnik.usunPostac, gSilnik.ListaOsobNiePracujacych, gSilnik.ListaOsobPracujacych, gSilnik.pobierzPostac, gSilnik.pobierzZUkladuMapy);

                budynek.obrazek = obrazekObiektu;
                budynek.obrazekIB = obrazekObiektu.Fill as ImageBrush;
                budynek.obrazekCT = budynek.obrazekIB.RelativeTransform as CompositeTransform;

                if(budynek is Budynki.DomekMieszkalny)
                {
                    budynek.obrazekCT.TranslateX = -3;
                }
                else
                    budynek.obrazekCT.TranslateX = -4;

                GridObrazka.Children.Clear();


                Stale.UstawPozycjeXYZ(budynek.obrazek, pos, parametry, gSilnik.ukladMapy[0][(int)pozycja.X + parametry.IloscZajetychPolX-1][(int)pozycja.Y + parametry.IloscZajetychPolY-1]);
                //budynek.obrazekCT = new CompositeTransform()
                //{
                //    TranslateX = -3,
                //    TranslateY = 0,
                //    CenterX = .5f,
                //    CenterY = .5f
                //};

                //budynek.obrazek = Stale.UtworzObrazek3(parametry.WielkoscObrazkaX, parametry.WielkoscObrazkaY, Stale.PobierzObrazekDomkuFormatka(id), 0, budynek.obrazekCT);

                //budynek.obrazekIB = budynek.obrazek.Fill as ImageBrush;

                gSilnik.dodajBudynki(budynek);

                for(int i = (int)pozycja.X; i < (int)pozycja.X + parametry.IloscZajetychPolX; i++)
                    for(int j = (int)pozycja.Y; j < (int)pozycja.Y + parametry.IloscZajetychPolY; j++)
                    {
                        GameEngine.obiektyNaMapie[i][j] = budynek;
                    }

                //gSilnik.dodajBudynki(budynek);

                GridObrazka.Children.Clear();
                //var obrazek = obrazekObiektu as UIElement;
                //Stale.WlasciwoscObrazkaComposit(ref obrazek, pos);
                //cPlansza.Children.Add(obrazekObiektu); //tutaj dodać Obiekt a nie obrazek globalny -.- 
            }//tutaj zrobic odpowiednie dodawanie do danej list w GameEngine, zrobic to przez switch(id)

            id = -1;
            moznaBudowac = false;
            GridObrazka = null;

            Window.Current.Content.PointerReleased -= BudynkiPointerReleased;

            gSilnik.WznowLogike();
        }
      
    }
}