﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;

namespace ProjektInzynierGra.Klasy.Game
{
    class MechanizmAnimacjiObrazek//juz nie bedzie potrzebna ta klasa
    {

        WriteableBitmap[] animacja;
        private async Task<WriteableBitmap[]> PobierzObrazekAnimacji(int nr, byte x, byte y)
        {
            byte[] srcPixels;
            int width = 0;

            var uri = new System.Uri(String.Format("ms-appx:///Assets/img/{0}.png", nr));

            using(IRandomAccessStream fileStream = await (await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(uri)).OpenAsync(Windows.Storage.FileAccessMode.Read))
            {
                BitmapDecoder decoder = await BitmapDecoder.CreateAsync(fileStream);
                PixelDataProvider pixelData = await decoder.GetPixelDataAsync(
                                                        BitmapPixelFormat.Bgra8,
                                                        BitmapAlphaMode.Straight,
                                                            new BitmapTransform(),
                                                        ExifOrientationMode.IgnoreExifOrientation,
                                                        ColorManagementMode.DoNotColorManage);
                width = (int)decoder.PixelWidth;
                srcPixels = pixelData.DetachPixelData();
            }

            List<WriteableBitmap> animacjaTym = new List<WriteableBitmap>();


            for(int i = 0; i < width / (x * 20); i++)
            {
                byte[] obrazek = new byte[x * 20 * y * 20 * 4];

                for(int j = 0; j < y * 20; j++)
                    for(int k = 0; k < x * 20; k++)
                    {
                        obrazek[(j * x * 20 + k) * 4 + 0] = srcPixels[(j * width + k + i * x * 20) * 4 + 0];
                        obrazek[(j * x * 20 + k) * 4 + 1] = srcPixels[(j * width + k + i * x * 20) * 4 + 1];
                        obrazek[(j * x * 20 + k) * 4 + 2] = srcPixels[(j * width + k + i * x * 20) * 4 + 2];
                        obrazek[(j * x * 20 + k) * 4 + 3] = srcPixels[(j * width + k + i * x * 20) * 4 + 3];
                    }
                WriteableBitmap obrazekMapa = new WriteableBitmap(x * 20, y * 20); // o co chodzi z tymi 200x200

                using(Stream stream = obrazekMapa.PixelBuffer.AsStream())
                {
                    await stream.WriteAsync(obrazek, 0, obrazek.Length);
                }
                animacjaTym.Add(obrazekMapa);
            }

            return animacjaTym.ToArray(); //animacja to od 0...5 i później od 6 do np. 9, czyli jedna tablica
        }
        public async Task<bool> InicjalizacjaAnimacji()
        {

            try
            {

                int ilosc = App.gameConstans.obrazkiWlasciwosciTabela.Length;
                List<WriteableBitmap> animacjatymczasowa = new List<WriteableBitmap>();


                

                for(int i = 0; i < ilosc; i++)
                {
                    var obrazek = App.gameConstans.obrazkiWlasciwosciTabela[i];
                    var temp = await PobierzObrazekAnimacji(i, (byte)obrazek.X, (byte)obrazek.Y);
                    for(int j = 0; j < temp.Count(); j++)
                        animacjatymczasowa.Add(temp[i]);
                }

                this.animacja = animacjatymczasowa.ToArray();
                animacjatymczasowa.Clear(); animacjatymczasowa = null;
            }
            catch(Exception)
            {
                return false;
            }

            return true;

        }//end of Start Method
    }
}
