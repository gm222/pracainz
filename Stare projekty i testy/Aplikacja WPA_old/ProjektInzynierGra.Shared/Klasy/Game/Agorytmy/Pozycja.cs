﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektInzynierGra.Klasy.Game.AlgorytmyFunckje
{
    public class Pozycja
    {
        private int x;
        private int y;
        private Pozycja rodzic;
        private int? krok;

        public Pozycja(int X, int Y, Pozycja Rodzic, int? Krok=null)
        {
            x = X; y = Y; rodzic = Rodzic; krok = Krok;
        }

        public int X
        {
            get { return x; }
            set { x = value; }
        }
        public int Y
        {
            get { return y; }
            set { y = value; }
        }
        public Pozycja Rodzic
        {
            get { return rodzic; }
            set { rodzic = value; }
        }
        public int? Krok
        {
            get { return krok; }
            set { krok = value; }
        }
    }
}
