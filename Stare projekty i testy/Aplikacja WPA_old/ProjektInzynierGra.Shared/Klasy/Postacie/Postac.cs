﻿
using ProjektInzynierGra.Klasy.Budynki;
using ProjektInzynierGra.Klasy.Game;
using System;
using System.Runtime.Serialization;
using Windows.Foundation;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

namespace ProjektInzynierGra.Klasy.Postacie
{
    [DataContract]
    [KnownType(typeof(MieszkaniecPrzychodzacy))]
    [KnownType(typeof(Mieszkaniec))]
    [KnownType(typeof(PracownikMiasta))]
    [KnownType(typeof(Straz))]
    public abstract class Postac
    {
        [DataMember]
        internal ulong id;
        [DataMember]
        internal SByte maxAnimacjiX = 0;
        [DataMember]
        internal SByte maxAnimacjiY = 0;
        [DataMember]
        internal SByte animacjaX = 0;
        [DataMember]
        internal SByte animacjaY = 0;
        [DataMember]
        internal Point pozycja; //X i Y
        [IgnoreDataMember]
        internal Rectangle obrazek;
        [IgnoreDataMember]
        internal ImageBrush obrazekIB;
        [IgnoreDataMember]
        internal CompositeTransform obrazekCT;
        [DataMember]
        internal bool czyWidoczny = false;

        //dodac jeszcze ID
        [IgnoreDataMember]
        internal Budynek domekRodzinny;
        [DataMember]
        internal ulong domekRodzinnyID;
        [DataMember]
        internal ulong zakladPracyID;
        [IgnoreDataMember]
        internal Budynek zakladPracy;
        internal GameEngine.UsunPostacHandler   usunPostacEvent;
        internal GameEngine.UsunZadanieHandler  usunZadanieEvent;
        internal GameEngine.DodajZadanieHandler dodajZadanieEvent;
        internal GameEngine.UkladMapyHandler    pobierzZUkladuMapy;

        public virtual void PrzywrocObrazek()
        {
            this.obrazekCT = new CompositeTransform()
            {
                TranslateX = 0,
                TranslateY = 0,
                CenterX = .5f,
                CenterY = .5f
            };
        }

        public virtual void PrzywrocDane(Budynek d, Budynek z)
        {
            this.domekRodzinny = d;
            this.zakladPracy = z;
        }
        public virtual void UsunReferencje()
        {
            if(this.zakladPracy != null)
            {
                this.zakladPracy.UsunPostac(this);
                this.zakladPracy = null;
            }

            this.usunPostacEvent(this, 1);
            this.domekRodzinny = null;
            usunZadanieEvent(this);
        }
        //mp.DodajDane(0this.dodajZadanieEvent, 1this.usunZadanieEvent, 2this.usunPostacEvent);
        public void DodajDane(params object[] obj)
        {
            this.dodajZadanieEvent  = obj[0] as GameEngine.DodajZadanieHandler;
            this.usunZadanieEvent   = obj[1] as GameEngine.UsunZadanieHandler;
            this.usunPostacEvent    = obj[2] as GameEngine.UsunPostacHandler;
            this.pobierzZUkladuMapy = obj[3] as GameEngine.UkladMapyHandler;
        }



        public Point Pozycja 
        { 
            get
            {
                return this.pozycja;
            }
            set
            {
                this.pozycja = value;
            }
        }
        public SByte AnimacjiaX
        {
            get 
            { 
                return animacjaX; 
            }
            set 
            { 
                animacjaX = value;
                if(animacjaX >= maxAnimacjiX)
                    animacjaX = 0;
            }
        }
        public SByte AnimacjiaY
        {
            get 
            { 
                return animacjaY; 
            }
            set 
            { 
                animacjaY = value;

                if(animacjaY >= maxAnimacjiY)
                    animacjaY = 0;
            }
        }
        
    }
}