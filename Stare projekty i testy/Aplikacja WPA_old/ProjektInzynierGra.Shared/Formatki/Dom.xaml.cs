﻿using ProjektInzynierGra.Klasy.Game;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using ProjektInzynierGra.Klasy;
using ProjektInzynierGra.Klasy.Budynki;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Formatki
{
    public sealed partial class Dom : Page
    {

        public Dom()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            var obj = e.Parameter as object[];

            this.lLista.DataContext = (new FormatkiDynamiczne(obj, this, new Type[]
                {
                    //typeof(ProjektInzynierGra.Klasy.Testowe.ATestowa),
                    typeof(DomekMieszkalny),
                    typeof(Straz)
                })).ListaObiektow;
        }

    }
}