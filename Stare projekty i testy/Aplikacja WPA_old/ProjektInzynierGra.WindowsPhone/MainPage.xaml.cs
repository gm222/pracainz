﻿using ProjektInzynierGra.Common;
using ProjektInzynierGra.Klasy.Game;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace ProjektInzynierGra
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private MiastoSave miastoInfo = null;
        bool load = false;
        public MainPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
            this.NavigationCacheMode = NavigationCacheMode.Enabled;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
            
        }

        private async void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            if(gSilnik != null)
            {
                gSilnik.ZatrzymajLogike();

                var pytanieBox = new Windows.UI.Popups.MessageDialog("Czy na pewno chcesz opuścić kampanie ?", "Pytanie");

                bool? result = null;
                pytanieBox.DefaultCommandIndex = 1;
                pytanieBox.Commands.Add(
                   new UICommand("Tak", new UICommandInvokedHandler((cmd) => result = true)));
                pytanieBox.Commands.Add(
                   new UICommand("Nie", new UICommandInvokedHandler((cmd) => result = false)));

                await pytanieBox.ShowAsync();

                if(result == null || result == false)
                {
                    gSilnik.WznowLogike();
                    return;
                }
                
                this.NavigationCacheMode = NavigationCacheMode.Disabled;
                //tutaj jeszcze czyszczenie zasobów gry
                this.navigationHelper.GoBack();
                HardwareButtons.BackPressed -= HardwareButtons_BackPressed;

            }
        }

        
        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);

            var obj = e.Parameter as object[];

            miastoInfo = obj[0] as MiastoSave;

            if(obj.Count() == 1)
            {
                this.gSilnik = new GameEngine();
                this.Loaded += GameStart;
            }
            else
            {
                load = true;
                SaveGame sG = obj[1] as SaveGame;

                try
                {
                    var plik = await Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.GetFileAsync(sG.Token);

                    this.gSilnik = await Stale.Odczyt<GameEngine>(plik);
                    this.Loaded += GameStartLoaded;

                }
                catch(Exception)
                {
                    //tutaj Zabezpieczenie
                    //this.navigationHelper.GoBack();
                    return;
                }
            }

            miastoInfo.ListaStanowGier.OrderBy(x => x.DataCzas);

            //SaveDodatkoweOpcje.DataContext = miastoInfo;
            //this.listaSave.ItemsSource = miastoInfo.ListaStanowGier;

            this.DataContext = this.gSilnik;
        }
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if(!widoczny)
            {
                //this.SaveBarVisible.Begin();
            }
            else
            {
                //this.SaveBarHide.Begin();
            }

            widoczny = !widoczny;

        }


        private void Load_Click(object sender, RoutedEventArgs e)
        {

        }

        private void RemoveSave_Click(object sender, RoutedEventArgs e)
        {

        }

        bool click = false;

        private void appBarButton_Click(object sender, RoutedEventArgs e)
        {
            //this.listaSave.SelectedIndex = -1;

            //(this.Resources["Clicked1"] as Storyboard).Begin();

            click = true;
        }


        private void Anuluj_Click(object sender, RoutedEventArgs e)
        {
            if(click == true)
            {
                //(this.Resources["Clicked2"] as Storyboard).Begin();
                click = false;
            }
            widoczny = !widoczny;
            //this.SaveBarHide.Begin();
            //tutaj reszte animacji

        }

        private async void Zapisz_Click(object sender, RoutedEventArgs e)
        {
            if(click == true)
            {
                //(this.Resources["Clicked2"] as Storyboard).Begin();
                click = false;
            }
            GameEngine stanGry = this.gSilnik;
            SaveGame zapisanaGra;

            StorageFile plik;

            //if(this.listaSave.SelectedIndex >= 0)
            //{
            //    zapisanaGra = this.listaSave.SelectedItem as SaveGame;
            //    plik = await Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.GetFileAsync(zapisanaGra.Token);
            //}
            //else
            //{
                var data = DateTime.Now;
                var nazwa = String.Format("{0}{1}{2}{3}{4}{5}{6}", data.Year, data.Month, data.Day, data.Hour, data.Minute, data.Second, data.Millisecond);

                string tok = await Stale.StworzPlik(KampanieLadowanie.folderZapisu);
                plik = await Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.GetFileAsync(tok);
                zapisanaGra = new SaveGame(nazwa, (sbyte)new Random().Next(0, 100), tok);
                miastoInfo.Add(zapisanaGra);
               

            //}

            zapisanaGra.DataCzas = DateTime.Now;

            gSilnik.ZatrzymajLogike();

            

            if(await Stale.Zapisz(stanGry, plik))
            {
                var sorted = miastoInfo.ListaStanowGier.OrderByDescending(a =>
                {
                    return a.DataCzas;

                });

                miastoInfo.ListaStanowGier = new System.Collections.ObjectModel.ObservableCollection<SaveGame>(sorted);

                await Stale.Zapisz(KampanieLadowanie.listaMiast, KampanieLadowanie.plikUstawien);//to może lecieć w tle - zrobić tylko sprawdzenie czy zapisze
                await new Windows.UI.Popups.MessageDialog("Gra zapisana").ShowAsync();
                gSilnik.WznowLogike();
            }
            else
            {
                await new Windows.UI.Popups.MessageDialog("Błąd podczas zapisywania.").ShowAsync();
            }



        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            //this.Frame.Content = new MainPage2();
        }

        private bool widoczny = false;

        #region NavigationHelper registration
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }


        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }

        #endregion

        private void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            AppButton1.Visibility = AppButton3.Visibility = Visibility.Collapsed;
            AppButton2.Visibility = AppButton3.Visibility = Visibility.Visible;

            OtworzZakladkiPaneluBocznego.Begin();
        }

        private void AppButton2_Click(object sender, RoutedEventArgs e)
        {
            AppButton2.Visibility = AppButton3.Visibility = Visibility.Collapsed;
            AppButton1.Visibility = AppButton3.Visibility = Visibility.Visible;
            if (krokOtworzeniaPaneluBocznego == true)
                ZamknijFramePaneluBocznego.Begin();
            else
                ZamknijZakladkiPaneluBocznego.Begin();

        }
    }
}
