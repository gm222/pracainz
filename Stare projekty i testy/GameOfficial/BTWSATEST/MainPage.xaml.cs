﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace BTWSATEST
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

        }

        private void ConnectClick(object sender, RoutedEventArgs e)
        {

        }

        private void AddItemClick(object sender, RoutedEventArgs e)
        {
            this.cont.Itemy.Add(new Obiekt(String.Format("Item{0}", this.cont.Itemy.Count() + 1), this.cont.Itemy.Count() + 1));

            this.lstLista.ItemsSource = null;
            this.lstLista.ItemsSource = this.cont.Itemy;
        }
    }

    public class Contener
    {
        List<Obiekt> _itemy = new List<Obiekt>();

        public List<Obiekt> Itemy
        {
            get { return this._itemy; }
        }

        public async Task<bool> Zapisz()
        {
            StorageFile sessionFile = await ApplicationData.Current.LocalFolder.CreateFileAsync("Plik2", CreationCollisionOption.ReplaceExisting);

            using (IRandomAccessStream sessionRandomAccess = await sessionFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                using (IOutputStream sessionOutputStream = sessionRandomAccess.GetOutputStreamAt(0))
                {
                    // var sessionSerializer = new DataContractSerializer(typeof(List<Obiekt>));
                    var sessionSerializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(List<Obiekt>));

                    sessionSerializer.WriteObject(sessionOutputStream.AsStreamForWrite(), _itemy);
                    await sessionOutputStream.FlushAsync();
                }
            }


            return true;
        }

        public async Task<bool> Odczyt()
        {
            StorageFile sessionFile = await ApplicationData.Current.LocalFolder.CreateFileAsync("Plik", CreationCollisionOption.OpenIfExists);

            if (sessionFile == null)
                return false;

            using (IInputStream sessionInputStream = await sessionFile.OpenReadAsync())
            {
                //var sessionSerializer = new DataContractSerializer(typeof(List<Obiekt>));

                var sessionSerializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(List<Obiekt>));

                using (var x = sessionInputStream.AsStreamForRead())
                {
                    this._itemy = (List<Obiekt>)sessionSerializer.ReadObject(x);
                    await x.FlushAsync();
                }
            }


            return true;



        }
    }

    [DataContractAttribute]
    public class Obiekt
    {
        string _name;
        int _id;

        [DataMember()]
        public String Nazwa
        {
            get { return this._name; }
            set { this._name = value; }
        }

        [DataMember()]
        public Int32 Id
        {
            get { return this._id; }
            set { this._id = value; }
        }

        public Obiekt(string name, int id)
        {
            this.Nazwa = name;
            this.Id = id;
        }
    }
}
