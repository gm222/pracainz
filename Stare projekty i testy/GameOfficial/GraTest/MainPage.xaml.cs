﻿using GraTest.Interfejsy;
using GraTest.Klasy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace GraTest
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            this.Loaded += GamerStart;
        }

        bool exit = false;
        GameEngine gSilnik;
        List<IObiektRender> listaObiektow = new List<IObiektRender>();


        private async void GamerStart(object sender, RoutedEventArgs e)
        {

            var ticks = Environment.TickCount;
            var lastTime = 0;
            int gTimeStart = 0, NextUpdateTick = 0;
            int TicksToSkip = 1000 / 60;

            gSilnik = new GameEngine();
          


            //List<Chipek> lista = new List<Chipek>();

            //for (int i = 0; i < 5000; i++)
            //    lista.Add(new Chipek());

            //this.lstLista.ItemsSource = lista;

            gTimeStart = NextUpdateTick = Environment.TickCount;
            Window.Current.Content.KeyDown += SprawdzanieExit;
            var kol = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 255, 255));
            this.aBtn.Background = kol;

            var red = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 0, 0));
            var green = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0, 255, 0));
            var blue = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0, 0, 255));

            //var lista2 = lista.ToArray();
            var rand = new Random();


            while (true && !exit)
            {
                //Window.Dispatcher.ProcessEvents(CoreProcessEventsOption.ProcessAllIfPresent);
                int loops = 0;



                if (Window.Current.CoreWindow.GetAsyncKeyState(Windows.System.VirtualKey.Escape) == CoreVirtualKeyStates.Down)
                    break;



                //while ((Environment.TickCount > NextUpdateTick) && (loops < 10) && !exit)
                //{
                    //foreach (var it in lista2)
                    //    it.ZmianaTla();
                    // await Task.Delay((int)TicksToSkip);
                    //switch (rand.Next(0, 3))
                    //{
                    //    case 0:
                    //        kol = red;
                    //        break;
                    //    case 1:
                    //        kol = green;
                    //        break;
                    //    case 2:
                    //        kol = blue;
                    //        break;
                    //}


                    foreach (var it in listaObiektow)
                    {
                        it.ZmianaTla();
                    }

                    NextUpdateTick += TicksToSkip;
                    loops++;
                //}

                //foreach (var it in lista2)
                //    await it.Render();



                //this.aBtn.Background = kol;

                foreach (var it in listaObiektow)
                {
                    it.Render();
                }

                await Task.Delay(250);
            }

            await new Windows.UI.Popups.MessageDialog("Exit").ShowAsync();






        }

        private void SprawdzanieExit(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Escape)
                this.exit = true;

        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.RunAsync(CoreDispatcherPriority.High, delegate
            {
                for (int i = 0; i < 1; i++)
                {
                    Dispatcher.RunAsync(CoreDispatcherPriority.High, delegate
                    {
                        var a = new Chipek1(gSilnik);
                        this.lstGrid.Items.Add(a);
                        this.listaObiektow.Add(a);
                    });
                }
            });

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }



        bool test1 = false;
        Point ruch;

        private void Button_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            test1 = true;
            var a = CoreCursorType.SizeAll;
            var x = e.GetCurrentPoint(this).Position.X;
            var y = e.GetCurrentPoint(this).Position.Y;
            ruch = new Point(x, y);

            Window.Current.CoreWindow.PointerCursor = new CoreCursor(a, 2);
        }

        private void Button_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            test1 = false;
            Window.Current.CoreWindow.PointerCursor = new CoreCursor(CoreCursorType.Arrow, 2);
            ruch = new Point();
            //Window.Current.CoreWindow.PointerCursor.Type = CoreCursorType.Hand;
        }

        private void Button_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            if (test1 == true)
            {

                var a = sender as Button;

                var x = e.GetCurrentPoint(this).Position.X;
                var y = e.GetCurrentPoint(this).Position.Y;


                var z = a.Margin;

                var left = z.Left += (x - ruch.X);
                var top = z.Top += (y - ruch.Y);


                var p = e.GetIntermediatePoints(this);
                var c = e.OriginalSource;

                a.Margin = new Thickness(left, top, 0, 0);

                ruch.X = x;
                ruch.Y = y;
            }
        }
    }
}
