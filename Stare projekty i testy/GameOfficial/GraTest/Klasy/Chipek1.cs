﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using GraTest.Interfejsy;
using System.Runtime.CompilerServices;
using System.ComponentModel;



namespace GraTest.Klasy
{
    class Chipek1 : Canvas,  IObiektRender
    {
        GameEngine _game;
        int pozycjaObrazek = -1;
        ImageBrush obrazekZrodlo = new ImageBrush();

        private WriteableBitmap Obrazek
        {
            get 
            {   
                return this._game.PobierzObrazek(pozycjaObrazek); 
            }
        }

        public Chipek1(GameEngine silnik)
        {
            this.Background = this.obrazekZrodlo;
            this._game = silnik;
            this.Width = this.Height = 200;
        }
        
        public void ZmianaTla()
        {
            if (pozycjaObrazek == this._game.Max-1)
            {
                pozycjaObrazek = 0;
                return;
            }

            pozycjaObrazek++;
        }

        public async void Render()
        {
            obrazekZrodlo.ImageSource = Obrazek;
        }
    }

}
