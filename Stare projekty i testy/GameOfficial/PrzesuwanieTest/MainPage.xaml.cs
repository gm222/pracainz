﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.ServiceModel.Channels;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Media3D;
using Windows.UI.Xaml.Navigation;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace PrzesuwanieTest
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        Obiekt obiekt;
        public MainPage()
        {
            this.InitializeComponent();
            obiekt = new Obiekt();
            obiekt.Inicit();
            this.DataContext = obiekt;

            //Window.Current.Content.PointerMoved += Content_PointerMoved;
            //Window.Current.Content.PointerEntered += Content_PointerEntered;
            //Window.Current.Content.PointerExited += Content_PointerExited;
  
        }

        private void ApplyProjection(Object sender, PointerRoutedEventArgs e)
        {
            Matrix3D m = new Matrix3D();

            // This matrix simply translates the image 100 pixels
            // down and 100 pixels right.
            m.M11 = 1.0; m.M12 = 0.2f; m.M13 = 0.0; m.M14 = 0.0;
            m.M21 = 0.0; m.M22 = 1.0; m.M23 = 0.0; m.M24 = 0.1;
            m.M31 = 0.0; m.M32 = 0.0; m.M33 = 1.0; m.M34 = 0.0;
            m.OffsetX = 100; m.OffsetY = 100; m.OffsetZ = 20; m.M44 = 0.33;

            Matrix3DProjection m3dProjection = new Matrix3DProjection();
            m3dProjection.ProjectionMatrix = m;

           // BeachImage.Projection = m3dProjection;

        }

        void Content_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            act = !act;
        }
        bool act = false;
        async void Content_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            act = true;
            while(act)
            {
                this.obiekt.X++;
                await Task.Delay(100);
            }
        }

        void Content_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            var a = e.GetCurrentPoint(this);
            var b = e.GetCurrentPoint(gGrid);

        }

        void Content_PointerCanceled(object sender, PointerRoutedEventArgs e)
        {
            
        }

        bool activated = false;
        Point point_new = new Point();

        CancellationTokenSource c;

        private async void gGrid_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            //if(e.Pointer.)
            //obiekt.MouseX = e.GetCurrentPoint(sender as Grid).Position.X;
            //obiekt.MouseY = e.GetCurrentPoint(sender as Grid).Position.X;
            c = new CancellationTokenSource();
            obiekt.Mouse = Window.Current.CoreWindow.PointerPosition;
            point_new = new Point(500, 500);
            activated = true;
            var calc = c.Token;
            Func<object[], Task> asd = Timer;
            var a = e.Pointer.PointerId;
            
                        await asd(new object[] {calc, this.gGrid});
                        //await Timer();
                    
        }

        private async Task Timer(object[] obj)
        {
            Message msg1; 
            MessageBuffer msg2;
            
            var token = (CancellationToken)obj[0];
            while (!token.IsCancellationRequested)
            {
                if (token.IsCancellationRequested)
                {
                    break;
                }

                

                if (obiekt.Mouse.X == point_new.X )
                {
                    obiekt.MouseX = new Point(obiekt.MouseX + 10, obiekt.MouseY + 10).X;
                    await this.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>
                    {
                        obiekt.X++; 
                        txtText.Text = obiekt.X.ToString();
                    });
                    
                    
                }

                if(obiekt.Mouse.Y == point_new.Y)
                {
                    obiekt.MouseY = new Point(obiekt.MouseX + 10, obiekt.MouseY + 10).Y;
                    await this.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, () =>
                    {
                        obiekt.Y++;
                        txtText.Text = obiekt.Y.ToString();
                    });
                }
                await Task.Delay(1);





            }

            
        }

        private void gGrid_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            var grid = sender as Grid;
            var pos = e.GetCurrentPoint(grid).Position;
            if (pos.X <= 50 || pos.X >= (int)grid.ActualWidth - 50 || pos.Y <= 50 || pos.Y >= (int)grid.ActualHeight - 50)
            {
                point_new = new Point(Window.Current.CoreWindow.PointerPosition.X, Window.Current.CoreWindow.PointerPosition.Y);
                obiekt.Mouse = point_new;
            }
            else
            obiekt.Mouse = pos;
        }

        private void gGrid_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {

        }

        private async void gGrid_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            c.Cancel();
            //await new Windows.UI.Popups.MessageDialog("Exit").ShowAsync();
        }

        private void BeachImage_PointerEntered(object sender, PointerRoutedEventArgs e)
        {

        }
    }

    public class Obiekt : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        TranslateTransform render;
        public TranslateTransform Render
        {
            get { return this.render; }
            set { this.render = value; OnPropertyChanged(); }
        }
        Point mouse = new Point();
        Point position = new Point();

        public void Inicit()
        {
            this.render = new TranslateTransform() { X = this.X, Y = this.Y };
        }

        public Point Mouse
        {
            get { return this.mouse; }
            set { this.mouse = value; MouseX = value.X; MouseY = value.Y; OnPropertyChanged(); }
        }

        public Point Position
        {
            get { return this.position; }
            set { this.position = value; OnPropertyChanged(); }
        }

        public double MouseX
        {
            get { return this.mouse.X; }
            set { this.mouse.X = value; OnPropertyChanged(); }
        }

        public double MouseY
        {
            get { return this.mouse.Y; }
            set { this.mouse.Y = value; OnPropertyChanged(); }
        }

        public double X
        {
            get { return this.position.X; }
            set { this.position.X = value; OnPropertyChanged(); this.Render = new TranslateTransform() { X = this.position.X, Y = this.position.Y }; }
        }

        public double Y
        {
            get { return this.position.Y; }
            set { this.position.Y = value; OnPropertyChanged(); this.Render = new TranslateTransform() { X = this.position.X, Y = this.position.Y }; }
        }

        private void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
    abstract class test1
{

}
    abstract class test2 : test1
    {

    }
    abstract class test3
    {

    }
    class a : test2
    {

    }
    class b : test3
    {

    }
    class c : test1
    {

    }
}
