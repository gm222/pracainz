﻿using TestExtendedSplash.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.ApplicationModel.Activation;
using System.Threading.Tasks;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace TestExtendedSplash
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class ExtendedSplash : Page
    {

        internal Frame rootFrame;
        internal bool dismissed = false; // Variable to track splash screen dismissal status.
        private SplashScreen splash; // Variable to hold the splash screen object.

        public ExtendedSplash(SplashScreen splashscreen, bool loadState)
        {
            this.InitializeComponent();
            Window.Current.SizeChanged += new WindowSizeChangedEventHandler(ExtendedSplash_OnResize);

            splash = splashscreen;

            if(splash != null)
            {
                splash.Dismissed += new TypedEventHandler<SplashScreen, Object>(DismissedEventHandler);
            }

            rootFrame = new Frame();
            rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];

            RestoreStateAsync(loadState);

            //rootFrame.Navigate(typeof(MainPage));
            this.Loaded += ExtendedSplash_Loaded;
        }

        async void ExtendedSplash_Loaded(object sender, RoutedEventArgs e)
        {
            int ile = 1000;
            this.pbProgress.Maximum = ile;
            for(int i = 0; i < ile; i += 50, this.pbProgress.Value = i)
            {
                await Task.Delay(500);
            }
            
            var result = rootFrame.Navigate(typeof(MainPage));

            if(!result)
            {
                await new Windows.UI.Popups.MessageDialog("Bład ładowania").ShowAsync();
                return;
            }

            Window.Current.Content = rootFrame;
        }


        private void DismissedEventHandler(SplashScreen sender, object args)
        {
            dismissed = true;
        }

        private void ExtendedSplash_OnResize(object sender, Windows.UI.Core.WindowSizeChangedEventArgs e)
        {

        }

        async void RestoreStateAsync(bool loadState)
        {
            if(loadState)
                await SuspensionManager.RestoreAsync();

            // Normally you should start the time consuming task asynchronously here and
            // dismiss the extended splash screen in the completed handler of that task
            // This sample dismisses extended splash screen  in the handler for "Learn More" button for demonstration
        }

    }
}
