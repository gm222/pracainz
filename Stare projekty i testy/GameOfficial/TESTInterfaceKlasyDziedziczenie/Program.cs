﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TESTInterfaceKlasyDziedziczenie
{
    class Program
    {
        static void Main(string[] args)
        {
            List<C> lista = new List<C>();
            for(int i = 0; i < 5; i++)
            {
                var a = new AA();
                a.Zmien();
                a.Zmieniaj(2, 3);
                var u = a.OdpC;
                lista.Add(a);
            }

            var ile = lista.Count();

            for(int i = 0; i < ile; i++)
            {
                
                var a = new AA();
                a.Zmien();
                a.Zmieniaj(2, 3);
                var u = a.OdpC;
                lista.Add(a);
            }

                
            
        }
    }


    public abstract class C
    {
        public abstract void Render();

        public void Zmien()
        {
            int a = 2, b = 3, c = a + b;

        }
    }


    abstract class A : C
    {
        int c;
        

        public void Zmieniaj(int a, int b)
        {
            c = a + b;
        }

        public int OdpC 
        { 
            get
            {
                return this.c;
            }
        }


        
    }

    class AA : A
    {
        int numer = 0;
        public override void Render()
        {
            numer++;
        }
    }
}
