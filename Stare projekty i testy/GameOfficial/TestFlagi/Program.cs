﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestFlagi
{
    class Program
    {
        [Flags]
        enum Flagi
        {
            None = 0,
            Jeden = 1,
            Dwa = 2,
            Cztery = 4,
            Osiem = 8,
            Szesnascie = 16
        };

        struct Point2
        {
            uint X;
            UInt16 Y;

            public Point2(uint x, UInt16 y)
            {
                X = x;
                Y = y;
            }
        }

        struct Point3
        {
            byte x;
            byte y;
        }

        static void Main(string[] args)
        {
            var a = Flagi.None;
            System.Diagnostics.Debug.WriteLine(a);
            Flagi b = Flagi.Cztery | Flagi.Osiem;
            System.Diagnostics.Debug.WriteLine(b);
            b|= a;
            System.Diagnostics.Debug.WriteLine(b);
            Flagi c = Flagi.Jeden | Flagi.Dwa | Flagi.Cztery | Flagi.Osiem | Flagi.Szesnascie;
            System.Diagnostics.Debug.WriteLine(c);

            var d = Enum.GetNames(typeof(Flagi));
            var e = Enum.GetValues(typeof(Flagi));
            var g = (e as Flagi[]).ToList();

            var h = g.Select(x => (int)x).ToList<int>();


            Point2 i = new Point2(200, 200);
            
        }
    }

    /// <summary>
    /// Opis klasy, tutaj można dodawać jakiś text
    /// </summary>
    /// <remarks>
    /// Longer comments can be associated with a type or member through
    /// the remarks tag.</remarks>
    class Test
    {
        public Test()
        {

        }

        /// <summary>
        /// This is just a test method, to check intellisense comments
        /// adsfdsfsb
        /// vcb
        /// vcb
        /// cvb
        /// cv
        /// dsf
        /// ewrw
        /// erw
        /// </summary>
        /// <summary>
        /// dfdsmewrwe TEST adafdf
        /// </summary>
        /// <param name="a">The name of the user as string </param>
        /// <param name="b">The password of the user as string</param>
        /// <returns >true if valid, flase if invalid </returns>
        public int test(int a, int b)
        {
            return 0;
        }
    }
}
