﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestSzybkosciKlas
{
    class Program
    {
        static void Main(string[] args)
        {

            System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch();




            var test = new List<string>();

            Int16 x = 20, y = 400, z = 1200;

            watch.Start();
                var b = new B(x, y ,z);
            watch.Stop();

            test.Add("b - " + watch.Elapsed.ToString());

            watch.Reset(); Thread.Sleep(100); watch.Start();
                var bb = new B()
                {
                    lok = new Point(),
                };

                bb.lok.X = x;
                bb.lok.Y = y;
                bb.lok.Z = z;
            watch.Stop();

            test.Add("bb - " + watch.Elapsed.ToString());

            watch.Reset(); Thread.Sleep(100); watch.Start();
                var bbb = new B(x, y, z);
            watch.Stop();

            test.Add("bbb - " + watch.Elapsed.ToString());

            watch.Reset(); Thread.Sleep(100); watch.Start();
                B bbbb = new B(x, y, z);
            watch.Stop();

            test.Add("bbbb - " + watch.Elapsed.ToString());

            watch.Reset(); Thread.Sleep(100); watch.Start();
                var d = new B(x, y, z);
            watch.Stop();

            test.Add("d - " + watch.Elapsed.ToString());

            watch.Reset(); Thread.Sleep(100); watch.Start();
                var dd = new D()
                {
                    lok = new Point(),
                };

                dd.lok.X = x;
                dd.lok.Y = y;
                dd.lok.Z = z;
            watch.Stop();
            test.Add("dd - " + watch.Elapsed.ToString());

            watch.Reset(); Thread.Sleep(100); watch.Start();
                var f = new F("Hello World !!!");
            watch.Stop();
                test.Add("f - " + watch.Elapsed.ToString());

            for(int i = 0; i < test.Count(); i++)
            {
                Console.WriteLine(test[i]);
            }

            Console.ReadKey(); //exit
        }
    }

    public struct Point
    {
        public Int16 X;
        public Int16 Y;
        public Int16 Z;
    }

    public abstract class A
    {
        public Point lok;
    }

    public class B : A
    {
        public B()
        {

        }
        public B(Int16 x, Int16 y, Int16 z)
        {
            this.lok = new Point();

            this.lok.X = x;
            this.lok.Y = y;
            this.lok.Z = z;
        }
    }

    public abstract class C : A
    {

    }

    public class D : C
    {
        public D()
        {

        }
        public D(Int16 x, Int16 y, Int16 z)
        {
            this.lok = new Point();

            this.lok.X = x;
            this.lok.Y = y;
            this.lok.Z = z;
        }
    }

    public abstract class E
    {
        public string Name;
    }

    public class F : E
    {
        public F(string n)
        {
            base.Name = n;
        }
    }




}
