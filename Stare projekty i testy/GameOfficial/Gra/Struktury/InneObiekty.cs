﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gra.Struktury
{
    public struct Point2
    {
        Int16 X;
        Int16 Y;
        Byte Z; //kierunek

        public Point2(Int16 x, Int16 y, Byte z)
        {
            X = x; Y = y; Z = z;
        }
    }
}
