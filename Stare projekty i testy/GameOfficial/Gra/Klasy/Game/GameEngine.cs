﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;


namespace Gra.Klasy.Game
{
    public class GameEngine
    {

        ImageBrush brush = new ImageBrush();
        WriteableBitmap[] animacja;
        private int max;
        bool exit = false; //wylaczenie gry
        MainPage formatka;
        delegate void EventHandler();
        event EventHandler e;


       
        

        public WriteableBitmap PobierzObrazek(int pozycja)
        {
            return animacja[pozycja];
        }

        public int Max
        {
            get { return this.max; }
        }

        public GameEngine()
        {

        }
        public GameEngine(MainPage _formatka) : this()
        {
            this.formatka = _formatka;
        }
        private async Task<WriteableBitmap[]> PobierzObrazekAnimacji(int nr, byte x, byte y)
        {
            byte[] srcPixels;
            int width = 0;

            var uri = new System.Uri(String.Format("ms-appx:///Assets/img/{0}.png", nr));
            
            using(IRandomAccessStream fileStream = await(await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(uri)).OpenAsync(Windows.Storage.FileAccessMode.Read))
            {
                BitmapDecoder decoder = await BitmapDecoder.CreateAsync(fileStream);
                PixelDataProvider pixelData = await decoder.GetPixelDataAsync(
                                                        BitmapPixelFormat.Bgra8,
                                                        BitmapAlphaMode.Straight,
                                                            new BitmapTransform(),
                                                        ExifOrientationMode.IgnoreExifOrientation,
                                                        ColorManagementMode.DoNotColorManage);
                width = (int)decoder.PixelWidth;
                srcPixels = pixelData.DetachPixelData();
            }

            List<WriteableBitmap> animacjaTym = new List<WriteableBitmap>();


            //tutaj dodać obsługę x i y
            for(int i = 0; i < width / (x * 20); i++)
            {
                byte[] obrazek = new byte[x * 20 * y * 20 * 4];

                for(int j = 0; j < y * 20; j++)
                    for(int k = 0; k < x * 20; k++)
                    {
                        obrazek[(j * x * 20 + k) * 4 + 0] = srcPixels[(j * width + k + i * x * 20) * 4 + 0];
                        obrazek[(j * x * 20 + k) * 4 + 1] = srcPixels[(j * width + k + i * x * 20) * 4 + 1];
                        obrazek[(j * x * 20 + k) * 4 + 2] = srcPixels[(j * width + k + i * x * 20) * 4 + 2];
                        obrazek[(j * x * 20 + k) * 4 + 3] = srcPixels[(j * width + k + i * x * 20) * 4 + 3];
                    }
                WriteableBitmap obrazekMapa = new WriteableBitmap(x*20, y*20); // o co chodzi z tymi 200x200

                using(Stream stream = obrazekMapa.PixelBuffer.AsStream())
                {
                    await stream.WriteAsync(obrazek, 0, obrazek.Length);
                }
                animacjaTym.Add(obrazekMapa);
            }
            
            return animacjaTym.ToArray(); //animacja to od 0...5 i później od 6 do np. 9, czyli jedna tablica
        }
        public async Task<bool> InicjalizacjaAnimacji()
        {
            try
            {

                int ilosc = GameConstans.obrazkiWlasciwosciTabela.Length;
                List<WriteableBitmap> animacjatymczasowa = new List<WriteableBitmap>();


                //var progress = MainPage.progress;
                
                //progress.Maximum = ilosc;
                //progress.Minimum = 0;
                //progress.IsIndeterminate = true;
                //progress.IsEnabled = true;
                //progress.Visibility = Visibility.Visible;
                
                for(int i = 0; i < ilosc; i++)
                {
                    var obrazek = GameConstans.obrazkiWlasciwosciTabela[i];
                    var temp = await PobierzObrazekAnimacji(i, (byte)obrazek.X, (byte)obrazek.Y);
                    for(int j = 0; j < temp.Count(); j++)
                        animacjatymczasowa.Add(temp[i]);
                    //progress.Value += i;
                }

                this.animacja = animacjatymczasowa.ToArray();
                animacjatymczasowa.Clear(); animacjatymczasowa = null;
            }
            catch(Exception)
            {
                return false;
            }

            return true;

        }//end of Start Method
        public async Task<bool> GameLoop()
        {
            Window.Current.Content.KeyDown += SprawdzanieExit;
            CompositionTarget.Rendering += new System.EventHandler<object>(RenderGlobal);
            

            //tutaj funkcje do inicjalizacji


            while(true && !exit)
            {
                //if(exit) break; //tutaj ma być wyswietlony appbar
                //Logika
                await Task.Delay(250); // 1/4 sekundy
            }

            //tutaj jeszcze zrobic sprzatanie
            await PosprzatajPoSobie();
            return true;
        }

        private void RenderGlobal(object sender, object e)
        {
            //tutaj dodać definicje renderowanie dla widocznych obiektów
        }
        private async Task<bool> PosprzatajPoSobie()
        {
            //tutaj zrobić foreach po wszystkich obiektach

            for(int i = 0; i < animacja.Length; i++)
                animacja[i] = null;

            animacja = null;


            return true;
        }
        //dodac jeszcze cheat :D przez string sklejanie
        private void SprawdzanieExit(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            //if (e.Key == Windows.System.VirtualKey.Escape)
            //    this.exit = true;

            

        }

        //Ładowanie obrazków do tablicy
        //Tworzenie animacji
        //Zrobić dla każdego budynku!!
        


        
    }//end of Class GameEngine
}//end of Namespace
