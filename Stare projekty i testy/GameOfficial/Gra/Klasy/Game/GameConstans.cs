﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;


namespace Gra.Klasy.Game
{
    public class GameConstans
    {
        public static ObrazkiWlasciwosci[] obrazkiWlasciwosciTabela = new ObrazkiWlasciwosci[]{};
        Kampania[] kampanie;
        string error;
        List<string> miasta = new List<string>()
        {
            "PierwszeMiasto",
            "DrugieMiasto",
            "TrzecieMiasto",
            "CzwarteMiasto",
            "PiąteMiasto",
            "SzósteMiasto"
        };

        public GameConstans()
        {

        }

        public GameConstans(string e) : this()
        {
            this.error = e;
            
        }

        private async Task<bool> Odczyt()
        {            
            var uri = new System.Uri("ms-appx:///Assets/ustawienia/obrazki.json");

            try
            {
                using(IInputStream fileStream = await (await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(uri)).OpenReadAsync())
                {
                    var sessionSerializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(List<ObrazkiWlasciwosci>));

                    using(var x = fileStream.AsStreamForRead())
                    {
                        var listobjects = (List<ObrazkiWlasciwosci>)sessionSerializer.ReadObject(x);
                        obrazkiWlasciwosciTabela = listobjects.ToArray();
                        await x.FlushAsync();
                    }
                }
            }
            catch(Exception)
            {
                return false;
            }
            
            return true;
        }

        public async Task<bool> GameSettings()
        {
            ApplicationDataContainer localsettings = ApplicationData.Current.LocalSettings;
            ApplicationDataCompositeValue tabdane = new ApplicationDataCompositeValue();

            if(localsettings.Values["dane"] == null || localsettings.Values.Count() == 0)
            {
                //tutaj uzupełnić jeszcze i zrobić atrybuty do których zostanie przypisane odpowiednie dane
                //adcv["location"] = "Chicago";
                localsettings.Values["dane"] = tabdane;
            }

            if(await Odczyt()) //inicializacja obiektów
                return false;

            tabdane = localsettings.Values["dane"] as ApplicationDataCompositeValue;


            return true;
        }



    }


    [DataContractAttribute]
    public class ObrazkiWlasciwosci
    {
        Int16 x;
        Int16 y;

        [DataMember()]
        public Int16 X
        {
            get { return this.x; }
            set { this.x = value; }
        }

        public Int16 Y
        {
            get { return this.y; }
            set { this.y = value; }
        }

        public ObrazkiWlasciwosci(Int16 x, Int16 y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}
