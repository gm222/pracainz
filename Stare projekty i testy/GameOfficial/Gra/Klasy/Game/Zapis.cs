﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gra.Klasy.Game
{
    public enum Flaga
    {
        Brak,
        WTrakcie,
        Gotowe
    }
    public class Kampania
    {
        bool dostepny;
        Flaga flaga;
        byte id_miasta;
        byte[] parent; //id_miasta, id_miasta
    
        public bool Dostepny 
        { 
            get
            {
                return this.dostepny;
            }
            set
            {
                this.dostepny = value;
            }
        }

        public Flaga Flaga 
        { 
            get 
            {
                return this.flaga;
            }
            set
            {
                this.flaga = value;
            }
        }

        
    }

    public class Save
    {
        DateTime czas;

        String nazwa;
        byte id_miasta;

        public DateTime Czas 
        { 
            get
            {
                return this.czas;
            }
            set
            {
                this.czas = value;
            }
        }

        public String Nazwa
        {
            get
            {
                return this.nazwa;
            }
            set
            {
                this.nazwa = value;
            }
        }

        public byte Miasto
        {
            get
            {
                return this.id_miasta;
            }
            set
            {
                this.id_miasta = value;
            }

        }


        public Save()
        {
            
        }
    }
}
