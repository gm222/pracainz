﻿using Gra.Struktury;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gra.Klasy.Postacie
{
    public abstract class Postac
    {
        Point2 pozycja; //X i Y
        Int16 hp;

        public Int16 HP 
        { 
            get
            {
                return this.hp;
            }
            set
            {
                this.hp = value;
            }
        }
    }
}
