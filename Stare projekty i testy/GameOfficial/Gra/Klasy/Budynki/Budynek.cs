﻿using Gra.Interfaces;
using Gra.Struktury;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml;

namespace Gra.Klasy.Budynki
{
    public abstract class Budynek : Bazowa //: UIElement, IObiektRender
    {

        Int16 maxWysOsob;
        Int16 maxPojemnoscOsob;
        Point2 lokalizacja; //tutaj X i Y
        //Int16 iloscZajetychPolX;
        //Int16 iloscZajetychPolY;
        //Int16[] iloscZajetychPol = new Int16[2]; // [0] - X; [1] - Y
        Point2 iloscZajetychPol;
        Int16 hp;
        String nazwa;

        public String Nazwa
        {
            get { return this.nazwa; }
            set { this.nazwa = value; }
        }

        public Point2 IloscZajetychPol 
        { 
            get
            {
                return this.iloscZajetychPol;
            }
            set
            {
                this.iloscZajetychPol = value;
            }
        }

        //public abstract Task Render(); 
        //public abstract Task ZmienTlo();
    }
}
