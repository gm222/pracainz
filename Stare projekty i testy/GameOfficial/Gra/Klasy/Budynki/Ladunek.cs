﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gra.Klasy.Budynki
{
    public abstract class Ladunek : Budynek
    {
        Int16 pojemnoscMagazynu;
        Boolean typLadunku;

        public Int16 PojemnoscMagazynu
        {
            get { return this.pojemnoscMagazynu; }
            set { this.pojemnoscMagazynu = value; }
        }

        public Boolean TypLadunku
        {
            get { return this.typLadunku; }
            set { this.typLadunku = value; }
        }



        
    }
    #region Pole
    public abstract class Pole : Ladunek
    {
    }

    public class Winorosle : Pole
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class DrzewoOliwne : Pole
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }


    #endregion
    #region Farma
    public abstract class Farma : Pole
    {
        Boolean typZwierzat;
        
        public Boolean TypZwierzat
        {
            get { return this.typZwierzat; }
            set { this.typZwierzat = value; }
        }
    }

    public class FarmaOwcow : Farma
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class FarmaKoz : Farma
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }
    #endregion

    public class Mleczarnia : Ladunek
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class Zbrojownia : Ladunek
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class StadninaKoni : Ladunek
    {
        Int16 iloscKoni;

        public Int16 IloscKoni
        {
            get { return this.iloscKoni; }
            set { this.iloscKoni = value; }
        }

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class ProdukcjaLukow : Ladunek
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class ChataPasterska : Ladunek
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class Tartak : Ladunek
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class Winnica : Ladunek
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class DomekMysliwski : Ladunek
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class DomekOliwny : Ladunek
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class DomekRybacki : Ladunek
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    #region Kopalnia
    public abstract class Kopalnia : Ladunek
    {
    }

    public class KopalniaRudy : Kopalnia
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class Mielnica : Kopalnia
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    #endregion



}
