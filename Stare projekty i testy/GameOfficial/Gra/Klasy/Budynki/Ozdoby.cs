﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gra.Klasy.Budynki
{
    public abstract class Ozdoby : Budynek
    {
        Int16 poziomUpiekszeniaMiasta;

        public Int16 PoziomUpiekszeniaMiasta
        {
            get { return this.poziomUpiekszeniaMiasta; }
            set { this.poziomUpiekszeniaMiasta = value; }
        }
    }

    #region Pomniki
    public abstract class Pomniki : Ozdoby
    {
    }

    public class PomnikWiedzy : Pomniki
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class PomnikZwyciestwa : Pomniki
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class PomnikObrony : Pomniki
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class PomnikSztuki : Pomniki
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

#endregion

    public class Ogrod : Ozdoby
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

}
