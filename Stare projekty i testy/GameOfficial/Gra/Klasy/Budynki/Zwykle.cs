﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gra.Klasy.Budynki
{
    public abstract class Zwykle : Budynek
    {    

    }

    public class Palac : Zwykle
    {
        Int16 iloscZlota;
        Int16 ludnosc;

        public Int16 IloscZlota
        {
            get { return this.iloscZlota; }
            set { this.iloscZlota = value; }
        }

        public Int16 Ludnosc
        {
            get { return this.ludnosc; }
            set { this.ludnosc = value; }
        }

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class BramaMiasta : Zwykle
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    #region BudynkiMieszkalne
    public abstract class BudynkiMieszkalne : Zwykle
    {
        Boolean typZasobow;
        Int16 iloscZasobow;
        Int16 czasOdwiedzenieStrazy;
        Int16 czasOdwiedzeniaMedyka;
        Int16 czasOdwiedzeniaWodnika;
    }

    public class DomekMieszkalny : BudynkiMieszkalne
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class DomekWojskowy : BudynkiMieszkalne
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    #endregion

    public class Straz : Zwykle
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class StrazMiejska : Zwykle
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class ZrodloWody : Zwykle
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class Szpital : Zwykle
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class Szkola : Zwykle
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class Podjum : Zwykle
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class Teatr : Zwykle
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class SzkolaAktorska : Zwykle
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class UrzadGminy : Zwykle
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class MorObronny : Zwykle
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }
    }




}
