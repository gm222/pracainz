﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gra.Klasy.Budynki
{
    public abstract class MagazynyWieloelementowe : Budynek
    {
        Int16 pojemnoscMagazynu;
        Boolean typLadunku;

        public Int16 PojemnoscMagazynu
        {
            get { return this.pojemnoscMagazynu; }
            set { this.pojemnoscMagazynu = value; }
        }

        public Boolean TypLadunku
        {
            get { return this.typLadunku; }
            set { this.typLadunku = value; }
        }
    }

    public class Spichlerz : MagazynyWieloelementowe
    {
        public override void Render()
        {
            throw new NotImplementedException();
        }

    }

    public class Magazyn : MagazynyWieloelementowe
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }

    public class Stragan : MagazynyWieloelementowe
    {

        public override void Render()
        {
            throw new NotImplementedException();
        }
    }
}
