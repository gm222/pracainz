﻿using Gra.Common;
using Gra.Formatki;
using Gra.Klasy.Game;
using Gra.SettingsPanel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace Gra
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        //Globals
        GameEngine gSilnik;
        List<TypeInfo> listaForatek;
        object[] obiekty;

        //do przechowywania w [0] UIElement i w [1] by przechowywać czy obiekt może zostać dodany w danym miejscu
        object[] obiektMiedzyKlasowyWartosc = new object[2];

        #region Wbudowane
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        #endregion
        #region Wbudowane
        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion



        #endregion

        public MainPage()
        {
            this.InitializeComponent();
            #region Wbudowane
                this.navigationHelper = new NavigationHelper(this);
                this.navigationHelper.LoadState += navigationHelper_LoadState;
                this.navigationHelper.SaveState += navigationHelper_SaveState;
            #endregion

            this.cContentPlotna.Width = Window.Current.Bounds.Width;
            this.cContentPlotna.Height = Window.Current.Bounds.Height;

            obiektMiedzyKlasowyWartosc[0] = null;
            obiektMiedzyKlasowyWartosc[1] = false;

            Func<UIElement,Task<bool>> f = TworzeniePlotna;
            obiekty = new object[]
            {
                f,
                obiektMiedzyKlasowyWartosc,
                this.cPlansza, 
                this.cContentPlotna
            };
            
            this.Loaded += GameStart;
        }





        //Główny silnik gry
        private async void GameStart(object sender, RoutedEventArgs e)
        {

            try
            {
                var deftypes = Assembly.Load(new AssemblyName(Windows.ApplicationModel.Package.Current.DisplayName));
                listaForatek = deftypes.DefinedTypes.ToList().Where(x => x.Namespace.Contains("Formatki")).ToList<TypeInfo>();
            }
            catch(Exception)
            {
                return;
            }
            gSilnik = new GameEngine(this);

            var status = await gSilnik.InicjalizacjaAnimacji();

            
            //if(status[0] == false)
            //{
            //    await new Windows.UI.Popups.MessageDialog("Błąd ładowania animacji").ShowAsync();
            //    this.navigationHelper.GoBack();
            //    return;
            //}

            await gSilnik.GameLoop();

            
        }

        private async void Formatka_Click(object sender, RoutedEventArgs e)
        {
            var tag   = (sender as Button).Tag.ToString();
            var bladS = "";
            var bladB = false;
            
            try
            {
                var formatka = listaForatek.Where(x => x.Name == tag).FirstOrDefault();
                var typ = formatka.AsType();
                var obj = Activator.CreateInstance(typ, (object)obiekty);
                this.fFrame.Content = obj as UIElement;
                return;
            }
            catch(Exception ex)
            {
                bladB = true;
                bladS = ex.Message;
            }

            if(bladB)
                await new Windows.UI.Popups.MessageDialog(bladS).ShowAsync();
        }
        private async Task<bool> TworzeniePlotna(UIElement obj)
        {
            //idea globalna - wysyłany jest tylko adres do Image
            var cMyszkaPlotno = new Canvas() 
            { 
                Visibility = Visibility.Visible, 
                Background = new SolidColorBrush(Windows.UI.Color.FromArgb(0,0,0,255)), 
                Width = cContentPlotna.Width, 
                Height = cContentPlotna.Height,
                
            };

            Canvas.SetZIndex(cMyszkaPlotno, 1);

            cMyszkaPlotno.PointerMoved += cPlotnoTworzenie_PointerMoved;

            cContentPlotna.Children.Add(cMyszkaPlotno);
            obiektMiedzyKlasowyWartosc[0] = obj;
            obiektMiedzyKlasowyWartosc[1] = true;
            this.cContentPlotna.Children.Add(obj);

            return true;
        }
        private async void cPlotnoTworzenie_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            //if(obiektMiedzyKlasowyWartosc[0] == null)
            //    return;
            
            var obiekt = obiektMiedzyKlasowyWartosc[0] as UIElement;
            var pos = e.GetCurrentPoint(cContentPlotna).Position;

            var translate = obiekt.RenderTransform as CompositeTransform;
            //translate.TransformPoint(pos);

            translate.TranslateX = pos.X;
            translate.TranslateY = pos.Y;

            //tutaj jeszcze zrobic czy dany obiekt mozna tutaj polozyc, flage ustawic w obiektMiedzyKlasowyWartosc[1]
        }


        //Mariusz
        Page plansza;
        double miniMapkaWidth = 0;
        double miniMapkaHeight = 0;
        private void Page_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            plansza = sender as Page;
            var width = plansza.ActualWidth;
            var height = plansza.ActualHeight;
            double przekatna = cPlansza.Width * Math.Sqrt(2);

            borMiniMapka.Width = 300 * (width - 400) / przekatna;
            borMiniMapka.Height = 300 * height / przekatna;

            miniMapkaWidth = (300 * (width - 400) / przekatna) / 2;
            miniMapkaHeight = (300 * height / przekatna) / 2;

            (borMiniMapka.RenderTransform as CompositeTransform).TranslateX = -(cPlansza.RenderTransform as CompositeTransform).TranslateX / (width / (300 * width / przekatna));
            (borMiniMapka.RenderTransform as CompositeTransform).TranslateY = -(cPlansza.RenderTransform as CompositeTransform).TranslateY / (height / (300 * height / przekatna));
        }

        DispatcherTimer timer;
        PointerRoutedEventArgs pointerEvent;
        bool wlaczony = false;
        private void Grid_PointerMoved_1(object sender, PointerRoutedEventArgs e)
        {
            if(wlaczony == false)
            {
                pointerEvent = e;
                double X = pointerEvent.GetCurrentPoint(plansza).Position.X;
                double Y = pointerEvent.GetCurrentPoint(plansza).Position.Y;
                timer = new DispatcherTimer();
                if(X < 30 || X > plansza.ActualWidth - 30 || Y < 30 || Y > plansza.ActualHeight - 30)
                {
                    timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
                    timer.Tick += new EventHandler<object>(timerTick);
                    timer.Start();
                    wlaczony = true;
                }
            }
        }
        private void timerTick(object sender, object e)
        {
            double X = pointerEvent.GetCurrentPoint(plansza).Position.X;
            double Y = pointerEvent.GetCurrentPoint(plansza).Position.Y;
            double TranslateX = (cPlansza.RenderTransform as CompositeTransform).TranslateX;
            double TranslateY = (cPlansza.RenderTransform as CompositeTransform).TranslateY;
            double przekatna = cPlansza.Width * Math.Sqrt(2);
            bool zmiana = false;
            if(X < 30)
            {
                zmiana = true;
                if(TranslateX + 25 > 0)
                    (cPlansza.RenderTransform as CompositeTransform).TranslateX = 0;
                else
                    (cPlansza.RenderTransform as CompositeTransform).TranslateX = TranslateX + 25;
            }
            if(X > plansza.ActualWidth - 30)
            {
                zmiana = true;
                if(TranslateX - 25 < -(przekatna + 400 - plansza.ActualWidth))
                    (cPlansza.RenderTransform as CompositeTransform).TranslateX = -(przekatna + 400 - plansza.ActualWidth);
                else
                    (cPlansza.RenderTransform as CompositeTransform).TranslateX = TranslateX - 25;
            }
            if(Y < 30)
            {
                zmiana = true;
                if(TranslateY + 25 > 0)
                    (cPlansza.RenderTransform as CompositeTransform).TranslateY = 0;
                else
                    (cPlansza.RenderTransform as CompositeTransform).TranslateY = TranslateY + 25;
            }
            if(Y > plansza.ActualHeight - 30)
            {
                zmiana = true;
                if(TranslateY - 25 < -(przekatna - plansza.ActualHeight))
                    (cPlansza.RenderTransform as CompositeTransform).TranslateY = -(przekatna - plansza.ActualHeight);
                else
                    (cPlansza.RenderTransform as CompositeTransform).TranslateY = TranslateY - 25;
            }
            if(zmiana == false)
            {
                timer.Stop();
                wlaczony = false;
            }

            var width = Windows.UI.Xaml.Window.Current.Bounds.Width;
            var height = Windows.UI.Xaml.Window.Current.Bounds.Height;
            (borMiniMapka.RenderTransform as CompositeTransform).TranslateX = -(cPlansza.RenderTransform as CompositeTransform).TranslateX / (width / (300 * width / przekatna));
            (borMiniMapka.RenderTransform as CompositeTransform).TranslateY = -(cPlansza.RenderTransform as CompositeTransform).TranslateY / (height / (300 * height / przekatna));
        }
        bool miniMapkaPressed = false;
        private void Grid_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            miniMapkaPressed = true;
            Window.Current.Content.PointerReleased += Grid_PointerReleased;
            ZmianaPolozeniaMiniMapki(sender);
        }
        private void Grid_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            miniMapkaPressed = false;
            Window.Current.Content.PointerReleased -= Grid_PointerReleased;
        }
        private void Grid_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            if(miniMapkaPressed == true)
            {
                ZmianaPolozeniaMiniMapki(sender);
            }
        }
        private void ZmianaPolozeniaMiniMapki(object sender)
        {
            var tym = (borMiniMapka.RenderTransform as CompositeTransform);
            double x = pointerEvent.GetCurrentPoint(sender as Grid).Position.X;
            if(x - miniMapkaWidth < 0)
                tym.TranslateX = 0;
            else if(x + miniMapkaWidth > 300)
                tym.TranslateX = 300 - 2 * miniMapkaWidth;
            else
                tym.TranslateX = x - miniMapkaWidth;

            double y = pointerEvent.GetCurrentPoint(sender as Grid).Position.Y;
            if(y - miniMapkaHeight < 0)
                tym.TranslateY = 0;
            else if(y + miniMapkaHeight > 300)
                tym.TranslateY = 300 - 2 * miniMapkaHeight;
            else
                tym.TranslateY = y - miniMapkaHeight;

            double przekatna = cPlansza.Width * Math.Sqrt(2);
            var width = plansza.ActualWidth;
            var height = plansza.ActualHeight;
            (cPlansza.RenderTransform as CompositeTransform).TranslateX = -(borMiniMapka.RenderTransform as CompositeTransform).TranslateX * (width / (300 * width / przekatna));
            (cPlansza.RenderTransform as CompositeTransform).TranslateY = -(borMiniMapka.RenderTransform as CompositeTransform).TranslateY * (height / (300 * height / przekatna));
        }
        private void Grid_PointerOpuszczenie(object sender, PointerRoutedEventArgs e)
        {
            miniMapkaPressed = false;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MenuHideVisible();
        }
        bool buttonWysuniety = false;
        private void MenuHideVisible()
        {
            if(buttonWysuniety == false)
            {
                Hide.Begin();
                buttonWysuniety = true;
            }
            else
            {
                Visible.Begin();
                buttonWysuniety = false;
            }
        }

        // Mariusz koniec




        private void ClosePopUp_ButtonClick(object sender, RoutedEventArgs e)
        {
            this.gPopUp.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void Ustawienia(object sender, RoutedEventArgs e)
        {
            App.settings.ShowIndependent();
            
        }

       
    }
}
