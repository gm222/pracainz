﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Gra.Formatki
{
    public sealed partial class Dom : UserControl
    {

        Canvas cPlansza;
        Canvas cContentPlotna;
        object[] obj2;
        Func<UIElement, Task<bool>> funkcja;

        public Dom()
        {
            this.InitializeComponent();
            
        }

        public Dom(object obj) : this()
        {
            var obiekt = obj as object[];


            this.funkcja = obiekt[0] as Func<UIElement,Task<bool>>;
            this.obj2 = obiekt[1] as object[];
            this.cPlansza = obiekt[2] as Canvas;
            this.cContentPlotna = obiekt[3] as Canvas;


            Image dMieszkalnyImage = new Image()
            {
                Width = Height = 50,
                Source = new BitmapImage(new Uri("ms-appx:///Assets/img/domek1.png"))
                
            };

            Image dWojskowyImage = new Image()
            {
                Width = Height = 50,
                Source = new BitmapImage(new Uri("ms-appx:///Assets/img/domek2.png"))

            };

            dMieszkalnyImage.PointerPressed += dMieszkalnyImage_PointerPressed;
            
            dWojskowyImage.PointerPressed += dWojskowyImage_PointerPressed;
            
            
            lLista.Items.Add(dMieszkalnyImage);
            lLista.Items.Add(dWojskowyImage);
        }

       
       

        void dWojskowyImage_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            
        }

        private async void dMieszkalnyImage_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            
            e.Handled = true;
            cContentPlotna.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            cContentPlotna.Children.Clear();
            if((bool)obj2[1] == true)
            {
                var xd = obj2[0] as UIElement;
                var pos = e.GetCurrentPoint(cPlansza).Position;
                var translate = xd.RenderTransform as CompositeTransform;
                translate.TranslateX = pos.X;
                translate.TranslateY = pos.Y;
                translate.Rotation = 0;

                cPlansza.Children.Add(xd);
            }
            obj2[0] = null;
            obj2[1] = false;
            Window.Current.Content.PointerReleased -= dMieszkalnyImage_PointerReleased;
            await Task.Delay(1);
        }

        private async void dMieszkalnyImage_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            var butt = e.GetCurrentPoint(this);

            if(!butt.Properties.IsLeftButtonPressed)
                return;
            
            cContentPlotna.Visibility = Visibility.Visible;
            Window.Current.Content.PointerReleased += dMieszkalnyImage_PointerReleased; //globalny pointer do zdarzenia opuszczenia elementow gui

            var a = new Image()
            {
                Source = new BitmapImage(new Uri("ms-appx:///Assets/img/domek1.png")),  
            };


            Canvas.SetZIndex(a, 0);

            var pos2 = Window.Current.CoreWindow.PointerPosition;
            //TranslateTransform translateTransform2 = new TranslateTransform() { X = pos2.X, Y = pos2.Y };
            a.RenderTransform = new CompositeTransform() { TranslateX = pos2.X, TranslateY = pos2.Y, Rotation = 45 };

            await funkcja(a);

        }

    }
}
