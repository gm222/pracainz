﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace Gra.Formatki
{
    public interface IPrzesuwanie
    {

    }

    public class TrickKlasa : Button, IPrzesuwanie, INotifyPropertyChanged
    {
        int xx = 0;
        int yy = 0;
        TranslateTransform xxyy;
        public int XX { get{return this.xx; } set {this.xx = value; OnPropertyChanged();} }
        public int YY { get { return this.yy; } set { this.yy = value; OnPropertyChanged(); } }
        public TranslateTransform XXYY
        {
            get
            {
                return xxyy;
            }

            set
            {
                this.xxyy = value; OnPropertyChanged();
            }
        }
        public TrickKlasa(int x, int y, string text)
        {
            XX = x;
            YY = y;
            Width = 200;
            Height = 100;
            XXYY = new TranslateTransform() { X = x, Y = y };
            RenderTransform = XXYY;
            Content = text;
            Background = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0, 0, 255));
        }

        

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
