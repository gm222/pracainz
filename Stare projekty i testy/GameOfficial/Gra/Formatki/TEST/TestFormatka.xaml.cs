﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace Gra.Formatki.TEST
{
    public sealed partial class TestFormatka : UserControl
    {
        private Canvas cPlansza;
        private Grid cContentPlotna;
        List<Object> obj2;
        Func<Task<bool>> f;
       
        
        public TestFormatka()
        {
            this.InitializeComponent();
        }

        public TestFormatka(object[] obj) : this()
        {
            this.cPlansza = obj[0] as Canvas;
            this.cContentPlotna = obj[1] as Grid;
            obj2 = obj[2] as List<Object>;
            f = obj[3] as Func<Task<bool>>;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            //plansza.Children.Add(new Button() { RenderTransform = new TranslateTransform() { X = 10, Y = 10 }, Width = 250, Height = 150, Content = "Kliknij mnie", Background = new SolidColorBrush(Windows.UI.Color.FromArgb(255,0,0,255))});
        }
        
        private async void Button_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            
            cContentPlotna.Visibility = Visibility.Visible;

            Canvas.SetZIndex(cContentPlotna, 100);

            

            var pos = Window.Current.CoreWindow.PointerPosition;

            TrickKlasa a = new TrickKlasa((int)pos.X , (int)pos.Y, "Kliknij mnie");
            obj2.Add(a);
            await f();

            lista.Add(a);
            cContentPlotna.Children.Add(a);
            
            await Task.Delay(1);
            Window.Current.Content.PointerReleased += Content_PointerReleased;
        }

        List<TrickKlasa> lista = new List<TrickKlasa>();

        async void Content_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            Window.Current.Content.PointerReleased -= Content_PointerReleased;
 
            cContentPlotna.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            cContentPlotna.Children.Clear();
            cPlansza.Children.Add(lista[0] as TrickKlasa);
            obj2.Clear();
            lista.Clear();
            await Task.Delay(1);
            //a = null;
        }

        
       
    }
}
