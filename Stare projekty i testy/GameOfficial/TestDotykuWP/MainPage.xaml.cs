﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace TestDotykuWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private void Canvas_ManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {
            
            if (e.Cumulative.Scale > 0.50 && e.Cumulative.Scale < 2)
            {
                (((sender as Canvas)).RenderTransform as CompositeTransform).ScaleX = e.Cumulative.Scale;
                (((sender as Canvas)).RenderTransform as CompositeTransform).ScaleY = e.Cumulative.Scale;
            }
            else if(e.Cumulative.Scale >= 2)
            {
                (((sender as Canvas)).RenderTransform as CompositeTransform).ScaleX = 2;
                (((sender as Canvas)).RenderTransform as CompositeTransform).ScaleY = 2;
            }
            else
            {
                (((sender as Canvas)).RenderTransform as CompositeTransform).ScaleX = 0.5;
                (((sender as Canvas)).RenderTransform as CompositeTransform).ScaleY = 0.5;
            }

        }

        private void Canvas_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {

        }

        private void Canvas_PointerPressed(object sender, PointerRoutedEventArgs e)
        {

        }

        private void Canvas_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            if (e.Cumulative.Scale > 0.50 && e.Cumulative.Scale < 2)
            {
                (((sender as Canvas)).RenderTransform as CompositeTransform).ScaleX = e.Cumulative.Scale;
                (((sender as Canvas)).RenderTransform as CompositeTransform).ScaleY = e.Cumulative.Scale;
            }

            (((sender as Canvas)).RenderTransform as CompositeTransform).TranslateX = e.Cumulative.Translation.X;
            (((sender as Canvas)).RenderTransform as CompositeTransform).TranslateY = e.Cumulative.Translation.Y;



        }
    }
}
