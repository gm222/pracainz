﻿using GraTest2.Interface;
using GraTest2.Klasy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Runtime.Serialization;
using System.Xml.Serialization;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace GraTest2
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            this.Loaded += GameStart;
            //DataContractSerializer a = new DataContractSerializer()
            
        }

        #region Globals
            
            bool exit = false;
            GameEngine gSilnik;
            List<IObiekt> listaObiektow = new List<IObiekt>();
        #endregion

        //Methods

        private async void GameStart(object sender, RoutedEventArgs e)
        {
           
            var _translate = new TranslateTransform();
             var transformGroup = new TransformGroup();
            transformGroup.Children.Add(_translate);

            cPlotno.RenderTransform = transformGroup;
            Matrix a23 = new Matrix();
            
            var ticks = Environment.TickCount;
            var lastTime = 0;
            int gTimeStart = 0, NextUpdateTick = 0;
            int TicksToSkip = 1000 / 60;

            gSilnik = new GameEngine();
            await gSilnik.Start();

            gTimeStart = NextUpdateTick = Environment.TickCount;
            
            var rand = new Random();

            int j = 0, k=0;
            for (int i = 0; i < 6000; i++)
            {
                if (j > 3950)
                {
                    j = 0;
                    k += 20;
                }

                var a = new Chipek1(gSilnik, new Point(j+20,k));
                //var a = new Chipek1(gSilnik, new Point(-20, -20));
                listaObiektow.Add(a);
                j += 20;
                this.cPlotno.Children.Add(a.Obraz);

              // await Task.Delay(1);
                
            }



            //foreach (var drawObject in listaObiektow)
            //{
            //    try
            //    {
            //        if (drawObject is Chipek1)
            //            this.cPlotno.Children.Add(((Chipek1)drawObject).Obraz);
            //    }
            //    catch (Exception)
            //    {
                    
                    
            //    }
                
                    
            //}
            this.cPlotno.Children.Add(new Button() { Background = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0, 255, 0)), Content= "SADASFDGDGFDGDFG"});

            Window.Current.Content.KeyDown += SprawdzanieExit;
            CompositionTarget.Rendering += new System.EventHandler<object>(Rysuj);



            while (true && !exit)
            {
                System.Diagnostics.Stopwatch a = System.Diagnostics.Stopwatch.StartNew();
                foreach (var it in listaObiektow)
                {
                    await it.ZmienTlo();

                    if (exit)
                        break;
                }


                a.Stop();

                await Task.Delay(250);
                NextUpdateTick = Environment.TickCount;
            }

            await new Windows.UI.Popups.MessageDialog("Exit").ShowAsync();

        }
        int ile = 0;
        private async void Rysuj(object sender, object e)
        {

            var ilosc = this.listaObiektow.Count();
            int licznik = 0;
            for (int i = 0; i < ilosc; i++)
            {
                if(licznik < 10)
                {
                    await this.listaObiektow[i].Render();
                    licznik++;
                }
                //else
                //{
                //    await this.listaObiektow[i].RenderWylacz();
                //}
                
            }
            
            //foreach (var it in listaObiektow)
            //{
            //    await it.Render();
            //}
        }

        private void SprawdzanieExit(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Escape)
                this.exit = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //this.ile++;

            ile = this.listaObiektow.Count() - 1;
        }
        int a2 = 0;
        private void Button_Click2(object sender, RoutedEventArgs e)
        {
            var a = new Chipek1(gSilnik, new Point(4000 + (a2+=150), 0));
            //listaObiektow.Add(a);
            listaObiektow.Insert(0, a);

            this.cPlotno.Children.Add(a.Obraz);
        }

        private void cPlotno_Tapped(object sender, TappedRoutedEventArgs e)
        {

        }

        Point xd;

        private void cPlotno_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            xd = new Point(e.Position.X, e.Position.Y);
        }

        private void cPlotno_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            
        }

        private void cPlotno_ManipulationStarting(object sender, ManipulationStartingRoutedEventArgs e)
        {

        }

        private void cPlotno_ManipulationInertiaStarting(object sender, ManipulationInertiaStartingRoutedEventArgs e)
        {

        }

        private void cPlotno_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {

        }

        private void cPlotno_ManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {

        }



    }
}
