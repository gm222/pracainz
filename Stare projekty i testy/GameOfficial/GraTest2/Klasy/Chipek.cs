﻿using GraTest2.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace GraTest2.Klasy
{
    class Chipek1 : IObiekt
    {

        

        GameEngine _game;
        int pozycjaObrazek = 0;
        ImageBrush obrazekZrodlo = new ImageBrush();
        Point _pozycja;
        Image _obrazek;
        TranslateTransform _translate;
        Point _vector;




        private WriteableBitmap ObrazekWB
        {
            get
            {
                return this._game.PobierzObrazek(pozycjaObrazek);
            }
        }

        public Image Obraz
        {
            get
            {
                return this._obrazek;
            }
        }

        public Chipek1(GameEngine silnik, Point vector)
        {
            this._game = silnik;
            this._vector = vector;
            this.pozycjaObrazek = new Random().Next(0, this._game.Max - 1);
            this._translate = new TranslateTransform() { X = this._vector.X, Y = this._vector.Y };
            //ScaleTransform asd = new ScaleTransform() { ScaleX = 0.1, ScaleY = 0.1 };
            var transformGroup = new TransformGroup();
            transformGroup.Children.Add(this._translate);
            //transformGroup.Children.Add(asd);


            this._obrazek = new Image() { Source = ObrazekWB, RenderTransform = transformGroup, Width = 20, Height = 20 };
            


            //this._obrazek = new Image() { Source = ObrazekWB, RenderTransform = _translate };


           
        }



        public async Task ZmienTlo()
        {
            


            if (pozycjaObrazek == this._game.Max - 1)
            {
                pozycjaObrazek = 0;
                return;
            }

            pozycjaObrazek++;
        }

        public async Task Render()
        {
            _obrazek.Source = ObrazekWB;
        }

        public async Task RenderWylacz()
        {
            this._obrazek.Source = null;
        }






        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        internal GameEngine GameEngine
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }
    }

    //class asd : Chipek1
    //{
    //    void test()
    //    {
    //        var a = base.GameEngine;
    //    }
    //}

    //class asd1
    //{
    //    public asd a;
    //    public asd1()
    //    {
    //        a = new asd();
    //    }

    //    internal int Age { get; set; }
    //}
}


//namespace test23
//{
//    class asd2
//    {
//        void test()
//        {

//            GraTest2.Klasy.asd1 asd23 = new GraTest2.Klasy.asd1();
            
//        }
//    }
//}
