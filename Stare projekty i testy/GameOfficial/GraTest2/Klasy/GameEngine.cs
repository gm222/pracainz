﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;

namespace GraTest2.Klasy
{
    class GameEngine
    {
        ImageBrush brush = new ImageBrush();
        WriteableBitmap[] animacja;
        private int max;

        delegate void EventHandler();
        event EventHandler e;

        public WriteableBitmap PobierzObrazek(int pozycja)
        {
            return animacja[pozycja];
        }

        public int Max
        {
            get { return this.max; }
        }

        public GameEngine()
        {
            Start();
            //Task.Run(async () => await Start());

            //Window.Current.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, async () =>
            //{
            //    await Start();
            //});
            #region Stary Kod
            //e += Start;
            //e.Invoke();
            #endregion
        }

        //Ładowanie obrazków do tablicy
        //Tworzenie animacji
        public async Task Start()
        {
            byte[] srcPixels;
            var uri = new System.Uri("ms-appx:///Assets/img/wall.png");
            int width = 0;

            using (IRandomAccessStream fileStream = await (await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(uri)).OpenAsync(Windows.Storage.FileAccessMode.Read))
            {
                BitmapDecoder decoder = await BitmapDecoder.CreateAsync(fileStream);
                PixelDataProvider pixelData = await decoder.GetPixelDataAsync(
                                                        BitmapPixelFormat.Bgra8,
                                                        BitmapAlphaMode.Straight,
                                                            new BitmapTransform(),
                                                        ExifOrientationMode.IgnoreExifOrientation,
                                                        ColorManagementMode.DoNotColorManage);
                width = (int)decoder.PixelWidth;
                srcPixels = pixelData.DetachPixelData();
            }

            List<WriteableBitmap> animacjaTym = new List<WriteableBitmap>();

            for (int i = 0; i < width / 200; i++)
            {
                byte[] obrazek = new byte[200 * 200 * 4];

                for (int j = 0; j < 200; j++)
                    for (int k = 0; k < 200; k++)
                    {
                        obrazek[(j * 200 + k) * 4 + 0] = srcPixels[(j * width + k + i * 200) * 4 + 0];
                        obrazek[(j * 200 + k) * 4 + 1] = srcPixels[(j * width + k + i * 200) * 4 + 1];
                        obrazek[(j * 200 + k) * 4 + 2] = srcPixels[(j * width + k + i * 200) * 4 + 2];
                        obrazek[(j * 200 + k) * 4 + 3] = srcPixels[(j * width + k + i * 200) * 4 + 3];
                    }
                WriteableBitmap obrazekMapa = new WriteableBitmap(200, 200);

                using (Stream stream = obrazekMapa.PixelBuffer.AsStream())
                {
                    await stream.WriteAsync(obrazek, 0, obrazek.Length);
                }
                animacjaTym.Add(obrazekMapa);
            }
            max = animacjaTym.Count;
            animacja = animacjaTym.ToArray();
        }




    }
}



