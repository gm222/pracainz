﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraTest2.Interface
{
    interface IObiekt
    {
        Task Render();
        Task ZmienTlo();
        Task RenderWylacz();
    }
}
