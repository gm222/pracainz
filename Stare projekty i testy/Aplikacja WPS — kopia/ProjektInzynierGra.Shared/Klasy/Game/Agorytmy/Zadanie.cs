﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Collections.ObjectModel;
using System.IO;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using System.Runtime.Serialization;
using ProjektInzynierGra.Klasy.Budynki;
using ProjektInzynierGra.Klasy.Postacie;

namespace ProjektInzynierGra.Klasy.Game.AgorytmyFunckje
{
    [DataContract]
    [KnownType(typeof(Budynek))]
    [KnownType(typeof(Postac))]
    [KnownType(typeof(Straz))]
    [KnownType(typeof(DomekMieszkalny))]
    [KnownType(typeof(Mieszkaniec))]
    [KnownType(typeof(MieszkaniecPrzychodzacy))]
    [KnownType(typeof(Strazak))]
    class Zadania
    {
        [DataMember]
        int licznik = 0;
        [DataMember]
        int czas_wykonywania = 0;
        [IgnoreDataMember]
        Action zadanie = null;
        [IgnoreDataMember]
        private object obiekt;
        [DataMember]
        private string nazwaFunkcji;
        [DataMember]
        private ulong nrObiektu;
        [DataMember]
        private bool czyPostac;

        public bool CzyPostac 
        {
            get { return this.czyPostac; }  
        }
        public ulong NrObiektu 
        {
            get { return this.nrObiektu; } 
        }
        public string NazwaFunkcji 
        {
            get { return this.nazwaFunkcji; }
            set { this.nazwaFunkcji = value; } 
        }

        //bool doUsuniecia = false; //zastanowic sie czy moze byc
        //public bool ObiektDoUsuniecia
        //{
        //    get { return this.doUsuniecia; }
        //    private set { this.doUsuniecia = value; } 
        //} //zastanowic sie czy moze byc
        public object Obiekt
        {
            get { return obiekt; }
            set { obiekt = value; }
        }

        public bool ZadanieGotoweDoOdpalenia
        { 
            get
            {
                return czas_wykonywania - licznik <= 0 ? true : false;
            }
        }

        public Action Zadanie 
        { 
            get
            {
                return this.zadanie;
            }
            set
            {
                this.zadanie = value;
            }
            
        }

        public void ZwiekszLicznikCzasu()
        {
            licznik++;
        }

        public Zadania(Action zad, object ob, ulong nr, int czas = 0, bool czyPostac = false)
        {
            // TODO: Complete member initialization
            this.zadanie = zad;
            this.obiekt = ob;
            this.nrObiektu = nr;
            this.czyPostac = czyPostac;
            this.czas_wykonywania = czas;
            this.nazwaFunkcji = zad.GetMethodInfo().Name;
        }

        ~Zadania()
        {
            this.obiekt = null;
            this.zadanie = null;
            this.nazwaFunkcji = null;
        }

        public bool ZadanieSpr()
        {
            if(licznik++ >= czas_wykonywania)
            {
                zadanie();
                return true;
            }
            else
                return false;
        }

        public static Delegate WydajZadanie(Type obj, string nazwa_zadania, object target, Type delegat)
        {
            MethodInfo objInfo = null;

            var x = obj.GetRuntimeMethods().ToList();
            foreach(var i in x)
            {
                if(i.Name == nazwa_zadania)
                {
                    objInfo = i;
                    break;
                }
            }

            return objInfo.CreateDelegate(delegat, target);
        }
    }
}
