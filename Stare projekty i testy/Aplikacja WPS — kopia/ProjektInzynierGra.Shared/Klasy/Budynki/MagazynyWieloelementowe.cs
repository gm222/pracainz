﻿using ProjektInzynierGra.Klasy.Postacie;
using System;

namespace ProjektInzynierGra.Klasy.Budynki
{
    public class Magazyn : MagazynyWieloelementowe
    {
        public override void UsunBudynek()
        {
            throw new NotImplementedException();
        }

        public override void UsunPostac(Postacie.Postac post)
        {
            throw new NotImplementedException();
        }
    }

    public abstract class MagazynyWieloelementowe : Budynek
    {
        
        private Int16 pojemnoscMagazynu;
        private Boolean typLadunku;

        public Int16 PojemnoscMagazynu
        {
            get { return this.pojemnoscMagazynu; }
            set { this.pojemnoscMagazynu = value; }
        }

        public Boolean TypLadunku
        {
            get { return this.typLadunku; }
            set { this.typLadunku = value; }
        }

        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }

        
    }

    public class Spichlerz : MagazynyWieloelementowe
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class Stragan : MagazynyWieloelementowe
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }
}
