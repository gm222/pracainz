﻿using ProjektInzynierGra.Klasy;
using ProjektInzynierGra.Klasy.Game;
using ProjektInzynierGra.Klasy.Game.AlgorytmyFunckje;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace ProjektInzynierGra
{
    public sealed partial class MainPage
    {
        private GameEngine gSilnik;
        private List<TypeInfo> listaForatek;
        private List<Type> listaformatek2 = new List<Type>();
        private FunkcjeXAML wlasciwosciMainPage = null;

        event EventHandler<object> gotowosc; //zmienic
        private bool Inicializuj()
        {
            if(!load)
                this.Loaded += GameStart;

            return true;
        }

        private async void GameStartLoaded(object sender, RoutedEventArgs e)
        {
            /*
             * try
            {
                wlasciwosciMainPage = new ProjektInzynierGra.Klasy.FunkcjeXAML(this.pageRoot, this.gGlowny, this.gMiniMapka, this.gMenu, this.borMiniMapka, this.bMenuButton, this.Hide, this.Visible, this.cContentPlotna, this.cMyszkaContent, this.cPlansza, this.cDroga, cBudynki, cNowaDroga);

                if(!await App.gameConstans.InicializujeAnimacje())
                {
                    return;
                }

                obiekty = new object[]
                {
                    wlasciwosciMainPage,
                    gSilnik
                };
                Algorytmy.UstawGameEngine(gSilnik);
                gSilnik.FunkcjeFormatki(this.wlasciwosciMainPage);
                this.gSilnik.PrzywrocDane();

                var deftypes = Assembly.Load(new AssemblyName(Windows.ApplicationModel.Package.Current.DisplayName));
                listaForatek = deftypes.DefinedTypes.ToList().Where(x => x.Namespace.Contains("Formatki")).ToList<TypeInfo>();
                foreach(var it in listaForatek)
                {
                    var typ = it.AsType();
                    listaformatek2.Add(typ);
                }
            }
            catch(Exception)
            {
                return;
            }

            // var status = await gSilnik.InicjalizacjaAnimacji();

            //if(status[0] == false)
            //{
            //    await new Windows.UI.Popups.MessageDialog("Błąd ładowania animacji").ShowAsync();
            //    this.navigationHelper.GoBack();
            //    return;
            //}
            gloadingGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.gSilnik.GameStart();*/

            try
            {
                wlasciwosciMainPage = new FunkcjeXAML(this.page, this.gGlowny, null, null/*this.gMenu*/, null, null/*this.bMenuButton*/, null/*this.Hide*/, null/*this.Visible*/, this.cContentPlotna, this.cMyszkaContent, this.cPlansza, this.cDroga, cBudynki, cNowaDroga);
                if(!await App.gameConstans.InicializujeAnimacje())
                {
                    return;
                }

                obiekty = new object[]
                {
                    wlasciwosciMainPage,
                    gSilnik
                };
                Algorytmy.UstawGameEngine(gSilnik);
                gSilnik.FunkcjeFormatki(this.wlasciwosciMainPage);
                this.gSilnik.PrzywrocDane();


                var deftypes = Assembly.Load(new AssemblyName("ProjektInzynierGra.WindowsPhone"));
                listaForatek = deftypes.DefinedTypes.ToList().Where(x => x.Namespace.Contains("Formatki")).ToList<TypeInfo>();
                foreach(var it in listaForatek)
                {
                    var typ = it.AsType();
                    listaformatek2.Add(typ);
                }

            }
            catch(Exception)
            {
                return;
            }

            // var status = await gSilnik.InicjalizacjaAnimacji();

            //if(status[0] == false)
            //{
            //    await new Windows.UI.Popups.MessageDialog("Błąd ładowania animacji").ShowAsync();
            //    this.navigationHelper.GoBack();
            //    return;
            //}
           // gloadingGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            //gloadingGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.gSilnik.GameStart();
        }

        public static void cNowaDroga_PointerMoved(object sender, PointerRoutedEventArgs e)
        {

            throw new NotImplementedException();
        }

        //async void MainPage_gotowosc(object sender, object e)
        //{
        //    //await GameStart(null, null);
        //}

        //Główny silnik gry
        object obiekty;
        private async void GameStart(object sender, RoutedEventArgs e)
        {



            await Task.Delay(300); //zaladowanie GUI

            wlasciwosciMainPage = new FunkcjeXAML(this.page, this.gGlowny, null, null/*this.gMenu*/, null, null/*this.bMenuButton*/, null/*this.Hide*/, null/*this.Visible*/, this.cContentPlotna, this.cMyszkaContent, this.cPlansza, this.cDroga, cBudynki, cNowaDroga);

            if(!(await App.gameConstans.LoadSettings()))
            {
                return;
            }

            if(!await App.gameConstans.InicializujeAnimacje())
            {
                return;
            }

            obiekty = new object[]
            {
                wlasciwosciMainPage,
                gSilnik
            };

            try
            {
                var deftypes = Assembly.Load(new AssemblyName("ProjektInzynierGra.WindowsPhone"));
                listaForatek = deftypes.DefinedTypes.ToList().Where(x => x.Namespace.Contains("Formatki")).ToList<TypeInfo>();
                foreach(var it in listaForatek)
                {
                    var typ = it.AsType();
                    listaformatek2.Add(typ);
                }
            }
            catch(Exception)
            {
                return;
            }

            // var status = await gSilnik.InicjalizacjaAnimacji();

            //if(status[0] == false)
            //{
            //    await new Windows.UI.Popups.MessageDialog("Błąd ładowania animacji").ShowAsync();
            //    this.navigationHelper.GoBack();
            //    return;
            //}
            Algorytmy.UstawGameEngine(gSilnik);
            gSilnik.FunkcjeFormatki(this.wlasciwosciMainPage);
            gSilnik.GameStart();
            //gloadingGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            //gloadingGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

        }

        private async void Formatka_Click(object sender, RoutedEventArgs e)
        {
            var tag = (sender as Button).Tag.ToString();
            var bladS = "";
            var bladB = false;

            try
            {
                var formatka = listaForatek.Where(x => x.Name == tag).FirstOrDefault();
                var typ = formatka.AsType();

                var index = this.listaForatek.IndexOf(formatka);


                //this.fFrame.Navigate(listaformatek2[index], obiekty);

                return;
            }
            catch(Exception ex)
            {
                bladB = true;
                bladS = ex.Message;
            }

            if(bladB)
                await new Windows.UI.Popups.MessageDialog(bladS).ShowAsync();
        }

    }
}
