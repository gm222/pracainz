﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace ProjektInzynierGra
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class StartPage : Page
    {
        public StartPage()
        {
            this.InitializeComponent();
            this.Loaded += StartPage_Loaded;
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;

            //pageTitle.FontFamily = new FontFamily(@"\Assets\Fonts\CASTELAR.TTF#CASTELAR");

        }

        private async void StartPage_Loaded(object sender, RoutedEventArgs e)
        {
            await Windows.UI.ViewManagement.StatusBar.GetForCurrentView().HideAsync();
        }

        private async void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            e.Handled = true;
            var pytanieBox = new Windows.UI.Popups.MessageDialog("Czy na pewno chcesz opuścić grę ?", "Pytanie");

            bool? result = null;
            pytanieBox.Commands.Add(
               new UICommand("Tak", new UICommandInvokedHandler((cmd) => result = true)));
            pytanieBox.Commands.Add(
               new UICommand("Nie", new UICommandInvokedHandler((cmd) => result = false)));

            await pytanieBox.ShowAsync();

            if(result == null || result == false)
            {
                e.Handled = true;
                return;
            }
            Application.Current.Exit();
            e.Handled = false;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
        }

        private void NewGame_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(KampanieLadowanie));
        }

        private void Info_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Ustawienia_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
