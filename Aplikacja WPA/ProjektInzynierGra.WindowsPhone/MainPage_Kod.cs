﻿using ProjektInzynierGra.Klasy;
using ProjektInzynierGra.Klasy.Game;
using ProjektInzynierGra.Klasy.Game.AlgorytmyFunckje;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace ProjektInzynierGra
{
    public sealed partial class MainPage
    {
        private GameEngine gSilnik;
        private List<TypeInfo> listaForatek;
        private List<Type> listaformatek2 = new List<Type>();
        private FunkcjeXAML wlasciwosciMainPage = null;
        private bool panel1Open = false;
        private bool panel2Open = false;

        event EventHandler<object> gotowosc; //zmienic
        private bool Inicializuj()
        {
            if(!load)
                this.Loaded += GameStart;

            return true;
        }

        private async void GameStartLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                wlasciwosciMainPage = new FunkcjeXAML(this.page, this.gGlowny, null, null/*this.gMenu*/, null, null/*this.bMenuButton*/, null/*this.Hide*/, null/*this.Visible*/, this.cContentPlotna, this.cMyszkaContent, this.cPlansza, this.cDroga, cBudynki, cNowaDroga);

                if(!await App.gameConstans.InicializujeAnimacje())
                {
                    return;
                }

                obiekty = new object[]
                {
                    wlasciwosciMainPage,
                    gSilnik
                };

                Algorytmy.UstawGameEngine(gSilnik);
                gSilnik.FunkcjeFormatki(this.wlasciwosciMainPage);
                this.gSilnik.PrzywrocDane();

                var deftypes = Assembly.Load(new AssemblyName("ProjektInzynierGra.WindowsPhone"));
                listaForatek = deftypes.DefinedTypes.ToList().Where(x => x.Namespace.Contains("Formatki")).ToList<TypeInfo>();

                foreach(var it in listaForatek)
                {
                    var typ = it.AsType();
                    listaformatek2.Add(typ);
                }

            }
            catch(Exception)
            {
                return;
            }

            gloadingGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            this.gSilnik.GameStart();
        }

        public static void cNowaDroga_PointerMoved(object sender, PointerRoutedEventArgs e)
        {

            throw new NotImplementedException();
        }

        //async void MainPage_gotowosc(object sender, object e)
        //{
        //    //await GameStart(null, null);
        //}

        //Główny silnik gry
        object obiekty;
        private async void GameStart(object sender, RoutedEventArgs e)
        {
            await Task.Delay(300); //zaladowanie GUI

            wlasciwosciMainPage = new FunkcjeXAML(this.page, this.gGlowny, null, null/*this.gMenu*/, null, null/*this.bMenuButton*/, null/*this.Hide*/, null/*this.Visible*/, this.cContentPlotna, this.cMyszkaContent, this.cPlansza, this.cDroga, cBudynki, cNowaDroga);

            if(!(await App.gameConstans.LoadSettings()))
            {
                return;
            }

            if(!await App.gameConstans.InicializujeAnimacje())
            {
                return;
            }

            obiekty = new object[] 
            {
                wlasciwosciMainPage,
                gSilnik
            };

            try
            {
                var deftypes = Assembly.Load(new AssemblyName("ProjektInzynierGra.WindowsPhone"));
                listaForatek = deftypes.DefinedTypes.ToList().Where(x => x.Namespace.Contains("Formatki")).ToList<TypeInfo>();
                foreach(var it in listaForatek)
                {
                    var typ = it.AsType();
                    listaformatek2.Add(typ);
                }
            }
            catch(Exception)
            {
                return;
            }

            Algorytmy.UstawGameEngine(gSilnik);
            gSilnik.FunkcjeFormatki(this.wlasciwosciMainPage);
            gSilnik.GameStart();
            gloadingGrid.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

        }

        private async void Formatka_Click(object sender, RoutedEventArgs e)
        {
            var tag = (sender as Button).Tag.ToString();
            var bladS = "";
            var bladB = false;

            try
            {
                var formatka = listaForatek.Where(x => x.Name == tag).FirstOrDefault();
                var typ = formatka.AsType();

                var index = this.listaForatek.IndexOf(formatka);

                this.fFrame.Navigate(listaformatek2[index], obiekty);

                ZamknijZakladkiPaneluBocznego.Completed += ZamknijZakladkiPaneluBocznego_Completed;
                ZamknijZakladkiPaneluBocznego.Begin();

                return;
            }
            catch(Exception ex)
            {
                bladB = true;
                bladS = ex.Message;
            }

            if(bladB)
                await new Windows.UI.Popups.MessageDialog(bladS).ShowAsync();
        }

        void ZamknijZakladkiPaneluBocznego_Completed(object sender, object e)
        {
            panel1Open = false;
            ZamknijZakladkiPaneluBocznego.Completed -= ZamknijZakladkiPaneluBocznego_Completed;
            panel2Open = true;
            OtworzFramePaneluBocznego.Begin();
        }

    }
}
