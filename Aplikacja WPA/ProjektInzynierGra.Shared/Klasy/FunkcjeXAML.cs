﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

namespace ProjektInzynierGra.Klasy
{
    public class FunkcjeXAML
    {
        private DispatcherTimer timer;
        private PointerRoutedEventArgs pointerEvent;

        private double miniMapkaWidth  = 0;
        private double miniMapkaHeight = 0;

        private bool wlaczony         = false;
        private bool miniMapkaPressed = false;
        private bool buttonWysuniety  = false;
        private bool firstSlider      = true;

        private Grid gMiniMapka;
        private Border borMiniMapka;
        private Button bMenuButton;
        private Storyboard ukryjButton;
        private Storyboard pokazButton;
        private Canvas cContentPlotna;
        private Canvas cMyszkaContent;
        private Canvas cPlansza;
        private Canvas cDroga;
        private Canvas cBudynki;
        private Canvas cNowaDroga;
        private Grid gGlowny;
        private Grid gMenu;
        private Page pageRoot;

        
        public Page PageRoot
        {
            get { return pageRoot; }
            set { pageRoot = value; }
        }
        public Grid MenuGrid
        {
            get { return this.gMenu; }
            set { this.gMenu = value; } 
        }
        public Grid GlownyGrid
        {
            get { return gGlowny; }
            set { gGlowny = value; }
        }
        public Canvas ContentPlotnaCanvas
        {
            get { return cContentPlotna; }
            set { cContentPlotna = value; }
        }
        public Canvas PlanszaCanvas
        {
            get { return cPlansza; }
            set { cPlansza = value; }
        }
        public Canvas DrogaCanvas
        {
            get { return cDroga; }
            set { cDroga = value; }
        }
        public Canvas BudynkiCanvas
        {
            get { return cBudynki; }
            set { cBudynki = value; }
        }
        public Canvas NowaDrogaCanvas
        {
            get { return cNowaDroga; }
            set { cNowaDroga = value; }
        }
        public Canvas MyszkaContentCanvas
        {
            get { return cMyszkaContent; }
            set { cMyszkaContent = value; }
        }
        public Grid MiniMapkaGrid
        {
            get { return gMiniMapka; }
            set { gMiniMapka = value; }
        }



        public FunkcjeXAML(Page pageRoot, Grid gGlowny, Grid gMiniMapka, Grid gMenu, Border borMiniMapka, Button bMenuButton, Storyboard ukryjButton, Storyboard pokazButton, Canvas cContentPlotna, Canvas cMyszkaContent, Canvas cPlansza, Canvas cDroga, Canvas cBudynki, Canvas cNowaDroga)
        {
            this.pageRoot       = pageRoot;
            this.gGlowny        = gGlowny;
            this.gMiniMapka     = gMiniMapka;
            this.gMenu          = gMenu;
            this.borMiniMapka   = borMiniMapka;
            this.bMenuButton    = bMenuButton;
            this.ukryjButton    = ukryjButton;
            this.pokazButton    = pokazButton;
            this.cContentPlotna = cContentPlotna;
            this.cPlansza       = cPlansza;
            this.cMyszkaContent = cMyszkaContent;
            this.cDroga         = cDroga;
            this.cBudynki       = cBudynki;
            this.cNowaDroga     = cNowaDroga;

            this.ContentPlotnaCanvas.Width = Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Bounds.Width;
            this.ContentPlotnaCanvas.Height = Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Bounds.Height;

            this.cMyszkaContent.Width = Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Bounds.Width;
            this.cMyszkaContent.Height = Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Bounds.Height;

            //this.pageRoot.SizeChanged       += Okno_Page_SizeChanged;
            this.gGlowny.PointerMoved       += GridgGlowny_PointerMoved;

            //if(gMiniMapka != null)
            //{
            //    this.gMiniMapka.PointerPressed  += GridgMiniMapka_PointerPressed;
            //    this.gMiniMapka.PointerReleased += GridgMiniMapka_PointerReleased;
            //    this.gMiniMapka.PointerMoved    += GridgMiniMapka_PointerMoved;
            //    this.gMiniMapka.PointerReleased += GridgMiniMapka_PointerOpuszczenie;
            //}
            //this.bMenuButton.Click          += bMenuButton_Click;

            //Okno_Page_SizeChanged(null, null);

        }



        private SolidColorBrush[] color = new SolidColorBrush[]
        { 
            new SolidColorBrush(Windows.UI.Color.FromArgb(150, 255, 0, 0)), 
            new SolidColorBrush(Windows.UI.Color.FromArgb(150, 0, 255, 0)) 
        };

        private void timerTick(object sender, object e)
        {
            double X = pointerEvent.GetCurrentPoint(this.pageRoot).Position.X;
            double Y = pointerEvent.GetCurrentPoint(this.pageRoot).Position.Y;
            double TranslateX = (cPlansza.RenderTransform as CompositeTransform).TranslateX;
            double TranslateY = (cPlansza.RenderTransform as CompositeTransform).TranslateY;
            double przekatna = cPlansza.Width * Math.Sqrt(2);
            bool zmiana = false;
            if(X < 30)
            {
                zmiana = true;
                if(TranslateX + 25 > 0)
                    (cPlansza.RenderTransform as CompositeTransform).TranslateX = 0;
                else
                    (cPlansza.RenderTransform as CompositeTransform).TranslateX = TranslateX + 25;
            }
            if(X > this.pageRoot.ActualWidth - 2)
            {
                zmiana = true;
                if(TranslateX - 25 < -(przekatna + 400 - this.pageRoot.ActualWidth))
                    (cPlansza.RenderTransform as CompositeTransform).TranslateX = -(przekatna + 400 - this.pageRoot.ActualWidth);
                else
                    (cPlansza.RenderTransform as CompositeTransform).TranslateX = TranslateX - 25;
            }
            if(Y < 30)
            {
                zmiana = true;
                if(TranslateY + 25 > 0)
                    (cPlansza.RenderTransform as CompositeTransform).TranslateY = 0;
                else
                    (cPlansza.RenderTransform as CompositeTransform).TranslateY = TranslateY + 25;
            }
            if(Y > this.pageRoot.ActualHeight - 30)
            {
                zmiana = true;
                if(TranslateY - 25 < -(przekatna - this.pageRoot.ActualHeight))
                    (cPlansza.RenderTransform as CompositeTransform).TranslateY = -(przekatna - this.pageRoot.ActualHeight);
                else
                    (cPlansza.RenderTransform as CompositeTransform).TranslateY = TranslateY - 25;
            }
            if(zmiana == false)
            {
                timer.Stop();
                wlaczony = false;
            }

            var width = Windows.UI.Xaml.Window.Current.Bounds.Width;
            var height = Windows.UI.Xaml.Window.Current.Bounds.Height;
            (borMiniMapka.RenderTransform as CompositeTransform).TranslateX = -(cPlansza.RenderTransform as CompositeTransform).TranslateX / (width / (300 * width / przekatna));
            (borMiniMapka.RenderTransform as CompositeTransform).TranslateY = -(cPlansza.RenderTransform as CompositeTransform).TranslateY / (height / (300 * height / przekatna));
        }
        private void ZmianaPolozeniaMiniMapki(object sender)
        {
            var tym = (borMiniMapka.RenderTransform as CompositeTransform);
            double x = pointerEvent.GetCurrentPoint(sender as Grid).Position.X;
            if(x - miniMapkaWidth < 0)
                tym.TranslateX = 0;
            else if(x + miniMapkaWidth > 300)
                tym.TranslateX = 300 - 2 * miniMapkaWidth;
            else
                tym.TranslateX = x - miniMapkaWidth;

            double y = pointerEvent.GetCurrentPoint(sender as Grid).Position.Y;
            if(y - miniMapkaHeight < 0)
                tym.TranslateY = 0;
            else if(y + miniMapkaHeight > 300)
                tym.TranslateY = 300 - 2 * miniMapkaHeight;
            else
                tym.TranslateY = y - miniMapkaHeight;

            double przekatna = cPlansza.Width * Math.Sqrt(2);
            var width = this.pageRoot.ActualWidth;
            var height = this.pageRoot.ActualHeight;
            (cPlansza.RenderTransform as CompositeTransform).TranslateX = -(borMiniMapka.RenderTransform as CompositeTransform).TranslateX * (width / (300 * width / przekatna));
            (cPlansza.RenderTransform as CompositeTransform).TranslateY = -(borMiniMapka.RenderTransform as CompositeTransform).TranslateY * (height / (300 * height / przekatna));
        }

        private void Okno_Page_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var width = this.pageRoot.ActualWidth;
            var height = this.pageRoot.ActualHeight;
            double przekatna = cPlansza.Width * Math.Sqrt(2);

            borMiniMapka.Width = 300 * (width - 400) / przekatna;
            borMiniMapka.Height = 300 * height / przekatna;

            miniMapkaWidth = (300 * (width - 400) / przekatna) / 2;
            miniMapkaHeight = (300 * height / przekatna) / 2;

            (borMiniMapka.RenderTransform as CompositeTransform).TranslateX = -(cPlansza.RenderTransform as CompositeTransform).TranslateX / (width / (300 * width / przekatna));
            (borMiniMapka.RenderTransform as CompositeTransform).TranslateY = -(cPlansza.RenderTransform as CompositeTransform).TranslateY / (height / (300 * height / przekatna));
        }

        private void GridgGlowny_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            if(wlaczony == false)
            {
                pointerEvent = e;
                double X = pointerEvent.GetCurrentPoint(this.pageRoot).Position.X;
                double Y = pointerEvent.GetCurrentPoint(this.pageRoot).Position.Y;
                timer = new DispatcherTimer();
                if(X < 30 || X > this.pageRoot.ActualWidth-2 || Y < 30 || Y > this.pageRoot.ActualHeight - 30)
                {
                    timer.Interval = new TimeSpan(0, 0, 0, 0, 1);
                    timer.Tick += new EventHandler<object>(timerTick);
                    timer.Start();
                    wlaczony = true;
                }
            }
        }

        private void GridgMiniMapka_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            miniMapkaPressed = true;
            Window.Current.Content.PointerReleased += GridgMiniMapka_PointerReleased;
            ZmianaPolozeniaMiniMapki(sender);
        }

        private void GridgMiniMapka_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            miniMapkaPressed = false;
            Window.Current.Content.PointerReleased -= GridgMiniMapka_PointerReleased;
        }

        private void GridgMiniMapka_PointerMoved(object sender, PointerRoutedEventArgs e)
        {
            if(miniMapkaPressed == true)
                ZmianaPolozeniaMiniMapki(sender);
        }

        private void GridgMiniMapka_PointerOpuszczenie(object sender, PointerRoutedEventArgs e)
        {
            miniMapkaPressed = false;
        }

        private void bMenuButton_Click(object sender, RoutedEventArgs e)
        {
            if(buttonWysuniety == false)
            {
                ukryjButton.Begin();
                buttonWysuniety = true;
            }
            else
            {
                pokazButton.Begin();
                buttonWysuniety = false;
            }
        }

        private void Slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            if(firstSlider == true)
            {
                firstSlider = false;
                return;
            }

            (cPlansza.RenderTransform as CompositeTransform).ScaleY = (cPlansza.RenderTransform as CompositeTransform).ScaleX = (e.NewValue / 100);
        }

    }
}
