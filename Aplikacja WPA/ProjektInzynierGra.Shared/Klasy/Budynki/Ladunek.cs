﻿using ProjektInzynierGra.Klasy.Postacie;
using System;

namespace ProjektInzynierGra.Klasy.Budynki
{
    public class ChataPasterska : Ladunek
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class DomekMysliwski : Ladunek
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class DomekOliwny : Ladunek
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class DomekRybacki : Ladunek
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class DrzewoOliwne : Pole
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public abstract class Farma : Pole
    {
        

        private Boolean typZwierzat;

        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }

       

        public Boolean TypZwierzat
        {
            get { return this.typZwierzat; }
            set { this.typZwierzat = value; }
        }

       
    }

    public class FarmaKoz : Farma
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }

    }

    public class FarmaOwcow : Farma
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public abstract class Kopalnia : Ladunek
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class KopalniaRudy : Kopalnia
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public abstract class Ladunek : Budynek
    {
        

        private Int16 pojemnoscMagazynu;
        private Boolean typLadunku;

        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
       
        public Int16 PojemnoscMagazynu
        {
            get { return this.pojemnoscMagazynu; }
            set { this.pojemnoscMagazynu = value; }
        }

        public Boolean TypLadunku
        {
            get { return this.typLadunku; }
            set { this.typLadunku = value; }
        }

       
    }

    public class Mielnica : Kopalnia
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class Mleczarnia : Ladunek
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public abstract class Pole : Ladunek
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class ProdukcjaLukow : Ladunek
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class StadninaKoni : Ladunek
    {

        private Int16 iloscKoni;

        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }

        public Int16 IloscKoni
        {
            get { return this.iloscKoni; }
            set { this.iloscKoni = value; }
        }
    }

    public class Tartak : Ladunek
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class Winnica : Ladunek
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class Winorosle : Pole
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }

    public class Zbrojownia : Ladunek
    {
        public override void UsunBudynek() { }
        public override void UsunPostac(Postac post) { }
    }
}
