﻿using ProjektInzynierGra.Klasy.Game;
using ProjektInzynierGra.Klasy.Postacie;
using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Windows.Foundation;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

namespace ProjektInzynierGra.Klasy.Budynki
{
    [DataContract]
    [KnownType(typeof(Zwykle))]
    [KnownType(typeof(BudynkiMieszkalne))]
    [KnownType(typeof(DomekMieszkalny))]
    [KnownType(typeof(Straz))]
    public abstract class Budynek
    {
        [DataMember]
        internal ulong id = 0;
        [DataMember]
        internal byte idWlasciwosci = 0;
        [DataMember]
        internal Int16 maxWysOsob;
        [DataMember]
        internal Int16 maxPojemnoscOsob;
        [DataMember]
        internal Point lokalizacja; //tutaj X i Y
        [DataMember]
        internal Point iloscZajetychPol;
        [DataMember]
        internal Int16 hp = 100;
        [DataMember]
        internal Int16 maxHP = 100;
        [DataMember]
        internal byte hpLost = 0;
        [IgnoreDataMember]
        internal Rectangle obrazek;
        [IgnoreDataMember]
        internal ImageBrush obrazekIB;
        [IgnoreDataMember]
        internal CompositeTransform obrazekCT;
        [IgnoreDataMember]
        internal GameEngine.UsunBudynekHandler usunBudynekEvent;
        [IgnoreDataMember]
        internal GameEngine.UsunPostacHandler usunPostacEvent;
        [IgnoreDataMember]
        internal GameEngine.UsunZadanieHandler usunZadanieEvent;
        [IgnoreDataMember]
        internal GameEngine.DodajZadanieHandler dodajZadanieEvent;
        [IgnoreDataMember]
        internal GameEngine.DodajPostacHandler dodajPostacEvent;
        [IgnoreDataMember]
        internal GameEngine.UkladMapyHandler pobierzZUkladuMapy;
        [IgnoreDataMember]
        internal ObservableCollection<Postacie.Postac> listaNiepracujacych;
        [IgnoreDataMember]
        internal ObservableCollection<Postacie.Postac> listaPracujacych;


        public virtual void UsunBudynek() { }
        public virtual void UsunPostac(Postac post) { }
        [DataMember]
        private bool usuwalny;
        [IgnoreDataMember]
        internal GameEngine.PobierzPostacHandler pobierzPostac;
        [DataMember]
        public bool Usuwalny
        {
            get { return usuwalny; }
            set { usuwalny = value; }
        }

        public virtual void PrzywrocObrazek() 
        {
            this.obrazekCT = new CompositeTransform()
            {
                TranslateX = 0,
                TranslateY = 0,
                CenterX = .5f,
                CenterY = .5f
            };
            
        }

        public virtual void PrzywrocDane() { }

        public void DodajDane(params object[] obj)
        {
            this.dodajZadanieEvent   = obj[0] as GameEngine.DodajZadanieHandler;
            this.dodajPostacEvent    = obj[1] as GameEngine.DodajPostacHandler;
            this.usunBudynekEvent    = obj[2] as GameEngine.UsunBudynekHandler;
            this.usunZadanieEvent    = obj[3] as GameEngine.UsunZadanieHandler;
            this.usunPostacEvent     = obj[4] as GameEngine.UsunPostacHandler;
            this.listaNiepracujacych = obj[5] as ObservableCollection<Postac>;
            this.listaPracujacych    = obj[6] as ObservableCollection<Postac>;
            this.pobierzPostac       = obj[7] as GameEngine.PobierzPostacHandler;
            this.pobierzZUkladuMapy  = obj[8] as GameEngine.UkladMapyHandler;
        }
        [DataMember]
        public Point IloscZajetychPol
        {
            get
            {
                return this.iloscZajetychPol;
            }
            set
            {
                this.iloscZajetychPol = value;
            }
        }
        [IgnoreDataMember]
        public ImageSource ObrazeKSource 
        {
            get { return this.obrazekIB.ImageSource; } 
        }
        [IgnoreDataMember]
        public String TypBudynku 
        { 
            get
            {
                return this.GetType().Name;
            }
        }


    }
}