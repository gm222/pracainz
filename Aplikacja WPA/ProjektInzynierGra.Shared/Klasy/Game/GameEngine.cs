﻿using ProjektInzynierGra.Klasy.Budynki;
using ProjektInzynierGra.Klasy.Game.AgorytmyFunckje;
using ProjektInzynierGra.Klasy.Postacie;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Imaging;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace ProjektInzynierGra.Klasy.Game
{
    [DataContract]
    [KnownType(typeof(Budynek))]
    [KnownType(typeof(Postacie.Postac))]
    [KnownType(typeof(DomekMieszkalny))]
    [KnownType(typeof(Straz))]
    [KnownType(typeof(Strazak))]
    [KnownType(typeof(Postacie.Mieszkaniec))]
    [KnownType(typeof(Postacie.MieszkaniecPrzychodzacy))]
    public class GameEngine : INotifyPropertyChanged
    {
        //delegaty
        public delegate void DodajZadanieHandler(Action zadanie, object ob, ulong nr, int czas = 0, bool czyPostac = false);
        public delegate void DodajBudynkiHandler(ProjektInzynierGra.Klasy.Budynki.Budynek bud);
        public delegate void DodajPostacHandler(ProjektInzynierGra.Klasy.Postacie.Postac post);
        public delegate void UsunPostacHandler(ProjektInzynierGra.Klasy.Postacie.Postac post, byte nrListy = 0);
        public delegate void UsunBudynekHandler(ProjektInzynierGra.Klasy.Budynki.Budynek bud);
        public delegate void UsunZadanieHandler(object obj);
        public delegate Postac PobierzPostacHandler(ulong id);
        public delegate int UkladMapyHandler(int z, int x, int y);
        
        //eventy
        public event PropertyChangedEventHandler PropertyChanged;
        public DodajZadanieHandler               dodajZadanie;
        public DodajBudynkiHandler               dodajBudynki;
        public DodajPostacHandler                dodajPostacie;
        public UsunPostacHandler                 usunPostac;
        public UsunBudynekHandler                usunBudynek;
        public UsunZadanieHandler                usunZadanie;
        public PobierzPostacHandler              pobierzPostac;
        public UkladMapyHandler                  pobierzZUkladuMapy;
        //z = 0 wtedy jest to z index
        //z = 1 wtedy droga blokada itd


        //tablice
        [IgnoreDataMember]
        static public Budynki.Budynek[][] obiektyNaMapie = new Budynki.Budynek[Stale.IloscPol][];
        [DataMember]
        public int[][][] ukladMapy                = new int[2][][]; // 0 - ZIndex, 1 - droga, blokady itd.

        

        //liczby
        private ulong idBudynkow = 0;
        private ulong idPostaci  = 0;
        [DataMember]
        private double time     = 0;
        [DataMember]
        public ulong IDBudynkow
        {
            get { return idBudynkow; }
            set { idBudynkow = value; }
        }
        [DataMember]
        public ulong IDPostaci
        {
            get { return idPostaci; }
            set { idPostaci = value; }
        }

        //listy
        private ObservableCollection<Postacie.Postac> listaPracujacych;
        private ObservableCollection<Postacie.Postac> listaNiePracujacych;
        [DataMember]
        private ObservableCollection<Budynki.Budynek>   listaStrazy;
        [DataMember]
        private ObservableCollection<Budynki.Budynek> listaBudynkow;
        [DataMember]
        private ObservableCollection<Postacie.Postac> listaPostaci;
        [DataMember]
        private List<Zadania>                         listaZadan;
        [DataMember]
        private ObservableCollection<ulong> listaIDPracujacych;
        [DataMember]
        private ObservableCollection<ulong> listaIDNiePracujacych;


        //inne
        private Animacja logikaStan;
        public static FunkcjeXAML mainPageFunkcje;
        private bool exit = false;

        public ObservableCollection<Postacie.Postac> ListaOsobPracujacych
        {
            get { return this.listaPracujacych; }
        }
        public ObservableCollection<Postacie.Postac> ListaOsobNiePracujacych
        {
            get { return this.listaNiePracujacych; }
        }

        private List<Budynki.Budynek> ScalBudynki()
        {
            List<Budynek> listaWszystkich = new List<Budynek>(listaBudynkow);
            listaWszystkich.AddRange(listaStrazy);
            return listaWszystkich;
        }

        public void PrzywrocDane()
        {
            dodajZadanie += DodajZadanie;
            dodajBudynki += DodajBudynki;
            dodajPostacie += DodajPostacie;
            usunPostac += UsunPostac;
            usunBudynek += UsunBudynek;
            usunZadanie += UsunZadanie;
            pobierzPostac += PobierzPostac;
            pobierzZUkladuMapy += PobierzZUkladuMapyDane;
            listaPracujacych = new ObservableCollection<Postacie.Postac>();
            listaNiePracujacych = new ObservableCollection<Postacie.Postac>();

            obiektyNaMapie = new Budynki.Budynek[Stale.IloscPol][];

            var wszystkieBudynki = ScalBudynki();

            for (int i = 0; i < Stale.IloscPol; i++)
                for (int j = 0; j < Stale.IloscPol; j++)
                    obiektyNaMapie[i] = new Budynek[Stale.IloscPol];

            for(int i = 0; i < Stale.IloscPol; i++)
                for(int j = 0; j < Stale.IloscPol; j++)
                    switch(ukladMapy[1][i][j])
                    {
                        case 1:
                            //buduj droge
                        mainPageFunkcje.DrogaCanvas.Children.Add(new Image()
                            {
                                Width = Stale.SzerokoscPola+1,
                                Height = Stale.SzerokoscPola + 1,
                                Source = new Windows.UI.Xaml.Media.Imaging.BitmapImage(new Uri("ms-appx:///Assets/img/droga.png")),
                                RenderTransform = new CompositeTransform() 
                                { 
                                    TranslateX = i * Stale.SzerokoscPola, 
                                    TranslateY = j * Stale.SzerokoscPola 
                                }
                            });
                        break;
                        case 2:
                            //buduj blokade
                        break;
                    }

            foreach(var item in wszystkieBudynki)
            {
                Point pozycja = new Point((int)item.lokalizacja.X / Stale.SzerokoscPola, (int)item.lokalizacja.Y / Stale.SzerokoscPola);

                for(int i = (int)pozycja.X; i < (int)pozycja.X + item.IloscZajetychPol.X; i++)
                    for(int j = (int)pozycja.Y; j < (int)pozycja.Y + item.IloscZajetychPol.Y; j++)
                    {
                        GameEngine.obiektyNaMapie[i][j] = item;

                    }
            }


            for(int i = 0; i < listaIDPracujacych.Count; i++) //przywracanie niepracujacych
            {
                foreach(var it in listaPostaci)
                {
                    if(it.id == listaIDPracujacych[i])
                    {
                        this.listaPracujacych.Add(it);
                        break;
                    }
                }
            }

            for(int i = 0; i < listaIDNiePracujacych.Count; i++) //przywracanie pracujacych
            {
                foreach(var it in listaPostaci)
                {
                    if(it.id == listaIDNiePracujacych[i])
                    {
                        this.listaNiePracujacych.Add(it);
                        break;
                    }
                }
            }
           
            foreach(var zad in listaZadan)
            {
                switch(zad.CzyPostac)
                {
                    case true:
                    
                        var p = listaPostaci.FirstOrDefault(x => x.id == zad.NrObiektu);
                        zad.Obiekt = p;
                        Action zadanieOb = Zadania.WydajZadanie(p.GetType(), zad.NazwaFunkcji, p, typeof(Action)) as Action;
                        zad.Zadanie = zadanieOb;
                    break;
                    case false:
                        var b = wszystkieBudynki.FirstOrDefault(x => x.id == zad.NrObiektu);                       
                        zad.Obiekt = b;
                        Action zadanieOb2 = Zadania.WydajZadanie(b.GetType(), zad.NazwaFunkcji, b, typeof(Action)) as Action;
                        zad.Zadanie = zadanieOb2;
                    break;
                }
            }

            foreach(var b in wszystkieBudynki)
            {
                b.DodajDane(dodajZadanie, dodajPostacie, usunBudynek, usunZadanie, usunPostac, ListaOsobNiePracujacych, ListaOsobPracujacych, pobierzPostac);
                b.PrzywrocDane();
                b.PrzywrocObrazek();
            }

            //foreach(var b in listaBudynkow)
            //{
            //    b.DodajDane(dodajZadanie, dodajPostacie, usunBudynek, usunZadanie, usunPostac, ListaOsobNiePracujacych, ListaOsobPracujacych, pobierzPostac);
            //    b.PrzywrocDane();
            //    b.PrzywrocObrazek();
                
            //    //tutaj jeszcze przywracanie na mapie i przywracanie obrazku
            //}

            //foreach(var b in listaStrazy)
            //{
            //    b.DodajDane(dodajZadanie, dodajPostacie, usunBudynek, usunZadanie, usunPostac, ListaOsobNiePracujacych, ListaOsobPracujacych, pobierzPostac);
            //    b.PrzywrocDane();
            //    b.PrzywrocObrazek();
            //}

            foreach(var p in listaPostaci)
            {
                p.DodajDane(this.dodajZadanie, this.usunZadanie, this.usunPostac);
                Budynek dom = listaBudynkow.Where(x => x.id == p.domekRodzinnyID).ToArray()[0] as Budynek;
                Budynek zaklad = null;
                var ex = wszystkieBudynki.Where(x => x.id == p.zakladPracyID);

                //var ex = listaStrazy.Where(x => x.id == p.zakladPracyID);
                //if(ex.Count() == 0)
                //{
                //    ex = listaBudynkow.Where(x => x.id == p.zakladPracyID);
                //}

                zaklad = ex.ToArray()[0] as Budynek;
                p.PrzywrocDane(dom, zaklad);
                p.PrzywrocObrazek();
            }



            //to na koncu by nie zrobic nowych rzeczy
            this.listaPracujacych.CollectionChanged += ListaPracujacych_Zmiana;
            this.listaNiePracujacych.CollectionChanged += ListaNiePracujacych_Zmiana;


        }

        private int PobierzZUkladuMapyDane(int z, int x, int y)
        {
            return this.ukladMapy[z][x][y];
        }

        private Postac PobierzPostac(ulong id)
        {
            foreach(var it in listaPostaci)
            {
                if(it.id == id)
                {
                    return it;
                }
            }
            return null;
        }
        public void FunkcjeFormatki(FunkcjeXAML asd)
        {
            mainPageFunkcje = asd;
        }

        public GameEngine()
        {
            dodajZadanie       += DodajZadanie;
            dodajBudynki       += DodajBudynki;
            dodajPostacie      += DodajPostacie;
            usunPostac         +=  UsunPostac;
            usunBudynek        += UsunBudynek;
            usunZadanie        += UsunZadanie;
            pobierzPostac      += PobierzPostac;
            pobierzZUkladuMapy += PobierzZUkladuMapyDane;

            listaBudynkow             = new ObservableCollection<Budynki.Budynek>();
            listaPracujacych          = new ObservableCollection<Postacie.Postac>();
            listaNiePracujacych       = new ObservableCollection<Postacie.Postac>();
            listaPostaci              = new ObservableCollection<Postacie.Postac>();
            listaStrazy               = new ObservableCollection<Budynki.Budynek>();
            listaZadan                = new List<Zadania>();
            listaIDPracujacych        = new ObservableCollection<ulong>();
            listaIDNiePracujacych     = new ObservableCollection<ulong>();

            this.listaPracujacych.CollectionChanged += ListaPracujacych_Zmiana;

            this.listaNiePracujacych.CollectionChanged += ListaNiePracujacych_Zmiana;

            
            for(int i = 0; i < 2; i++)
            {
                ukladMapy[i] = new int[Stale.IloscPol][];

                for (int j = 0; j < Stale.IloscPol; j++)
                    ukladMapy[i][j] = new int[Stale.IloscPol];
            }

            for (int i = 0; i < Stale.IloscPol; i++)
            {
                obiektyNaMapie[i] = new Budynki.Budynek[Stale.IloscPol];
            }

            for(int i = 0; i < Stale.IloscPol; i++)
                for(int j = 0; j < Stale.IloscPol; j++)
                    ukladMapy[0][i][j] = i + j;
        }

        private void ListaNiePracujacych_Zmiana(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch(e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                this.listaIDNiePracujacych.Add((e.NewItems[0] as Postacie.Postac).id);
                break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                this.listaIDNiePracujacych.Remove((e.OldItems[0] as Postacie.Postac).id);
                break;
            }
        }

        private void ListaPracujacych_Zmiana(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch(e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    this.listaIDPracujacych.Add((e.NewItems[0] as Postacie.Postac).id);
                break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    this.listaIDPracujacych.Remove((e.OldItems[0] as Postacie.Postac).id);
                break;
            }
        }

        private void UsunZadanie(object obj)
        {
            object zablokuj = new object();

            Parallel.For(0, this.listaZadan.Count, item => 
            {
                lock(zablokuj)
                {
                    if(listaZadan[item].Obiekt == obj)
                    {
                        //listaZadan[item].Obiekt = null; //tutaj dodać jeszcze usuwanie numerka ID
                        listaZadan[item] = null;
                        listaZadan.RemoveAt(item);
                        
                    }
                }
            });
        }

        private void UsunPostac(Postacie.Postac post, byte nrListy = 0)
        {
            switch(nrListy)
            {
                case 0: //Pracujacy
                    this.listaPracujacych.Remove(post);
                break;
                case 1: //Niepracujacy
                    this.listaNiePracujacych.Remove(post);
                break;
                case 2: //przeniesienie z listy osób pracujacych do niepracujacych
                    this.listaPracujacych.Remove(post);
                    this.listaNiePracujacych.Add(post);
                    post.zakladPracy = null;
                return;
            }

            this.listaPostaci.Remove(post);
        }

        private void UsunBudynek(Budynki.Budynek bud)
        {
            if(bud is ProjektInzynierGra.Klasy.Budynki.Straz)
                this.listaStrazy.Remove(bud);
            else
                this.listaBudynkow.Remove(bud);

            UsunBudynekZPlanszy(bud.obrazek);
        }
        public void GameStart()
        {
            Window.Current.Content.KeyDown += SprawdzanieExit;
            WznowLogike();
            //stoper.Start();
        }
        private IEnumerator<bool> GameLoop()
        {
            while(!exit)
            {

                Parallel.ForEach(listaBudynkow, item =>
                {
                    item.hp -= item.hpLost;
                    if(item.hp <= 25) // tutaj zrobic zgloszenie dla strazaka
                    {

                    }
                });

                var lenZadania = listaZadan.Count;

                for(int i = 0; i < lenZadania; i++)
                {
                    listaZadan[i].ZwiekszLicznikCzasu();

                    if(listaZadan[i].ZadanieGotoweDoOdpalenia) //Dodać tutaj animacje nową
                    {
                        listaZadan[i].Zadanie();

                        listaZadan[i] = null;
                        listaZadan.RemoveAt(i);
                        --lenZadania; --i;
                    }
                }

                time++;

                yield return true;
            }
        }


        private void DodajZadanie(Action zadanie, object ob, ulong nr, int czas = 0, bool czyPostac = false)
        {
            this.listaZadan.Add(new Zadania(zadanie, ob, nr, czas, czyPostac));
        }

        private void DodajPostacie(Postacie.Postac post)
        {
            this.listaPostaci.Add(post);
            //this.ListaIDBudynek.Add(id);
            post.id = IDPostaci++;
        }

        
        private void DodajBudynki(Budynki.Budynek bud)
        {
            this.listaBudynkow.Add(bud);
            bud.id = IDBudynkow++;

            if(bud is Budynki.Straz)
            {
                //tutaj dodawanie do listy strazy
            }
            if(bud is Budynki.DomekMieszkalny)
            {
                //tutaj cośtam
            }

            bud.obrazek.DataContext = bud;
            mainPageFunkcje.BudynkiCanvas.Children.Add(bud.obrazek);
        }

        private void UsunBudynekZPlanszy(UIElement obj)
        {
            mainPageFunkcje.BudynkiCanvas.Children.Remove(obj);
        }

        public void WznowLogike()
        {
            this.logikaStan = SilnikAnimacji.DodajAnimacje(GameLoop(), TimeSpan.FromMilliseconds(250));
        }

        public void ZatrzymajLogike()
        {
            SilnikAnimacji.ZatrzymajLogike();
        }
        
        private void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if(PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void SprawdzanieExit(object sender, Windows.UI.Xaml.Input.KeyRoutedEventArgs e)
        {
            if(e.Key == Windows.System.VirtualKey.Escape)
            {
                //Tutaj dodac jeszcze ostrzeżenie i czy napewno chcemy zakończyć
                //albo otwarcie AppBara i tam Zatrzymanie logiki
                ZatrzymajLogike();
            }
        }
    }
}
