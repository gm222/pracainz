﻿using ProjektInzynierGra.Klasy.Budynki;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace ProjektInzynierGra.Klasy.Game.AlgorytmyFunckje
{
    static public class Algorytmy
    {
        static private GameEngine gSilnik;
        public static void UstawGameEngine(GameEngine gE)
        {
            gSilnik = gE;
        }

        #region BFS
        public static int[][] BFS(Point StartPoint, Point EndPoint)
        {
            List<Pozycja> trasa = new List<Pozycja>();
            bool[][] tym = new bool[Stale.IloscPol][];
            for (int i = 0; i < Stale.IloscPol; i++)
                tym[i] = new bool[Stale.IloscPol];

            Stopwatch watch = new Stopwatch();
            watch.Start();

            trasa.Add(new Pozycja((int)StartPoint.X, (int)StartPoint.Y, null));
            tym[(int)StartPoint.X][(int)StartPoint.Y] = true;

            bool koniec = false;
            int pozycjaNaLiscie = 0;
            Pozycja aktualnaPozycja;
            do
            {
                aktualnaPozycja = trasa[pozycjaNaLiscie];
                int x = aktualnaPozycja.X;
                int y = aktualnaPozycja.Y;
                SprawdzanieBFS(x - 1, y, ref tym, ref trasa, ref aktualnaPozycja, ref koniec, ref EndPoint);
                if (koniec == true)
                    break;
                SprawdzanieBFS(x, y - 1, ref tym, ref trasa, ref aktualnaPozycja, ref koniec, ref EndPoint);
                if (koniec == true)
                    break;
                SprawdzanieBFS(x + 1, y, ref tym, ref trasa, ref aktualnaPozycja, ref koniec, ref EndPoint);
                if (koniec == true)
                    break;
                SprawdzanieBFS(x, y + 1, ref tym, ref trasa, ref aktualnaPozycja, ref koniec, ref EndPoint);
                if (koniec == true)
                    break;
                pozycjaNaLiscie++;
            } while (koniec == false && trasa.Count != pozycjaNaLiscie);


            List<int[]> doWypisania = new List<int[]>();
            if (koniec == true)
                while (aktualnaPozycja.Rodzic != null)
                {
                    doWypisania.Add(new int[]{aktualnaPozycja.X,aktualnaPozycja.Y});
                    aktualnaPozycja = aktualnaPozycja.Rodzic;
                }
            int[][] tymczasowa = doWypisania.ToArray();

            System.Diagnostics.Debug.WriteLine("BFS: " + watch.Elapsed.ToString());
            if (doWypisania.Count > 0)
                return tymczasowa;
            else
                return null;
        }

        private static void SprawdzanieBFS(int x, int y, ref  bool[][] tym, ref List<Pozycja> trasa, ref Pozycja aktualnaPozycja, ref bool koniec, ref Point EndPoint)
        {
            if (x >= 0 && x < Stale.IloscPol && y >= 0 && y < Stale.IloscPol)
            {
                if(tym[x][y] == false && gSilnik.ukladMapy[1][x][y] == 1 )
                {
                    trasa.Add(new Pozycja(x, y, aktualnaPozycja, 0));
                    tym[x][y] = true;
                }
                else if (EndPoint.X == x && EndPoint.Y == y)
                {
                    trasa.Add(new Pozycja(x, y, aktualnaPozycja, 0));
                    tym[x][y] = true;
                    koniec = true;
                }
            }
        }
        #endregion

        #region BFS_Obserwacja

        public static List<int[][]> BFS_Obserwacja(int iloscKrokow, Point StartPoint)
        {
            List<Pozycja> trasa = new List<Pozycja>();
            bool[][] tym = new bool[Stale.IloscPol][];
            for (int i = 0; i < Stale.IloscPol; i++)
                tym[i] = new bool[Stale.IloscPol];

            trasa.Add(new Pozycja((int)StartPoint.X, (int)StartPoint.Y, null, 0));
            tym[(int)StartPoint.X][(int)StartPoint.Y] = true;

            bool koniec = true;
            
            int x = trasa[0].X;
            int y = trasa[0].Y;
            SprawdzanieBFSObserwacja(x - 1, y, ref tym, ref trasa, trasa[0], ref koniec, ref iloscKrokow);
            SprawdzanieBFSObserwacja(x, y - 1, ref tym, ref trasa, trasa[0], ref koniec, ref iloscKrokow);
            SprawdzanieBFSObserwacja(x + 1, y, ref tym, ref trasa, trasa[0], ref koniec, ref iloscKrokow);
            SprawdzanieBFSObserwacja(x, y + 1, ref tym, ref trasa, trasa[0], ref koniec, ref iloscKrokow);

            if(koniec == true)
                return null;

            int pozycjaNaLiscie = 0;
            List<int[][]> kierunki = new List<int[][]>();
            Pozycja aktualnaPozycja;
            do
            {
                koniec = true;
                aktualnaPozycja = trasa[pozycjaNaLiscie++];
                x = aktualnaPozycja.X;
                y = aktualnaPozycja.Y;
                SprawdzanieBFSObserwacja(x - 1, y, ref tym, ref trasa, aktualnaPozycja, ref koniec, ref iloscKrokow);
                SprawdzanieBFSObserwacja(x, y - 1, ref tym, ref trasa, aktualnaPozycja, ref koniec, ref iloscKrokow);
                SprawdzanieBFSObserwacja(x + 1, y, ref tym, ref trasa, aktualnaPozycja, ref koniec, ref iloscKrokow);
                SprawdzanieBFSObserwacja(x, y + 1, ref tym, ref trasa, aktualnaPozycja, ref koniec, ref iloscKrokow);
                if(aktualnaPozycja.Krok == iloscKrokow || koniec == true)
                {
                    List<int[]> listaOtrzymana = new List<int[]>();
                    DodanieDoListy(aktualnaPozycja, listaOtrzymana);
                    kierunki.Add(listaOtrzymana.ToArray());
                }
            } while (trasa.Count > pozycjaNaLiscie);
            return kierunki;
        }
        private static void SprawdzanieBFSObserwacja(int x, int y, ref  bool[][] tym, ref List<Pozycja> trasa, Pozycja aktualnaPozycja, ref bool koniec, ref int iloscKrokow)
        {
            if (x >= 0 && x < Stale.IloscPol && y >= 0 && y < Stale.IloscPol)
                if (tym[x][y] == false && gSilnik.ukladMapy[1][x][y] == 1  && aktualnaPozycja.Krok + 1 <= iloscKrokow)
                {
                        trasa.Add(new Pozycja(x, y, aktualnaPozycja, aktualnaPozycja.Krok + 1));
                        tym[x][y] = true;
                        koniec = false;
                }
        }
        private static void DodanieDoListy(Pozycja aktualnaPozycja, List<int[]> tym)
        {
            if(aktualnaPozycja.Rodzic != null)
                DodanieDoListy(aktualnaPozycja.Rodzic, tym);

            tym.Add(new int[] { aktualnaPozycja.X, aktualnaPozycja.Y });
            
        }

        #endregion

        #region AStar
        public static int[][] AStar(Point StartPoint, Point EndPoint, Budynek EndBudynek)
        {
            Element[,] tymPlansza = new Element[Stale.IloscPol,Stale.IloscPol];

            List<Element> trasa = new List<Element>();
            trasa.Add(new Element((int)StartPoint.X, (int)StartPoint.Y, null, Math.Sqrt(Math.Pow(StartPoint.X - EndPoint.X, 2) + Math.Pow(StartPoint.Y - EndPoint.Y, 2)), 0));

            tymPlansza[(int)StartPoint.X,(int)StartPoint.Y] = trasa[0];

            Element pozycjaKoncowa = null;
            SprawdzanieSasiadow(trasa[0].X, trasa[0].Y, trasa[0], EndPoint, ref tymPlansza, EndBudynek);

            do
            {
                Element aktualnaPozycja = null;
                if(trasa.Count>0) 
                    aktualnaPozycja = trasa[0];
                else
                    break;

                foreach (Element tymElement in trasa)
                    if (aktualnaPozycja.Suma > tymElement.Suma)
                        aktualnaPozycja = tymElement;

                do
                {

                    if (aktualnaPozycja.IloscSasiadow == 0)
                    {
                        trasa.Remove(aktualnaPozycja);
                        break;
                    }

                    aktualnaPozycja = aktualnaPozycja.NajlepszySasiad();

                    if ((aktualnaPozycja.X == EndPoint.X && aktualnaPozycja.Y == EndPoint.Y) ||  GameEngine.obiektyNaMapie[aktualnaPozycja.X][aktualnaPozycja.Y] == EndBudynek)
                    {
                        pozycjaKoncowa = aktualnaPozycja;
                        break;
                    }

                    trasa.Add(aktualnaPozycja);

                    SprawdzanieSasiadow(aktualnaPozycja.X, aktualnaPozycja.Y, aktualnaPozycja, EndPoint, ref tymPlansza, EndBudynek);
                    if (aktualnaPozycja.CzyBezKary == false)
                    {
                        aktualnaPozycja.Kara++;
                        break;
                    }

                } while (true);

            } while (pozycjaKoncowa == null && trasa.Count != 0);

            if (pozycjaKoncowa != null)
                return TworzenieListy(pozycjaKoncowa, new List<int[]>()).ToArray();
            else
                return null;
        }

        private static void SprawdzanieSasiadow(int x, int y, Element aktualnaPozycja, Point EndPoint, ref Element[,] tymPlansza, Budynek EndBudynek)
        {
            SprawdzanieAStar(x - 1, y, aktualnaPozycja, EndPoint, ref tymPlansza, EndBudynek);
            SprawdzanieAStar(x, y - 1, aktualnaPozycja, EndPoint, ref tymPlansza, EndBudynek);
            SprawdzanieAStar(x + 1, y, aktualnaPozycja, EndPoint, ref tymPlansza, EndBudynek);
            SprawdzanieAStar(x, y + 1, aktualnaPozycja, EndPoint, ref tymPlansza, EndBudynek);
        }

        private static List<int[]> TworzenieListy(Element pozycjaKoncowa, List<int[]> lista)
        {
            if (pozycjaKoncowa.Rodzic != null)
                TworzenieListy(pozycjaKoncowa.Rodzic, lista);
            lista.Add(new int[] { pozycjaKoncowa.X, pozycjaKoncowa.Y });
            return lista;
        }

        private static void SprawdzanieAStar(int x, int y, Element aktualnaPozycja, Point EndPoint, ref Element[,] tymPlansza, Budynek EndBudynek)
        {
            if (x >= 0 && x < Stale.IloscPol && y >= 0 && y < Stale.IloscPol && (GameEngine.obiektyNaMapie[x][y] == null || GameEngine.obiektyNaMapie[x][y] == EndBudynek))
            {
                double odleglosc = Math.Sqrt(Math.Pow(x - EndPoint.X, 2) + Math.Pow(y - EndPoint.Y, 2));
                int kara;
                if (odleglosc < aktualnaPozycja.OdlegloscOdKonca)
                    kara = aktualnaPozycja.Kara;
                else
                    kara = aktualnaPozycja.Kara + 1;
                Element tym = new Element(x, y, aktualnaPozycja, odleglosc , kara);
                if (tymPlansza[x,y] == null)
                {
                    aktualnaPozycja.DodajSasiada(tym);
                    tymPlansza[x,y] = tym;
                }
                else if (tymPlansza[x,y].Suma - tym.Suma>0)
                {
                    tymPlansza[x,y].Delete();
                    aktualnaPozycja.DodajSasiada(tym);
                    tymPlansza[x,y] = tym;
                }
            }
        }
        #endregion

        #region BFS-Droga
        public static int[][] BFSDroga(Point StartPoint, Point EndPoint)
        {
            List<Pozycja> trasa = new List<Pozycja>();
            bool[][] tym = new bool[Stale.IloscPol][];
            for (int i = 0; i < Stale.IloscPol; i++)
                tym[i] = new bool[Stale.IloscPol];

            trasa.Add(new Pozycja((int)StartPoint.X, (int)StartPoint.Y, null));
            tym[(int)StartPoint.X][(int)StartPoint.Y] = true;

            bool koniec = false;

            int pozycjaNaLiscie = 0;
            Pozycja aktualnaPozycja = trasa[pozycjaNaLiscie];
            List<int[]> doWypisania = new List<int[]>();
            if(StartPoint == EndPoint)
            {
                Wypisywanie(ref doWypisania, aktualnaPozycja);
                return doWypisania.ToArray();
            }
            do
            {
                aktualnaPozycja = trasa[pozycjaNaLiscie];
                int x = aktualnaPozycja.X;
                int y = aktualnaPozycja.Y;
                SprawdzanieBFSDroga(x - 1, y, ref tym, ref trasa, ref aktualnaPozycja, ref koniec, ref EndPoint);
                if (koniec == true)
                    break;
                SprawdzanieBFSDroga(x, y - 1, ref tym, ref trasa, ref aktualnaPozycja, ref koniec, ref EndPoint);
                if (koniec == true)
                    break;
                SprawdzanieBFSDroga(x + 1, y, ref tym, ref trasa, ref aktualnaPozycja, ref koniec, ref EndPoint);
                if (koniec == true)
                    break;
                SprawdzanieBFSDroga(x, y + 1, ref tym, ref trasa, ref aktualnaPozycja, ref koniec, ref EndPoint);
                if (koniec == true)
                    break;
                pozycjaNaLiscie++;
            } while (koniec == false && trasa.Count != pozycjaNaLiscie);


            if (koniec == true)
                Wypisywanie(ref doWypisania, trasa[trasa.Count-1]);
            int[][] tymczasowa = doWypisania.ToArray();

            if (doWypisania.Count > 0)
                return tymczasowa;
            else
                return null;
        }

        private static void SprawdzanieBFSDroga(int x, int y, ref  bool[][] tym, ref List<Pozycja> trasa, ref Pozycja aktualnaPozycja, ref bool koniec, ref Point EndPoint)
        {
            if (x >= 0 && x < Stale.IloscPol && y >= 0 && y < Stale.IloscPol)
            {
                if (EndPoint.X == x && EndPoint.Y == y)
                {
                    trasa.Add(new Pozycja(x, y, aktualnaPozycja, 0));
                    tym[x][y] = true;
                    koniec = true;
                }
                else if (tym[x][y] == false && GameEngine.obiektyNaMapie[x][y] == null)
                {
                    trasa.Add(new Pozycja(x, y, aktualnaPozycja, 0));
                    tym[x][y] = true;
                }
            }
        }
        #endregion
        private static void Wypisywanie(ref List<int[]> doWypisania, Pozycja aktualnaPozycja)
        {
            if (aktualnaPozycja.Rodzic != null)
                Wypisywanie(ref doWypisania, aktualnaPozycja.Rodzic);
            doWypisania.Add(new int[] { aktualnaPozycja.X, aktualnaPozycja.Y });
        }
    }
}
